  ___                   ____  _                       _          _
 / _ \ _ __   ___ _ __ / ___|| |_ _ __ __ _ _ __   __| | ___  __| |
| | | | '_ \ / _ \ '_ \\___ \| __| '__/ _` | '_ \ / _` |/ _ \/ _` |
| |_| | |_) |  __/ | | |___) | |_| | | (_| | | | | (_| |  __/ (_| |
 \___/| .__/ \___|_| |_|____/ \__|_|  \__,_|_| |_|\__,_|\___|\__,_|
      |_|

PROJECT DEFINITION
------------------------------------------------------

OpenStranded is a free clone of the freeware game Stranded II made by Peter
Schau� ("Unreal Software") using open source libraries. It will be released
under the licensing terms of the GNU General Public License (GPL) in its
version 3.

This project definition should be considered as a draft. Comments and criticism
are highly appreciated.



SHORT-TERM 1ST PRIORITY PROJECT GOAL

* Creating an early version of the game ("Pre-Alpha") to be presented on the
  Unreal Software web forum, including hence:
	+ Basic features like running and swimming/diving
	+ Basic abilities to load Stranded II's object definitions stored in
	  text files
	+ Reading and writing of maps and savegames (ignoring scripts)

Note: The editor is not at top priority, rather a better support of the
Stranded II maps and definition will be done



ACTUAL PROJECT GOALS AFTER PRE-ALPHA

Okay, the main goal should be obvious: Clone Stranded II the best as you can.
In addition:

* Create an enhanced map editor
	+ Better user interface, e.g. improved text input (Copy-and-Paste)
* OpenStranded map and save format (probably XML)
	+ Option of conversion of Stranded II maps into OpenStranded maps
* Ability to read the whole object/game/etc definitions, thus being able to use
  Stranded II's objects and definitions
* Full implementation of the scripting engine (note below)



TECHNICAL ISSUES

* The mainly used programming language is C++ (the parser will be written in C
  because it is better supported by Flex and Bison at the moment)
	+ Coding guidelines yet to be discussed
* Irrlicht is used as graphics engine
* The sound engine will be Irrklang
* Script parser is going to be implented using Flex and Bison
* Linux support



LEGACY STRANDED II SUPPORT

* At least 95% compatibility with the Stranded II Scripting Language
  (S2S, note below)
* Ability to read and write savegames and maps created using Stranded II (note
  below)



SCRIPTING LANGUAGE

* PHP/C-like style (as in S2S)
* Implementation of all commands of S2S
* Improvements and simplifications in the syntax
	+ All function's arguments have to be put in braces (instead of just
	  those with return values)
	+ Full implementation of loops (for, while, do-while), legacy support
	  for the old "loop()" command
	+ Clean up implementation of elementary types like floating-point
	  numbers and especially strings
	+ (Add array-like datatype?)
* Improve parsing of nested function calls
* Allow map crator to define own functions (S2S could just handle procedures
  without return value, using the "event" command)
* Addition of some kind of "new mode" for changes of existing commands and
  clean-ups



FEATURES BEYOND 1.0

* Multiplay mode (note: the caption is "_BEYOND_ 1.0", if you want it earlier,
  do it yourself ;) )
