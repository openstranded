S2S Parser                                    OpenStranded Development Protocol

Date: 02-15-2009
Author: Hermann Walth
Topic: The s2s_val Datatype

/----------------------\
| 0. Table of Contents |
\----------------------/

0. Table of Contents
1. Introduction
2. The Datatype
3. Possible Extensions
4. Addenda

/-----------------\
| 1. Introduction |
\-----------------/

Easy extensibility is one of the main aims of the OpenStranded project.
Therefore, thorough documentation of its core interfaces is an important task.
There is one data type which the whole structure of the scripting
parser revolves around: The s2s_val datatype.

This document is to serve two purposes: Firstly, to document this cardinal
data structure for those who wish to extend its capabilities or write
custom scripting functions; and secondly, to propose several extensions to the
current datatype to OpenStranded's project lead.

/-----------------\
| 2. The Datatype |
\-----------------/

The s2s_val structure is defined in the header file "s2script.h" (within
the parser's directory, which is "./src/s2script/", seen relatively to the 
openstranded root directory), along with a few functions to handle this type.
The structure is basically only a container for all the different datatypes
that any value in the parser's context may incorporate.

s2s_val consists of two fields: A type indicator `type` and a union `val` 
	containing the actual value.
The type indicator itself is an enumerated type and may contain one of the
	following constants:
	* S2S_INT_T - indicating an integer numeric value
	* S2S_FLOAT_T - indicating a floating point numeric value
	* S2S_STRING_T - indicating a text value
	With the extensions proposed in §3, more possible constants will be
	added to the above enumeration.
The value union consists of three fields, one for each possible datatype.
(Remark: It could also have been a void pointer instead of a union, but the
parser's programmer, both options being apparent, chose the union over a 
pointer because he considered it both more memory efficient and contributing
to documentation as all possible datatypes would be declared within the 
s2s_val structure)
Any of these fields should only be accessed if the type indicator says that it
is meaningful, e.g. the field `fnum` should not be accessed if the the type
indicator is set to S2S_STRING_T.
The following is a list of all currently existent fields and their 
	corresponding type indicator values:
	* val.string - text value of type `char*`; type indicator is
	  S2S_STRING_T
	* val.num - integer number of type `long`; type indicator is S2S_INT_T
	* val.fnum - floating point number of type `double`;
			type indicator is S2S_FLOAT_T
Note that the extensions may as well add more fields to this union or assign
	another type indicator to an existing field.

As said before, the "s2script.h" header additionally declares a few functions
to handle s2s_val variables. These functions are usually inline, but some
are implemented in the c++ module "s2script.cc".
This file shall document these functions as well.

Firstly, there are the type checking functions. These have a name of the form
	s2s_val_is{typename}, where typename may be either int, float or string,
	and take one single argument of type s2s_val to check whether the type
	indicator of its argument corresponds to the function's type, returning
	a boolean.
	A call to one of these functions is equivalent to the expression
	`var.type == S2S_$type_T`. These inline functions are supposed to ease
	typing and reading of such tests.

Secondly, there are the builder functions. Their names have the form
	s2s_val_build{typename}. Each accepts a single argument of the type
	corresponding to `typename` (i.e. long, double or char*) and construct
	an s2s_val from this argument, setting the type indicator correctly.
	Note that s2s_val_buildstring does not copy its string argument but
	rather sets the new s2s_val's string pointer to exactly the same string.
	This may be harmful in some cases, especially when one of these string
		pointers may be deallocated while the other is still in use.
		(read the OSDP from 02-11-2009 for more information on this
		issue)
	Being inline functions, there are equivalent expressions to these
	methods as well. At several points in the parser's code, such 
	alternative expressions are still in use. These structure literals are
	relatively hard to read and should be avoided.
	As an example, the function call `s2s_buildstring ("foo")` is equivalent
	to the expression `(s2s_val) {S2S_STRING_T, {.string = "foo"}}`

Next, the functions s2s_val_copy and s2s_val_free are closely related
	to the issue discussed in OSDP 2009-02-11. For once, these functions
	are not inline.
	Both functions take an s2s_val argument; s2s_val_copy returns an s2s_val
		while s2s_val_free returns void.
	To non-string values, these functions do nothing (or simply return their
		argument).
	Given a string value, however, s2s_val_copy allocates memory on the heap
		to store a copy of the argument and returns this copy.
		s2s_val_free deallocates the memory associated with the
		argument.
	Beware not to call s2s_val_free with a string that already has been
		deallocated or that is stored statically. Doing so will result
		in unpredictable behaviour.
		Note that script variables always call s2s_val_free upon their
		expiration.
		It is therefore best to always set variables to s2s_vals created
			by s2s_val_copy.

/------------------------\
| 3. Possible Extensions |
\------------------------/

This document proposes the following three additional s2s_val types:
1) S2S_ERROR_T
2) S2S_VOID_T
3) S2S_OBJECT_T
Each of these types shall be introduced separately.

1) S2S_ERROR_T is a type that shall be returned by functions to indicate
	that something has gone wrong. This type will be especially useful for
	type checking routines of function arguments, i.e. many functions will
	use this type to tell the parser that one of their arguments is
	ill-typed or ill-formatted.
	s2s_vals of this type will also use the string value field to store a
	short error message which will be reported to the player.
	It is to be discussed whether these error messages should be statically
		built-into the function code (disallowing the parser to
		deallocate them) or whether they should be created dynamically
		(prompting the parser to deallocate them)
	Because these types are crucial to the parser, they have already been
		implemented before the project lead's consent was given. There
		are, however, no routines yet to handle these new types.

2) All scripting functions are required to return an s2s_val struct, even if
	they don't return anything meaningful at all. 
	The S2S_VOID type shall allow these functions to tell the parser that
	their return value has no meaning.
	This serves one single purpose: To strictly prevent non-returning
		functions from being used within expressions. This would not be 
		possible if such functions were forced to pretend to return a
		meaningful value.
	If an s2s_val's type indicator is set to S2S_VOID_T, all of its value
		fields should be ignored.

3) S2S_OBJECT_T would be the most extensive type the parser possibly 
	could have.
	One may be able to tell from heavy modifications of the original game -
	like S2Ext or ASoS - that modders might come to a point where they need
	to build custom script value types. This is what S2S_OBJECT_T shall be
	for.
	This type would add a new field to s2s_val's value union:
		A void pointer to the custom data structure,
		and optionally a string which would effectively serve an 
		extensive type indicator but also as a short description of the
		assigned object.
	These two components would be bundled in another struct which would then
		make up the new field of the value union, making the s2s_val
		structure rather complex.
	One could even design an interface to register certain functions and
		associate these with a certain type indicator string. These
		methods could then be respected by functions such as echo, free
		or the in-string-variable expansion routine.
		More detailed ideas on this subject would, however, go beyond
		the scope of this document.
	The S2S language would, with this extension, thus come quite near to the
		concept of object orientated programming.
	It is, however, arguable whether this functionality is really 
		needed for S2S.

/------------\
| 4. Addenda |
\------------/

The project lead has decided on 02-15-2009 that:
1) S2S_ERROR_T will be implemented. All errormessages have to be
	dynamically allocated.
2) S2S_VOID_T will be implemented in the way it is described.
3) S2S_OBJECT_T will be implemented as well. It will have type indicator
	strings for now, but these may be obsoleted by a shared type string
	table with each object containing an index to this table.
	Abstract type checking functions shall be created to make a smooth
	transition possible if one is required.
	The question whether OOP-like functionality is required is not yet
	clarified.

