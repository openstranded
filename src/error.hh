/*
 * This file includes global error routines used throughout OpenStranded
 *
 * Copyright (C) 2008 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_ERROR_HH
#define STRANDED_ERROR_HH

#include <iostream>
#include <string>
#include <exception>

/**
 * The StrandedException base class.
 * OpenStranded uses a classical Warning - Error - Fatal exception
 * level scheme.
 * You should use the following classes instead of their StrandedException
 * base class.
 * NOTE: Implementation of warnings through exceptions is somewhat difficult,
 * for you will most probably want to continue your function without disruption
 * after throwing a Warning, exceptions will, however, always break out of the
 * function.
 * Therefore you should not throw this exception if there is another
 * approach to this problem.
 * @see StrandedWarning
 * @see StrandedError
 * @see StrandedFatal
 */
class StrandedException: public std::exception
{
	private:
		/**
		 * The error message.
		 */
		std::string message;
		
	public:
		/**
		 * The constructor.
		 * @param message The error message
		 */
		StrandedException (const char* message);

		/**
		 * The constructor.
		 * @param message The error message
		 */
		StrandedException (const std::string& message);

		/**
		 * The destructor.
		 */
		~StrandedException () throw ();

		/**
		 * This is a virtual method of the std::exception class
		 * @return The error message
		 */
		const char*
		what () throw ();
		
		/**
		 * Print the error message.
		 */
		virtual void
		print ();
};



/**
 * This class defines an Exception with the "Warning" level.
 * Consider the comments on the StrandedException base class.
 * @see StrandedException
 */
class StrandedWarning: public StrandedException
{
	public:
		/**
		 * The constructor.
		 * @param message The error message
		 */
		StrandedWarning (const char* message);

		/**
		 * The constructor.
		 * @param message The error message
		 */
		StrandedWarning (const std::string& message);

		/**
		 * Print the error message.
		 * This method will print the error message preceded by an
		 * indicator of the error level.
		 */
		void
		print ();
};


/**
 * This class defines an Exception with the "Error" level.
 * Consider the comments on the StrandedException base class.
 * @see StrandedException
 */
class StrandedError: public StrandedException
{
	public:
		/**
		 * The constructor.
		 * @param message The error message
		 */
		StrandedError (const char* message);

		/**
		 * The constructor.
		 * @param message The error message
		 */
		StrandedError (const std::string& message);

		/**
		 * Print the error message.
		 * This method will print the error message preceded by an
		 * indicator of the error level.
		 */
		void
		print ();
};


/**
 * This class defines an Exception with the "Fatal" level.
 * Consider the comments on the StrandedException base class.
 * @see StrandedException
 */
class StrandedFatal: public StrandedException
{
	public:	
		/**
		 * The constructor.
		 * @param message The error message
		 */
		StrandedFatal (const char* message);

		/**
		 * The constructor.
		 * @param message The error message
		 */
		StrandedFatal (const std::string& message);

		/**
		 * Print the error message.
		 * This method will print the error message preceded by an
		 * indicator of the error level.
		 */
		void
		print ();
};

#endif /* STRANDED_ERROR_HH */
