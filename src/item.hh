/*
 * This file includes the definitions for the item specific classes
 * derived from the Kingdom, Type and Entity base classes.
 *
 * Copyright (C) 2008 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_ITEM_HH
#define STRANDED_ITEM_HH

#include "kingdom.hh"
#include "type.hh"
#include "entity.hh"

#define S2_ITEM 3

/**
 * This is the class of all entities which belong to the kingdom
 * "item".
 * An item is the only entity which can be collected by the player
 * (without scripts) and which is stored in the inventory afterwards.
 * An item can't move without the help of scripts.
 */
class ItemEntity: public Entity
{
	public:
		/**
		 * The constructor.
		 * @param type The ID of the Type the item should belong to
		 */
		ItemEntity (ID type);
};


/**
 * This is the class of all item types derived from the Item base class.
 */
class ItemType: public Type
{
	public:
		/**
		 * The constructor.
		 * @param name The name of the new item type
		 */
		ItemType (const char* name);
};


/**
 * The Item Kingdom class
 */
class ItemKingdom: public Kingdom
{
	public:
		/**
		 * The constructor.
		 */
		ItemKingdom ();



		//
		// Virtual methods
		//

		/**
		 * Create a new item entity
		 * @param type The ID of the type the new item should belong to
		 * @return A pointer to the new Entity
		 * @see ItemEntity::ItemEntity()
		 */
		Entity*
		createEntity (ID type);

		/**
		 * Create a new item type
		 * @param name The name of the new type (visible in editor)
		 * @return A pointer to the new Type
		 * @see ItemType::ItemType()
		 */
		Type*
		createType (const char* name);
};

#endif /* STRANDED_ITEM_HH */
