/*
 * This file includes definitions for OpenStranded output mapping
 *
 * Copyright (C) 2009 David Kolossa
 *
 * This file is part of OpenStranded.
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <cstdarg>

/**
 * This is the class for OpenStranded output to either cout or cerr and/or log
 * files.
 */
class Out
{
	private:
		class Printer
		{
			private:
				void (*printer)(std::string);

			public:
				Printer(void (*printer)(std::string));



				//
				// Texts
				//

				Printer&
			 	operator<<(std::string msg);

				Printer&
			 	operator<<(const char *msg);

				Printer&
				operator<<(char msg);



				//
				// Signed integers
				//

				Printer&
				operator<<(int msg);

				Printer&
				operator<<(short msg);

				Printer&
				operator<<(long msg);

				Printer&
				operator<<(long long msg);



				//
				// Unsigned integers
				//

				Printer&
				operator<<(unsigned int msg);

				Printer&
				operator<<(unsigned short msg);

				Printer&
				operator<<(unsigned long msg);

				Printer&
				operator<<(unsigned long long msg);



				//
				// Decimals
				//

				Printer&
				operator<<(float msg);

				Printer&
				operator<<(double msg);



				//
				// Format strings
				//

				int
				vprintf(const char *format, va_list args);
		
				int
				printf(const char *format, ...);
		};

	public:
		/**
		 * The console mode flags.
		 * These flags determine which messages are displayed on the
		 * console and which aren't.
		 */
		static int consoleFlags;

		/**
		 * The logging mode flags.
		 * These flags determine which messages are written to log files
		 * and which aren't
		 */
		static int loggingFlags;

		/**
		 * These flags indicate whether a printer has not finished its
		 * line.
		 */
		static int isWriting;

		/**
		 * Print fatal error message to stderr.
		 * @param The message
		 */
		static void
		fatalPrinter(std::string msg);

		/**
		 * Print common error message to stderr.
		 * @param The message
		 */
		static void
		errorPrinter(std::string msg);

		/**
		 * Print warning message to stderr.
		 * @param The message
		 */
		static void
		warningPrinter(std::string msg);

		/**
		 * Print a common message to stdout.
		 * Note that these messages can't be shut off.
		 * @param The message
		 */
		static void
		msgPrinter(std::string msg);

		/**
		 * Print a debug message.
		 * @param The message
		 */
		static void
		debugPrinter(std::string msg);


		/**
		 * The fatal stream.
		 */
		static Printer fatal;

		/**
		 * The error console stream.
		 */
		static Printer error;

		/**
		 * The warning stream.
		 */
		static Printer warning;

		/**
		 * The msg stream.
		 */
		static Printer msg;

		/**
		 * The debug stream.
		 */
		static Printer debug;

		/**
		 * The log file.
		 */
		static FILE *logFile;
		
		static const int NONE = 0;
		static const int FATAL = 1;
		static const int ERROR = 2;
		static const int WARNING = 4;
		static const int DEBUG = 8;
		static const int MSG = 16;	// only used internally

		static const char endl = '\n';

		Out();

		/**
		 * Set the log file name and open the file.
		 * This needs to be called at least once to allow log file
		 * generation.
		 * @param fileName The new log file name
		 */
		static void
		setLogFile(std::string fileName);
		

		/**
		 * Get console mode flags.
		 * These flags indicate which messages should be written to the
		 * console (cout, cerr).
		 * @return The current console mode flags.
		 */
		static int
		getConsoleFlags();

		/**
		 * Set console mode flags.
		 * These flags indicate which messages should be written to the
		 * console (cout, cerr).
		 * @param newflags The new console mode flags.
		 */
		static void
		setConsoleFlags(int newflags);

		/**
		 * Get logging mode flags.
		 * These flags indicate which messages should be written to
		 * log files.
		 * @return The current logging mode flags.
		 */
		static int
		getLoggingFlags();

		/**
		 * Set logging mode flags.
		 * These flags indicate which messages should be written to
		 * log files.
		 * @param newflags The new logging mode flags.
		 */
		static void
		setLoggingFlags(int newflags);
};

