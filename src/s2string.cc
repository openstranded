/*
 * See s2string.hh for more information about this file.
 *
 * Copyright (C) 2008 David Kolossa
 *
 * This file is part of OpenStranded.
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstring>
#include <cctype>
#include "s2string.hh"

int
s2str::explode (std::string str, std::string *buf, const char *token, size_t count)
{
	std::vector<size_t> tokenPos (1, 0);
	int warnFlag = 0;

	while (str.find (token, tokenPos.back () ) != std::string::npos)
	{
		tokenPos.push_back (str.find (token, tokenPos.back () ));
		str.erase (tokenPos.back (), 1);
	}
	tokenPos.push_back (str.length());

	// the number of tokens in str has to be one lower than count
	// but because of the additional entries for position 0 and the end,
	// the size of thevector has to be count+1
	if (tokenPos.size () < count+1)
	{
		warnFlag |= EXPLODE_LOWER;	// number of substrings lower than count
	}

	if (tokenPos.size () > count+1)
	{
		warnFlag |= EXPLODE_GREATER;	// number of substrings greater than count
	}

	// Catch the special case that no token occured
	if (tokenPos.size () == 2)
	{
		buf[0] = str;
		warnFlag |= EXPLODE_NO_TOKEN;
	}
	else
	{
		for (int i=0; i < (int) count; i++)
		{
			buf[i] = str.substr (tokenPos[i], (tokenPos[i+1] - tokenPos[i]));
		}
	}

	return warnFlag;
}


bool
s2str::stof (std::string str, float &buf)
{
	std::istringstream stream;
	bool pointOccured = false;

	for (std::string::iterator c = str.begin (); c != str.end (); c++)
	{
		if ( (*c >= '0' && *c <= '9') || (*c == '.' && !pointOccured) )
		{
			continue;
		}
		else if (*c == ' ' || *c == '\t')
		{
			str.erase (c--);
			continue;
		}
		else
		{
			buf = 0.0;
			return false;
		}
	}
	stream.str (str);
	stream >> buf;
	return true;
}


bool
s2str::stoi (std::string str, int &buf)
{
	std::istringstream stream;
	if (!s2str::isInteger (str))
		return false;

	// Remove whitespace
	for (std::string::iterator c = str.begin (); c != str.end (); c++)
	{
		if (*c == ' ' || *c == '\t')
		{
			str.erase (c--);
		}
	}
	stream.str (str);
	stream >> buf;
	return true;
}

bool
s2str::stob (std::string str, bool &buf)
{
	if (str == "true" || str == "on")
	{
		buf = true;
		return true;
	}
	if (str == "false" || str == "off")
	{
		buf = false;
		return true;
	}

	if(s2str::isInteger (str))
	{
		int tmp;

		// Remove whitespace
		for (std::string::iterator c = str.begin (); c != str.end (); c++)
		{
			if (*c == ' ' || *c == '\t')
			{
				str.erase (c--);
			}
		}

		std::istringstream (str) >> tmp;
		buf = (tmp != 0);
		return true;
	}
	
	return false;
}

bool
s2str::isInteger (std::string &str)
{
	for (std::string::iterator c = str.begin (); c != str.end (); c++)
	{
		if ((*c >= '0' && *c <= '9') || *c == ' ' || *c == '\t')
		{
			continue;
		}
		else
		{
			return false;
		}
	}
	return true;
}

bool
s2str::isDecimal (std::string &str)
{
	bool pointOccured = false;

	for (std::string::iterator c = str.begin (); c != str.end (); c++)
	{
		if ( (*c >= '0' && *c <= '9') || (*c == '.' && !pointOccured) || *c == ' ' || *c == '\t' )
		{
			continue;
		}
		else
		{
			return false;
		}
	}
	return true;
}

bool
s2str::isBool (std::string &str)
{
	if (str == "true" || str == "on" || str == "false" || str == "off")
		return true;

	return s2str::isInteger (str);
}

std::string
s2str::toLowercase(std::string &str)
{
	std::string lowercaseString;
	for(std::string::iterator c = str.begin(); c != str.end(); c++)
	{
		lowercaseString += std::tolower(*c);
	}
	return lowercaseString;
}

std::string
s2str::toUppercase(std::string &str)
{
	std::string uppercaseString;
	for(std::string::iterator c = str.begin(); c != str.end(); c++)
	{
		uppercaseString += std::toupper(*c);
	}
	return uppercaseString;
}


