/*
 * This file includes the implementation of the unit specific classes
 *
 * Copyright (C) 2008 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "unit.hh"

/*
 * These are the declarations of the UnitEntity class
 */

UnitEntity::UnitEntity (ID type):
	Entity(type)
{}


/*
 * These are the declarations of the UnitType class
 */

UnitType::UnitType (const char* name):
	Type (name)
{}

/*
 * Definitions of UnitKingdom
 */

UnitKingdom::UnitKingdom ():
	Kingdom ("unit")
{}


Entity*
UnitKingdom::createEntity (ID type)
{
	return new UnitEntity (type);
}


Type*
UnitKingdom::createType (const char* name)
{
	return new UnitType (name);
}
