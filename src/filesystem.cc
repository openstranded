/*
 * For information about this file see filesystem.hh
 *
 * Copyright (C) 2009 David Kolossa
 *
 * This file is part of OpenStranded.
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <cstdlib>
#include <cstring>

#ifndef WIN32
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <dirent.h>
	#include <fcntl.h>
	#include <errno.h>
#endif

#include "filesystem.hh"
#include "s2string.hh"
#include "eal/engine.hh"



//
// Class FileSystem
//

std::string FileSystem::modName = "";

void
FileSystem::setModName(std::string name)
{
	modName = name;
}

std::vector<std::string> *
FileSystem::getDirectoryVector(std::string dirPath)
{
	std::vector<std::string> *fileNames = new std::vector<std::string>();
	#ifndef WIN32
		DIR *dir = opendir(dirPath.c_str());
		if(dir == NULL)
		{
			std::cerr << "Directory " << dirPath << "could not be opened";
			return NULL;
		}
		dirent *file;
		while((file = readdir(dir)) != NULL)
		{
			if(std::strcmp(file->d_name, ".")  != 0 &&
			   std::strcmp(file->d_name, "..") != 0)
			{
				fileNames->push_back(std::string(file->d_name));
			}
		}
		closedir(dir);
		return fileNames;

	#else
		// TODO: Have fun, Windows porters!
		return NULL;
	#endif
}

std::string
FileSystem::getFileNameFromDirectoryVector(std::vector<std::string> *dirVector, const std::vector<std::string> &extensions, std::string name)
{
	for(std::vector<std::string>::iterator f = dirVector->begin(); f != dirVector->end(); f++)
	{
		// Split filename into extension and actual filename
		std::string extension = f->substr(f->find_last_of('.')+1);
		std::string cutFileName = f->substr(0, f->find_last_of('.'));

		// Check whether a file with a matching extension fits
		for(std::vector<std::string>::const_iterator e = extensions.begin(); e != extensions.end(); e++)
		{
			#ifndef WIN32
				// case-sensitive
				if((cutFileName + "." + s2str::toLowercase(extension)) == (name + "." + *e))
			#else
				// case-insensitive
				if((s2str::toLowercase(cutFileName) + "." + s2str::toLowercase(extension)) == (s2str::toLowercase(name) + "." + s2str::lowercase(*e)))
			#endif
			{
				return *f;
			}
		}
	}
	return "";
}

std::string
FileSystem::getDirectoryPart(std::string name)
{
	std::string::size_type pos;
	std::string directoryPart = "";
	if((pos = name.find_last_of('/')) != std::string::npos)
	{
		directoryPart = name.substr(0, pos+1);
	}
	return directoryPart;
}

std::string
FileSystem::getFilePart(std::string name)
{
	std::string::size_type pos;
	std::string filePart = name;
	if((pos = name.find_last_of('/')) != std::string::npos)
	{
		filePart = name.substr(pos+1);
	}
	return filePart;
}




//
// Directories
//
bool
FileSystem::initUserDirectory()
{
	std::string basePath = "";

	#ifndef WIN32
		basePath = std::getenv("HOME");
		basePath += "/.openstranded/";

		// user directory
		if(opendir(basePath.c_str()) == NULL)
		{
			int dirCreated = mkdir(basePath.c_str(), S_IRWXU);
			if(dirCreated != 0)
			{
				std::cerr << "WARNING: OpenStranded user directory couldn't be created" << std::endl;
				return false;
			}
		}

		// XXX: error handling could be solved in another (better) way

		int dirCreated;
		
		// map directory
		dirCreated = mkdir((basePath+"maps/").c_str(), S_IRWXU);
		if(dirCreated != 0)
		{
			std::cerr << "WARNING: OpenStranded user map directory couldn't be created" << std::endl;
		}

		// save directory
		dirCreated = mkdir((basePath+"saves/").c_str(), S_IRWXU);
		if(dirCreated != 0)
		{
			std::cerr << "WARNING: OpenStranded save game directory couldn't be created" << std::endl;
		}

		// "extra" directory for use by engine
		dirCreated = mkdir((basePath+"extra/").c_str(), S_IRWXU);
		if(dirCreated != 0)
		{
			std::cerr << "WARNING: OpenStranded save game directory couldn't be created" << std::endl;
		}
		return true;
	#else
		// TODO: Windows stuff
	#endif
}

DirectoryInfo*
FileSystem::getUserDirectoryInfo()
{
	std::string path = "";

	#ifndef WIN32
		path += std::getenv("HOME");
		path += "/.openstranded/";
		if(opendir(path.c_str()) == NULL)
		{
			// user directory is not yet initialized
			if(!initUserDirectory())
			{
				return NULL;
			}
		}
		return new DirectoryInfo(path);	
	#else
		// TODO: Windows stuff
		return NULL;
	#endif
}

DirectoryInfo*
FileSystem::getGameDirectoryInfo()
{
	// FIXME: I don't want this hardcoded but I can't get CMake to
	// deliver me the install path...
	return new DirectoryInfo("/usr/share/games/openstranded/");
}

DirectoryInfo*
FileSystem::getModDirectoryInfo()
{
	// FIXME: Same as in getGameDirectoryInfo()
	if(modName != "")
		return new DirectoryInfo("/usr/share/games/openstranded/mods/" + modName + "/");
	else
		return new DirectoryInfo("/usr/share/games/openstranded/mods/standard/");
}



//
// General Files
//
FileInfo*
FileSystem::getSaveFileInfo(std::string name)
{
	std::string path;

	path = getUserDirectoryInfo()->path + "saves/" + name;

	FILE *f;
	if((f = std::fopen((path+".sav").c_str(), "r+")) != NULL)
	{
		fclose(f);
		return new FileInfo(path, 0);
	}
	else
	{
		return new FileInfo(path, errno);
	}
}

FileInfo*
FileSystem::getMapFileInfo(std::string name)
{
	std::string path = "";

	//
	// The map file extension priority vector
	//
	std::vector<std::string> mapExtensions(1, "");
	mapExtensions[0] = "s2";


	//
	// User lookup
	//
	DirectoryInfo *userDir = getUserDirectoryInfo();
	std::string dirPart = getDirectoryPart(name);
	std::string dirPath = userDir->path + "maps/" + dirPart;
	delete userDir;
	userDir = NULL;

	// Get file name
	std::vector<std::string> *dirVector = getDirectoryVector(dirPath);
	std::string fileName = "";
	if(dirVector != NULL)
	{
		fileName = getFileNameFromDirectoryVector(dirVector, mapExtensions, FileSystem::getFilePart(name));
	}
	delete dirVector;
	dirVector = NULL;
	std::cout << "MAP FILE NAME: " << fileName << std::endl;
	path = dirPath + fileName;

	// Does our file actually exist?
	if(fileName != "")
	{
		// Do we have read access to the file?
		FILE *f;
		if((f = std::fopen(path.c_str(), "r+")) != NULL)
		{
			std::fclose(f);
			return new FileInfo(path, 0);
		}
		else
		{
			return new FileInfo(path, errno);
		}
	}



	//
	// User lookup failed, do global lookup
	//
	DirectoryInfo *modDir = getModDirectoryInfo();
	dirPart = getDirectoryPart(name);
	dirPath = modDir->path + "maps/" + dirPart;
	delete modDir;
	modDir = NULL;


	// Get file name
	dirVector = getDirectoryVector(dirPath);
	std::cout << name << std::endl;
	fileName = "";
	if(dirVector != NULL)
	{
		fileName = getFileNameFromDirectoryVector(dirVector, mapExtensions, FileSystem::getFilePart(name));
	}
	std::cout << "MAP FILE NAME: " << fileName << std::endl;
	delete dirVector;
	dirVector = NULL;
	path = dirPath + fileName;
	std::cout << "MAP FILE PATH: " << path << std::endl;

	// Does our file actually exist?
	if(fileName == "")
	{
		return new FileInfo(path, ENOENT);
	}

	// Do we have read access to the file?
	FILE *f;
	if((f = std::fopen(path.c_str(), "r+")) != NULL)
	{
		std::fclose(f);
		return new FileInfo(path, 0);
	}
	else
	{
		return new FileInfo(path, errno);
	}
}



//
// GFX files
//
FileInfo*
FileSystem::getGfxFileInfo(std::string name, GfxType type)
{
	std::string path = "";

	//
	// Determine some constants based on FileType
	//
	std::string typeName;
	std::string typePath;
	const std::vector<std::string> &allowedExtensions =
		eal::Engine::get()->getGfxExtensions(type);

	switch(type)
	{
		case GFX_NONE:
			typeName = "GFX";
			typePath = "gfx/";
			break;

		case GFX_TEXTURE:
			typeName = "texture";
			typePath = "gfx/textures/";
			break;

		case GFX_MODEL:
			typeName = "model";
			typePath = "gfx/models/";
			break;

		case GFX_ICON:
			typeName = "icon";
			typePath = "gfx/icons/";
			break;

		case GFX_SPRITE:
			typeName = "sprite";
			typePath = "gfx/sprites/";
			break;

		case GFX_SKY:
			typeName = "sky";
			typePath = "gfx/skies/";
			break;
	}


	// TODO: Map specific lookup
	// If only thinking about compatibility to Stranded II, this lookup may
	// be bypassed by GfxType GFX_ICON and GFX_SPRITE. But I guess it would
	// be easier for users and developers to simply allow these in maps.


	// Global lookup
	DirectoryInfo* modDir = getModDirectoryInfo();
	std::string dirPath = modDir->path + typePath + getDirectoryPart(name);
	delete modDir;
	modDir = NULL;


	// Get file name
	std::vector<std::string> *dirVector = getDirectoryVector(dirPath);
	std::string fileName = getFileNameFromDirectoryVector(dirVector, allowedExtensions, getFilePart(name));
	delete dirVector;
	dirVector = NULL;
	std::cout << dirPath << std::endl;
	std::cout << fileName << std::endl;
	path = dirPath + fileName;
	std::cout << path << std::endl;

	// Does our file actually exist?
	if(fileName == "")
	{
		std::cerr << "WARNING: " << typeName << " " << fileName
		          << " not found" << std::endl;
		return new FileInfo(path, ENOENT);
	}

	// Do we have read access to the file?
	FILE *f;
	if((f = std::fopen(path.c_str(), "r+")) != NULL)
	{
		std::fclose(f);
		return new FileInfo(path, 0);
	}
	else
	{
		return new FileInfo(path, errno);
	}
}

FileInfo*
FileSystem::getTextureFileInfo(std::string name)
{
	return getGfxFileInfo(name, GFX_TEXTURE);
}

FileInfo*
FileSystem::getModelFileInfo(std::string name)
{
	return getGfxFileInfo(name, GFX_MODEL);
}

FileInfo*
FileSystem::getIconFileInfo(std::string name)
{
	return getGfxFileInfo(name, GFX_ICON);
}

FileInfo*
FileSystem::getSpriteFileInfo(std::string name)
{
	return getGfxFileInfo(name, GFX_SPRITE);
}

FileInfo*
FileSystem::getSkyFileInfo(std::string name)
{
	return getGfxFileInfo(name, GFX_SKY);
}



//
// SFX files
//
FileInfo*
FileSystem::getSfxFileInfo(std::string name, SfxType type)
{
	std::string path = "";

	//
	// Determine some constants based on FileType
	//
	std::string typeName;
	std::string typePath;
	const std::vector<std::string> &allowedExtensions =
		eal::Engine::get()->getSfxExtensions(type);

	switch(type)
	{
		// SFX files
		case SFX_NONE:
			typeName = "SFX";
			typePath = "sfx/";
			break;

		case SFX_EFFECT:
			typeName = "effect";
			typePath = "sfx/effects/";
			break;

		case SFX_SPEECH:
			typeName = "speech";
			typePath = "sfx/speech/";
			break;
	}


	// TODO: Map specific lookup
	// Even without completion of this Todo this is compatible to Stranded
	// II.


	// Global lookup
	DirectoryInfo* modDir = getModDirectoryInfo();
	std::string dirPath = modDir->path + typePath + getDirectoryPart(name);
	delete modDir;
	modDir = NULL;


	// Get file name
	std::vector<std::string> *dirVector = getDirectoryVector(dirPath);
	std::string fileName = getFileNameFromDirectoryVector(dirVector, allowedExtensions, getFilePart(name));
	delete dirVector;
	dirVector = NULL;
	path = dirPath + fileName;

	// Does our file actually exist?
	if(fileName == "")
	{
		std::cerr << "WARNING: " << typeName << " " << fileName
		          << " not found" << std::endl;
		return new FileInfo(path, ENOENT);
	}

	// Do we have read access to the file?
	FILE *f;
	if((f = std::fopen(path.c_str(), "r+")) != NULL)
	{
		std::fclose(f);
		return new FileInfo(path, 0);
	}
	else
	{
		return new FileInfo(path, errno);
	}
}

FileInfo*
FileSystem::getSpeechFileInfo(std::string name)
{
	return getSfxFileInfo(name, SFX_SPEECH);
}

FileInfo*
FileSystem::getEffectFileInfo(std::string name)
{
	return getSfxFileInfo(name, SFX_EFFECT);
}



//
// Class DirectoryInfo
//
DirectoryInfo::DirectoryInfo(std::string path)
	: path(path)
{ }



//
// Class FileInfo
//
FileInfo::FileInfo(std::string path, int error)
	: path(path), error(error)
{ }

bool
FileInfo::exists()
{
	return this->error != ENOENT;
}

