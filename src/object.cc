/*
 * This file includes the implementation of the object specific classes.
 *
 * Copyright (C) 2008 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "object.hh"
#include "kingdom.hh"

/*
 * These are the declarations of the ObjectEntity class
 */

ObjectEntity::ObjectEntity (ID type):
	Entity(type)
{
	Type *typedata = Kingdom::kingdomList[S2_OBJECT]->getType(type);
	load(typedata);
}


/*
 * These are the declarations of the ObjectType class
 */

ObjectType::ObjectType (const char* name):
	Type(name)
{}


/*
 * Declarations of the ObjectKingdom
 */

ObjectKingdom::ObjectKingdom ():
	Kingdom ("object")
{}


Entity*
ObjectKingdom::createEntity (ID type)
{
	return new ObjectEntity (type);
}


Type*
ObjectKingdom::createType (const char* name)
{
	return new ObjectType (name);
}
