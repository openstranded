/*
 * This file includes the base class definition for individual entities
 *
 * Copyright (C) 2008 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_ENTITY_HH
#define STRANDED_ENTITY_HH

#include "type.hh"
#include <string>
#include "eal/eal.hh"

typedef unsigned int ID;


/**
 * An entity is a single, independent piece on the map.
 * It belongs to a "kingdom" which defines its most basic properties
 * (e.g. collectable, controllable...) and a "type" which is
 * a kind of prototype for differerent entities defining whether
 * the entity is e.g. a wooden hut, or a bamboo hut, or a tree or
 * whatever
 */
class Entity
{
	private:
		/**
		 * The ID of the type as assigned in the constructor.
		 */
		ID typeID;


		/**
		 * A graphical object
		 */
		eal::SceneNode *scenenode;

		eal::Vector3 position;

	public:
		/**
		 * The entity's constructor.
		 * @param type The ID of the type this entity belongs to
		 */
		Entity (ID type);


		/**
		 * The destructor.
		 */
		virtual
		~Entity () {};

		/**
		 * Loads non-type-specific data from the type information
		 */
		void
		load(Type *type);

		/**
		 * Change the position of this object
		 */
		virtual void
		setPosition (eal::Vector3 position);

		/**
		 * Returns the current position of the object
		 */
		virtual eal::Vector3
		getPosition ();
};

#endif /* STRANDED_ENTITY_HH */
