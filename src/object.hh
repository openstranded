/*
 * This file includes the definitions for the object specific classes
 * derived from the Kingdom, Type and Entity base classes.
 *
 * Copyright (C) 2008 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_OBJECT_HH
#define STRANDED_OBJECT_HH

#include "kingdom.hh"
#include "type.hh"
#include "entity.hh"

#define S2_OBJECT 1

/**
 * This is the class of all entities which belong to the kingdom
 * "object", derived from the type base class.
 * This class represents an object: an entity which doesn't move,
 * except on script actions. In addition, the player's action with
 * objects are mostly limited to damaging and destroying them 
 * (without scripts).
 */
class ObjectEntity: public Entity
{
	public:
		/**
		 * The constructor.
		 * @param The ID of the ObjectType this entity belongs to
		 * @see Entity::Entity()
		 */
		ObjectEntity (ID type);
};


/**
 * This is the class of the object types, derived from the type base
 * class.
 * The single ObjectType instances define the default values for new
 * created object entities as given in the definition files.
 */
class ObjectType: public Type
{
	public:
		/**
		 * The constructor.
		 * @param name The name of this type (used in editor)
		 * @see Type::Type()
		 */
		ObjectType (const char* name);

};


/**
 * Object kingdom class
 */
class ObjectKingdom: public Kingdom
{
	public:
		/**
		 * The constructor.
		 */
		ObjectKingdom ();



		//
		// Virtual methods
		//

		/**
		 * Creates a new ObjectEntity object.
		 * @param type The ID of the type which the new object should be
		 * @see ObjectEntity::ObjectEntity()
		 */
		Entity*
		createEntity (ID type);

		/**
		 * Creates a new ObjectType object.
		 * @param name The name of this type
		 * @see ObjectType::ObjectType()
		 */
		Type*
		createType (const char* name);
};

#endif /* STRANDED_OBJECT_HH */
