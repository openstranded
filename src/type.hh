/*
 * This file includes the base class definition for types.
 * An entity's conduct is very precisely defined by its assigned type.
 *
 * Copyright (C) 2008 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_TYPE_HH
#define STRANDED_TYPE_HH

#include <string>

typedef unsigned int ID;


/**
 * Types are the prototypes of the single entities.
 * There is one type class per kingdom.
 * The single type instances include the information about the types
 * as read from the definition files.
 */
class Type
{
	protected:
		std::string name;
		float scaleX, scaleY, scaleZ;
		uint8_t colorR, colorG, colorB;
		std::string model;
		std::string iconPath;
		float fadingDistance;
		float alpha;
		float shine;

	public:

		/**
		 * The constructor.
		 * @param name The name of this type (used only in editor)
		 */
		Type (const char *name);

		/**
		 * The destructor.
		 */
		virtual
		~Type () {};



		//
		// Name
		//
		/**
		 * Set the type name.
		 * @param name The new name of the type
		 */
		inline void
		setName (const std::string &name) { this->name = name; }

		/**
		 * Get the type name.
		 * @return The current name of the type
		 */
		inline std::string
		getName () { return this->name; }



		//
		// Model path
		//

		/**
		 * Set the model.
		 * The model is specified by the path to the model file.
		 * @param path Path to the new model file 
		 */
		inline void
		setModel (const std::string &path) { this->model = path; }

		/**
		 * Return the path to the current model.
		 * @return Path of the current model file
		 */
		inline std::string
		getModel () { return this->model; }



		//
		// Icon path
		//

		/**
		 * Set the path to the icon used.
		 * The icon is the little thumbnail in the editor.
		 * @param path Path to the new icon file
		 */
		inline void
		setIconPath (const std::string &path) { this->iconPath = path; }

		/**
		 * Return the current icon path.
		 * @return The path to the current icon file
		 */
		inline std::string
		getIconPath () { return this->iconPath; }



		//
		// Scaling values
		//

		/**
		 * Set the X scaling value.
		 * The scaling values are divided into their coordinate
		 * components.
		 * @param scaleX The new X scaling value
		 */
		inline void
		setScaleX (const float scaleX) { this->scaleX = scaleX; }

		/**
		 * Get the X scaling value.
		 * @return The current X scaling value
		 */
		inline float
		getScaleX () { return this->scaleX; }

		/**
		 * Set the Y scaling value.
		 * @param scaleY The new Y scaling value
		 */
		inline void
		setScaleY (const float scaleY) { this->scaleY = scaleY; }

		/**
		 * Get the Y scaling value.
		 * @return The current Y scaling value
		 */
		inline float
		getScaleY () { return this->scaleY; }

		/**
		 * Set the Z scaling value.
		 * @param scaleZ The new Z scaling value
		 */
		inline void
		setScaleZ (const float scaleZ) { this->scaleZ = scaleZ; }

		/**
		 * Get the Z scaling value.
		 * @return The current Z scaling value
		 */
		inline float
		getScaleZ () { return this->scaleZ; }



		//
		// Color values
		//

		/**
		 * Set the red value.
		 * @param colorR The new red value ranging from 0 to 255
		 */
		inline void
		setColorR (const uint8_t colorR) { this->colorR = colorR; }

		/**
		 * Get the red value.
		 * @return The current red value ranging from 0 to 255
		 */
		inline uint8_t
		getColorR () { return this->colorR; }

		/**
		 * Set the green value.
		 * @param colorG The new green value ranging from 0 to 255
		 */
		inline void
		setColorG (const uint8_t colorG) { this->colorG = colorG; }

		/**
		 * Get the green value
		 * @return The current green value ranging from 0 to 255
		 */
		inline uint8_t
		getColorG () { return this->colorG; }

		/**
		 * Set the blue value
		 * @param colorB The new blue value ranging from 0 to 255
		 */
		inline void
		setColorB (const uint8_t colorB) { this->colorB = colorB; }

		/**
		 * Get the blue value
		 * @return The current blue value ranging from 0 to 255
		 */
		inline uint8_t
		getColorB () { return this->colorB; }



		//
		// Autofading
		//

		/**
		 * Set the Autofading distance.
		 * This is the distance to the player when an object fades out of sight
		 * @param fadingDistance The new Autofading distance
		 */
		inline void
		setFadingDistance (const float fadingDistance) { this->fadingDistance = fadingDistance; }

		/**
		 * Get the Autofading distance
		 * @return The current Autofading distance
		 */
		inline float
		getFadingDistance () { return this->fadingDistance; }



		//
		// Alpha
		//

		/**
		 * Set the value of transparency (aka alpha channel).
		 * @param alpha The new alpha value ranging from 0.0 (invisible) to 1.0 (opaque)
		 */
		inline void
		setAlpha (const float alpha) { this->alpha = alpha; }

		/**
		 * Returns the transparency value.
		 * @return The current alpha value
		 */
		inline float
		getAlpha () { return this->alpha; }



		//
		// Shine effect
		//

		/**
		 * Set the shine value of the model
		 * @param shine The new shine value
		 */
		inline void
		setShine (const float shine) { this->shine = shine; }

		/**
		 * Get the shine value of the model
		 * @return The current shine value
		 */
		inline float
		getShine () { return this->shine; }
};

#endif /* STRANDED_TYPE_HH */
