/*
 *This file includes the basic classes for map/savegame loading
 *
 * Copyright (C) 2008 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "map.hh"
#include "bbfile.hh"
#include "kingdom.hh"
#include "object.hh"
#include "eal/eal.hh"
#include "eal/engine.hh"

#include <iostream>

Map::Map ()
{
	environment = eal::Engine::get()->addEnvironment();
}
Map::~Map ()
{
}

bool
Map::load (std::string path)
{
	// Open file
	BBFile mapfile(path);
	if (!mapfile)
	{
		std::cerr << "Map file couldn't be opened: " << path << std::endl;
		return false;
	}

	// General map info
	std::string format = mapfile.readLine();
	std::string version = mapfile.readLine();
	std::string date = mapfile.readLine();
	std::string time = mapfile.readLine();
	format = mapfile.readLine();
	std::string mode = mapfile.readLine();

	std::string typeformat = mapfile.readLine();
	int objectformat = 0;
	int unitformat = 0;
	int infoformat = 0;
	if (typeformat.size() >= 3)
	{
		objectformat = typeformat[0] - '0';
		unitformat = typeformat[1] - '0';
		infoformat = typeformat[2] - '0';
	}

	mapfile.readLine();
	mapfile.readLine();
	mapfile.readLine();
	mapfile.readLine();
	mapfile.readLine();

	// Map image
	uint8_t mapimage[96 * 72 * 3];
	mapfile.readData(mapimage, 96 * 72 * 3);

	// Password
	// TODO
	mapfile.readByte();
	mapfile.readLine();

	// Environment settings
	int day = mapfile.readInt();
	int hour = mapfile.readByte();
	int minute = mapfile.readByte();
	int freezetime = mapfile.readByte();
	std::string skybox = mapfile.readString();
	int multiplayer = mapfile.readByte();
	int climate = mapfile.readByte();
	std::string music = mapfile.readString();
	std::string briefing = mapfile.readString();
	int fogr = mapfile.readByte();
	int fogg = mapfile.readByte();
	int fogb = mapfile.readByte();
	int fogmode = mapfile.readByte();

	mapfile.readByte();

	// Quick slots
	for (int i = 0; i < 10; i++)
		mapfile.readString();

	// Color map
	int colormapsize = mapfile.readInt();
	printf("Color map size: %d\n", colormapsize);
	uint8_t *colormap = new uint8_t[colormapsize * colormapsize * 3];
	mapfile.readData(colormap, colormapsize * colormapsize * 3);
	// Heightmap
	int heightmapsize = mapfile.readInt();
	printf("Height map size: %d\n", heightmapsize);
	float *heightmap = new float[(heightmapsize + 1) * (heightmapsize + 1)];
	mapfile.readData(heightmap, (heightmapsize + 1) * (heightmapsize + 1) * sizeof(float));
	// Grass map
	uint8_t *grassmap = new uint8_t[(colormapsize + 1) * (colormapsize + 1)];
	mapfile.readData(grassmap, (colormapsize + 1) * (colormapsize + 1));

	// Create terrain
	environment->setSize(heightmapsize);
	environment->setHeightData(heightmap);
	environment->setColorMap(colormap, colormapsize);
	environment->setSkyCube(skybox);
	environment->setFog(eal::Color(fogr, fogg, fogb));
	// TODO: Grass

	// Entities
	// Load objects
	int objectcount = mapfile.readInt();
	for (int i = 0; i < objectcount; i++)
	{
		ID id = mapfile.readInt();
		ID type = 0;
		if (objectformat)
		{
			type = mapfile.readShort();
		}
		else
		{
			type = mapfile.readByte();
		}
		float x = mapfile.readFloat();
		float z = mapfile.readFloat();
		float yaw = mapfile.readFloat();
		float health = mapfile.readFloat();
		float maxhealth = mapfile.readFloat();
		int daytimer = mapfile.readInt();

		// Create object
		Entity *newobject = Kingdom::kingdomList[S2_OBJECT]->insertEntity(type, id);
		newobject->setPosition(eal::Vector3(x, environment->getHeight(eal::Vector2(x, z)), z));
	}
	printf("%d objects.\n", objectcount);
	// TODO: Load other entity types

	return false;
}
void
Map::destroy ()
{
}
