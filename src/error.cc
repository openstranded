#include <iostream>
#include <string>
#include "output.hh"
#include "error.hh"

StrandedException::StrandedException (const char* m)
{
	this->message = std::string (m);
}

StrandedException::StrandedException (const std::string& m)
{
	this->message = m;
}

StrandedException::~StrandedException () throw ()
{}

const char*
StrandedException::what () throw ()
{
	return message.c_str ();
}

void
StrandedException::print ()
{
	Out::msg << "StrandedException: " << this->what () << Out::endl;
}


/*
 * Derived constructors
 */

StrandedWarning::StrandedWarning (const char* m):
StrandedException (m) {}

StrandedWarning::StrandedWarning (const std::string& m):
StrandedException (m) {}

StrandedError::StrandedError (const char* m):
StrandedException (m) {}

StrandedError::StrandedError (const std::string& m):
StrandedException (m) {}

StrandedFatal::StrandedFatal (const char* m):
StrandedException (m) {}

StrandedFatal::StrandedFatal (const std::string& m):
StrandedException (m) {}

/*
 * Derived methods
 */

void
StrandedWarning::print ()
{
	Out::warning << this->what () << Out::endl;
}

void
StrandedError::print ()
{
	Out::error << this->what () << Out::endl;
}

void
StrandedFatal::print ()
{
	Out::msg << this->what () << Out::endl;
}
