
#include "eal/engine.hh"
#include "settings.hh"
#include "kingdom.hh"
#include "map.hh"
#include "output.hh"
#include "filesystem.hh"
#ifdef HAVE_IRRLICHT
#include "eal/irrlicht/engine.hh"
#endif
#ifdef HAVE_HORDE3D
#include "eal/horde3d/engine.hh"
#endif

#include <iostream>

int
main (int argc, char **argv)
{
	// Get mod
	std::string mod = "standard";
	if (argc > 1)
	{
		mod = argv[1];
	}
	FileSystem::setModName(mod);

	// Setup core services
	Out::setConsoleFlags(Out::FATAL | Out::ERROR | Out::WARNING | Out::DEBUG);
	Out::setLoggingFlags(Out::NONE);
	Out::setLogFile("openstranded.log");

	GlobalSettings settings("settings.cfg");
	settings.readConfigFile();
	settings.setModName(mod);
	try
	{
		// Start engine
		std::string renderer = settings.getRenderer();
		#if !defined(HAVE_IRRLICHT) && defined(HAVE_HORDE3D)
		renderer = "Horde3D";
		#endif
		#if defined(HAVE_IRRLICHT) && !defined(HAVE_HORDE3D)
		renderer = "Irrlicht";
		#endif
		if ((renderer != "Irrlicht") && (renderer != "Horde3D"))
			renderer = "Irrlicht";
		eal::Engine *engine = 0;
		if (renderer == "Horde3D")
		{
			#ifdef HAVE_HORDE3D
			engine = new eal::Horde3DEngine;
			#endif
		}
		else if (renderer == "Irrlicht")
		{
			#ifdef HAVE_IRRLICHT
			engine = new eal::IrrlichtEngine;
			#endif
		}
		if (!engine)
			throw std::string("No renderer available!");

		Kingdom::initKingdomList(mod);		

		engine->init(&settings);
		// Load map
		FileInfo *mapFileInfo = FileSystem::getMapFileInfo("adventure/map02");
		std::string filename = mapFileInfo->path;
		delete mapFileInfo;
		Map map;
		map.load(filename);
		while (true)
		{
			if (!eal::Engine::get()->update()) break;
		}
	}
	catch (std::string e)
	{
		std::cerr << "Error: " << e << "\n";
		return -1;
	}
	catch (...)
	{
		std::cerr << "Error: Unknown exception.\n";
		return -1;
	}
	Kingdom::uninitKingdomList();
	return 0;
}
