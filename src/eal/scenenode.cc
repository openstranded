/*
 * This file defines SceneNodes, the representants of entities in the
 * world.
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/scenenode.hh"
#include "eal/engine.hh"
#include "eal/irrlicht/engine.hh"

#include <irrlicht/irrlicht.h>
using namespace irr;

namespace eal
{
	SceneNode::SceneNode ()
	{
	}
	SceneNode::~SceneNode ()
	{
	}
}
