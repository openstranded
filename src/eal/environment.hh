/*
 * Terrain, weather, lights
 *
 * Copyright (C) 2009 David Kolossa, Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_ENVIRONMENT_HH
#define STRANDED_ENVIRONMENT_HH

#include "eal/vector.hh"
#include "eal/color.hh"

namespace eal
{
	/**
	 * This class represents space and the ground
	 */
	class Environment
	{
		public:
			Environment();
			virtual ~Environment();

			/**
			 * Sets the size of the terrain. Terrains always have the same size
			 * on the x and z axis.
			 */
			virtual void
			setSize (int size) = 0;

			/**
			 * Returns the size of the terrain.
			 * @see Environment::setSize()
			 */
			virtual int
			getSize () = 0;

			/**
			 * Sets the height of the terrain on a certain position
			 */
			virtual void
			setHeight (const Vector2 &pos, double height) = 0;

			/**
			 * Get the terrain height on a certain position
			 */
			virtual double
			getHeight (const Vector2 &pos) = 0;

			/**
			 * Updates the terrain data.
			 * @param heightdata Array to the height data ((size+1)*(size+1) floats)
			 */
			virtual void
			setHeightData(float *heightdata) = 0;

			/**
			 * Sets the color of the terrain on a certain position
			 */
			virtual void
			setColor (const Vector2 &pos, const Color &color) = 0;

			/**
			 * Get the terrain color on a certain position
			 */
			virtual Color
			getColor (const Vector2 &pos) = 0;

			/**
			 * Sets the color map of the terrain using raw input data.
			 * @param colormap Array with 24bit packed color data.
			 * @param size Size of the square
			 */
			virtual void
			setColorMap (unsigned char *colormap, int size) = 0;
			/**
			 * Sets the color map of the terrain using a texture file.
			 * @param path Path to the color texture
			 */
			virtual void
			setColorMap (std::string path) = 0;

			/**
			 * Sets the sky cube texture.
			 * @param sky Name of the sky cube. The textures are loaded from
			 * skies/<sky>_xy.jpg automatically.
			 */
			virtual void
			setSkyCube(std::string sky) = 0;

			/**
			 * Sets the color of the fog
			 */
			virtual void
			setFog(const Color &fogcolor) = 0;

			/*
			 * Planned methods:
			 * Set a light source on a certain position
			 * Change properties of light source
			 * Change color of terrain on certain position
			 */
	};
}

#endif
