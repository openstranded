/*
 * Simple camera scene node
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_CAMERA_HH
#define STRANDED_CAMERA_HH

#include "eal/vector.hh"

namespace eal
{
	/*
	 * This class represents the camera (point of view)
	 */
	class Camera
	{
		public:
			Camera();
			virtual ~Camera();

			/*
			 * Move the camera to a certain position
			 */
			virtual void
			setPosition(const Vector3 &pos) = 0;

			/*
			 * Returns the camera position
			 */
			virtual Vector3
			getPosition() = 0;

			/*
			 * Set yaw, roll and pitch angles
			 */
			virtual void
			setAngles(const Vector3 &angles) = 0;

			/*
			 * Get angle values
			 */
			virtual Vector3
			getAngles() = 0;
	};
}

#endif
