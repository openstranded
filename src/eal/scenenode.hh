/*
 * This file defines SceneNodes, the representants of entities in the
 * world.
 *
 * Copyright (C) 2009 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_SCENENODE_HH
#define STRANDED_SCENENODE_HH

#include <string>
#include <stdint.h>

#include "vector.hh"
#include "texture.hh"
#include "color.hh"

namespace eal
{
	/**
	 * This class represents an entity from one of the several
	 * kingdoms.
	 */
	class SceneNode
	{
		public:
			SceneNode();
			virtual ~SceneNode();

			/**
			 * Returns the model used for this object
			 */
			virtual std::string
			getModel() = 0;

			/**
			 * Change the model of the object to the given Model
			 */
			virtual void
			setModel(std::string model) = 0;

			/**
			 * Returns the texture for this object
			 */
			virtual Texture&
			getTexture() = 0;

			/**
			 * Change the texture of this object
			 */
			virtual void
			setTexture(Texture &texture) = 0;

			/**
			 * Returns the color of the object
			 */
			virtual Color
			getColor() = 0;

			/**
			 * Set the color of the object
			 */
			virtual void
			setColor(const Color &color) = 0;

			/**
			 * Change the position of this object
			 */
			virtual void
			setPosition(const Vector3 &position) = 0;

			/**
			 * Returns the current position of the object
			 */
			virtual Vector3
			getPosition() = 0;

			/**
			 * Change the rotation angles of the object
			 */
			virtual void
			setRotation(const RotationVector &rotation) = 0;

			/**
			 * Return the rotation angles
			 */
			virtual RotationVector
			getRotation() = 0;

			/**
			 * Play an animation
			 * Takes startframe, endframe, animation speed and animation
			 * mode as arguments
			 * mode values are as follows:
			 * 0 - stop animation
			 * 1 - loop animation 
			 * 2 - when done playing: reverse animation, repeat (ping-pong)
			 * 3 - plays animation only once (one-shot)
			 */
			virtual void
			playAnimation(int start, int end, double speed, int mode) = 0;
	};
}


#endif /* STRANDED_SCENENODE_HH */

