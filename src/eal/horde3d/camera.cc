/*
 * Horde3D camera scene node
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/horde3d/camera.hh"

#include <horde3d/Horde3D.h>
#include <cmath>

namespace eal
{
	static inline float degToRad( float f )
	{
		return f * (3.1415926f / 180.0f);
	}

	Horde3DCamera::Horde3DCamera(int camera)
	{
		this->camera = camera;
		Horde3D::setNodeParamf(camera, CameraNodeParams::FarPlane, 3000.0f);
		rotation[0] = 0;
		rotation[1] = -45;
		position[0] = 0;
		position[1] = 500;
		position[2] = 500;
		sun = 0;
	}
	Horde3DCamera::~Horde3DCamera()
	{
	}

	void
	Horde3DCamera::setPosition(const Vector3 &pos)
	{
		
	}

	Vector3
	Horde3DCamera::getPosition()
	{

	}

	void
	Horde3DCamera::setAngles(const Vector3 &angles)
	{
		
	}

	Vector3
	Horde3DCamera::getAngles()
	{

	}

	void Horde3DCamera::update(float mx, float my, bool w, bool a, bool s, bool d)
	{
		rotation[0] -= mx / 2;
		rotation[1] -= my / 2;
		Horde3D::setNodeTransform(camera, position[0], position[1], position[2], rotation[1], rotation[0], 0, 1, 1, 1);

		float curVel = 2.0f;

		//if( _keys[287] ) curVel *= 5;	// LShift

		if (w)
		{
			// Move forward
			position[0] -= sinf( degToRad( rotation[0] ) ) * cosf( -degToRad( rotation[1] ) ) * curVel;
			position[1] -= sinf( -degToRad( rotation[1] ) ) * curVel;
			position[2] -= cosf( degToRad( rotation[0] ) ) * cosf( -degToRad( rotation[1] ) ) * curVel;
		}

		if (s)
		{
			// Move backward
			position[0] += sinf( degToRad( rotation[0] ) ) * cosf( -degToRad( rotation[1] ) ) * curVel;
			position[1] += sinf( -degToRad( rotation[1] ) ) * curVel;
			position[2] += cosf( degToRad( rotation[0] ) ) * cosf( -degToRad( rotation[1] ) ) * curVel;
		}

		if (a)
		{
			// Strafe left
			position[0] += sinf( degToRad( rotation[0] - 90) ) * curVel;
			position[2] += cosf( degToRad( rotation[0] - 90 ) ) * curVel;
		}

		if (d)
		{
			// Strafe right
			position[0] += sinf( degToRad( rotation[0] + 90 ) ) * curVel;
			position[2] += cosf( degToRad( rotation[0] + 90 ) ) * curVel;
		}

		// Set sun position
		float camx, camz;
		Horde3D::getNodeTransform(camera, &camx, 0, &camz, 0, 0, 0, 0, 0, 0);
		Horde3D::setNodeTransform(sun, camx, 2000, camz, -90, 0, 0, 1, 1, 1);
	}
}
