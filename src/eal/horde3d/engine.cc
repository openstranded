/*
 * Horde3D implementation of the rendering system
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/horde3d/engine.hh"
#include "eal/horde3d/camera.hh"
#include "eal/horde3d/scenenode.hh"
#include "eal/horde3d/environment.hh"
#include "eal/horde3d/extensions/extensions.hh"
#include "output.hh"
#include "settings.hh"
#include "filesystem.hh"

#include <horde3d/Horde3D.h>
#include <horde3d/Horde3DUtils.h>
#include <GL/glfw.h>

namespace eal
{
	static int mousex = 0;
	static int mousey = 0;
	static int mousedx = 0;
	static int mousedy = 0;
	static bool keys[300] = {false};
	static float debugview = 0.0f;

	static void GLFWCALL mouseMoveListener(int x, int y)
	{
		mousedx += x - mousex;
		mousedy += y - mousey;
		mousex = x;
		mousey = y;
	}
	static void GLFWCALL keyPressListener(int key, int action)
	{
		// Save key state
		if (key > 300) return;
		if (action == GLFW_PRESS)
		{
			keys[key] = true;
			if (key == 'Q')
			{
				debugview = 1.0f - debugview;
				Horde3D::setOption(EngineOptions::DebugViewMode, debugview);
			}
		}
		else
		{
			keys[key] = false;
		}
	}

	Horde3DEngine::Horde3DEngine() : Engine()
	{
		instance = this;
		converter = 0;
	}
	Horde3DEngine::~Horde3DEngine()
	{
		Horde3D::release();
		glfwTerminate();
	}

	bool
	Horde3DEngine::init(GlobalSettings *settings)
	{
		this->settings = settings;
		searchpath = std::string("mods/") + settings->getModName() + "/extra/horde3d|";
		searchpath += std::string("mods/") + settings->getModName() + "/sys/gfx|";
		searchpath += "extra/horde3d";
		// Load settings
		int width = 1024;
		int height = 768;
		bool fullscreen = false;
		if (settings)
		{
			width = settings->getResolutionX();
			height = settings->getResolutionY();
			fullscreen = settings->getFullScreen();
		}
		// Create window
		glfwInit();
		if(!glfwOpenWindow(width, height, 8, 8, 8, 8, 24, 8, fullscreen?GLFW_FULLSCREEN:GLFW_WINDOW))
		{
			Out::fatal << "Could not open window." << Out::endl;
			glfwTerminate();
			return false;
		}
		glfwSetWindowTitle("OpenStranded");
		glfwSetMousePosCallback(mouseMoveListener);
		glfwSetKeyCallback(keyPressListener);
		glfwDisable(GLFW_MOUSE_CURSOR);
		glfwSwapInterval(0);
		// Initialize Horde3D
		if (!Horde3D::init())
		{
			Out::fatal << "Could not initialize Horde3D!\n" << Out::endl;
			glfwTerminate();
			return false;
		}
		horde3d::installExtensions();
		Horde3D::setupViewport(0, 0, width, height, true);
		converter = new Horde3DConverter(std::string("mods/") + settings->getModName());
		// Load pipeline and create camera
		ResHandle pipeline = Horde3D::addResource(ResourceTypes::Pipeline,"pipelines/forward.pipeline.xml", 0);
		panelmat = Horde3D::addResource( ResourceTypes::Material, "overlays/panel.material.xml", 0 );
		fontmat = Horde3D::addResource( ResourceTypes::Material, "overlays/font.material.xml", 0 );
		loadResources();
		cameranode = Horde3D::addCameraNode( RootNode, "camera", pipeline);
		camera = new Horde3DCamera(cameranode);

		// Debug output
		Horde3DUtils::dumpMessages();
		return true;
	}

	SceneNode*
	Horde3DEngine::addSceneNode()
	{
		return new Horde3DSceneNode;
	}
	Environment*
	Horde3DEngine::addEnvironment()
	{
		return new Horde3DEnvironment;
	}

	void
	Horde3DEngine::loadResources()
	{
		Horde3DUtils::loadResourcesFromDisk(searchpath.c_str());
	}

	bool
	Horde3DEngine::update()
	{
		((Horde3DCamera*)camera)->update(mousedx, mousedy, keys[GLFW_KEY_UP],
			keys[GLFW_KEY_LEFT], keys[GLFW_KEY_DOWN], keys[GLFW_KEY_RIGHT]);
		mousedx = 0;
		mousedy = 0;
		Horde3DEnvironment::updateWater(glfwGetTime());
		Horde3DUtils::showFrameStats(fontmat, panelmat, 2);
		Horde3D::render(cameranode);
		Horde3D::finalizeFrame();
		Horde3D::clearOverlays();
		glfwSwapBuffers();
		Horde3DUtils::dumpMessages();
		return glfwGetWindowParam(GLFW_OPENED);
	}

	const std::vector<std::string>&
	Horde3DEngine::getGfxExtensions(GfxType type)
	{
		// FIXME: The extension stuff is more complicated than on Irrlicht
		static std::vector<std::string> v;
		return v;
	}

	const std::vector<std::string>&
	Horde3DEngine::getSfxExtensions(SfxType type)
	{
		// FIXME: Ommited extensions for horde
		static std::vector<std::string> v;
		return v;
	}
}
