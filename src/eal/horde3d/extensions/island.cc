/*
 * Island scene node for Horde3D
 *
 * Copyright (C) 2009 Mathias Gottschlag, Nicolas Schulz, Volker Wiendl
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/horde3d/extensions/island.hh"
#include "egModules.h"
#include "egMaterial.h"
#include "utOpenGL.h"

namespace eal
{
	namespace horde3d
	{
		const char *vsIslandDebugView =
			"uniform mat4 worldMat;\n"
			"varying vec4 color;\n"
			"void main() {\n"
			"	color = gl_Color;\n"
			"	gl_Position = gl_ModelViewProjectionMatrix * worldMat * gl_Vertex;\n"
			"}";

		const char *fsIslandDebugView =
			"varying vec4 color;\n"
			"void main() {\n"
			"	gl_FragColor = color;\n"
			"}\n";

		ShaderCombination IslandNode::debugViewShader;

		IslandNode::~IslandNode()
		{
		}

		SceneNodeTpl *IslandNode::parsingFunc(std::map< std::string, std::string > &attribs)
		{
			// TODO
		}
		SceneNode *IslandNode::factoryFunc(const SceneNodeTpl &nodeTpl)
		{
			if( nodeTpl.type != SNT_IslandNode ) return 0x0;

			return new IslandNode( *(IslandNodeTpl *)&nodeTpl );
		}
		void IslandNode::renderFunc(const std::string &shaderContext, const std::string &theClass, bool debugView,
			const Frustum *frust1, const Frustum *frust2, RenderingOrder::List order, int occSet)
		{
			CameraNode *curCam = Modules::renderer().getCurCamera();
			if( curCam == 0x0 ) return;

			Modules::renderer().setMaterial( 0x0, "" );

			// Loop through island queue
			for( uint32 i = 0, s = (uint32)Modules::sceneMan().getRenderableQueue().size(); i < s; ++i )
			{
				if( Modules::sceneMan().getRenderableQueue()[i].type != SNT_IslandNode )
				{
					continue;
				}

				IslandNode *island = (IslandNode*)Modules::sceneMan().getRenderableQueue()[i].node;

				if (!debugView)
				{
					if (!island->materialRes->isOfClass(theClass)) continue;
					if (!Modules::renderer().setMaterial(island->materialRes, shaderContext)) continue;
				}
				else
				{
					Modules::renderer().setShader(&debugViewShader);
				}

				int terrainSize = glGetUniformLocation( Modules::renderer().getCurShader()->shaderObject, "terrainSize" );

				Vec3f localCamPos( curCam->getAbsTrans().x[12], curCam->getAbsTrans().x[13], curCam->getAbsTrans().x[14] );
				localCamPos = island->_absTrans.inverted() * localCamPos;
				// Draw terrain
				// TODO
				for (int i = 0; i < TILE_COUNT; i++)
				{
					if (island->terrainpatches[i] != 0)
					{
						glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, island->indexBuffer[0]);
						glBindBuffer(GL_ARRAY_BUFFER, island->terrainpatches[i]);
						glVertexPointer(3, GL_FLOAT, sizeof(float) * 3, (char*)0);
						glNormalPointer(GL_FLOAT, sizeof(float) * 3,
							(char*)((TILE_SIZE + 1) * (TILE_SIZE + 1) * sizeof(float) * 3));
						glEnableClientState(GL_VERTEX_ARRAY);
						glEnableClientState(GL_NORMAL_ARRAY);

						// World transformation
						ShaderCombination *curShader = Modules::renderer().getCurShader();
						if (curShader->uni_worldMat >= 0)
						{
							glUniformMatrix4fv(curShader->uni_worldMat, 1, false, &island->_absTrans.x[0]);
						}
						if (curShader->uni_worldNormalMat >= 0)
						{
							Matrix4f normalMat4 = island->_absTrans.inverted().transposed();
							float normalMat[9] = { normalMat4.x[0], normalMat4.x[1], normalMat4.x[2],
												normalMat4.x[4], normalMat4.x[5], normalMat4.x[6],
												normalMat4.x[8], normalMat4.x[9], normalMat4.x[10] };
							glUniformMatrix3fv(curShader->uni_worldNormalMat, 1, false, normalMat );
						}

						if( terrainSize >= 0 )
							glUniform4f( terrainSize, island->sizeX, island->sizeZ, 0, 0 );

						glDrawElements(GL_TRIANGLES, TILE_SIZE * TILE_SIZE * 6, GL_UNSIGNED_SHORT, (char *)0);
						Modules::stats().incStat(EngineStats::BatchCount, 1);
						Modules::stats().incStat(EngineStats::TriCount, TILE_SIZE * TILE_SIZE * 2);

						glDisableClientState(GL_VERTEX_ARRAY);
						glDisableClientState(GL_NORMAL_ARRAY);
					}
				}
			}
		}

		bool IslandNode::canAttach(SceneNode &parent)
		{
			return true;
		}
		int IslandNode::getParami(int param)
		{
			switch( param )
			{
			case IslandNodeParams::MaterialRes:
				if( materialRes != 0x0 ) return materialRes->getHandle();
				else return 0;
			case IslandNodeParams::SizeX:
				return sizeX;
			case IslandNodeParams::SizeZ:
				return sizeZ;
			default:
				return SceneNode::getParami( param );
			}
		}
		bool IslandNode::setParami(int param, int value)
		{
			Resource *res;
			switch( param )
			{
			case IslandNodeParams::MaterialRes:
				res = Modules::resMan().resolveResHandle( value );
				if (res == 0x0 || res->getType() != ResourceTypes::Material)
				{
					Modules::log().writeDebugInfo( "Invalid Material resource for Island node %i", _handle );
					return false;
				}
				materialRes = (MaterialResource*)res;
				return true;
			case IslandNodeParams::SizeX:
				if ((value <= 0) || (value > 512))
				{
					Modules::log().writeDebugInfo( "Invalid size for Terrain node %i (must be 2^x + 1)", _handle );
					return false;
				}

				if (sizeX == value)
					return true;

				sizeX = value;
				// TODO: Recreate terrain
				return true;
			case IslandNodeParams::SizeZ:
				if ((value <= 0) || (value > 512))
				{
					Modules::log().writeDebugInfo( "Invalid size for Terrain node %i (must be 2^x + 1)", _handle );
					return false;
				}

				if (sizeZ == value)
					return true;

				sizeZ = value;
				// TODO: Recreate terrain
				return true;
			default:
				return SceneNode::setParami(param, value);
			}
		}
		float IslandNode::getParamf(int param)
		{
			switch (param)
			{
			case IslandNodeParams::MeshQuality:
				return meshQuality;
			default:
				return SceneNode::getParamf(param);
			}
		}
		bool IslandNode::setParamf(int param, float value)
		{
			switch (param)
			{
			case IslandNodeParams::MeshQuality:
				meshQuality = value;
				return true;
			default:
				return SceneNode::setParamf(param, value);
			}
		}

		IslandNode::IslandNode(const IslandNodeTpl &islandTpl) : SceneNode(islandTpl),
			materialRes(islandTpl.matRes), sizeX(islandTpl.sizeX), sizeZ(islandTpl.sizeZ),
			meshQuality(islandTpl.meshQuality), heightdata(islandTpl.heightdata)
		{
			_renderable = true;
			localBBox.getMinCoords() = Vec3f(-1000, -1000, -1000);
			localBBox.getMaxCoords() = Vec3f(1000, 1000, 1000);
			for (int i = 0; i < TILE_COUNT; i++)
			{
				terrainpatches[i] = 0;
				vertexdata[i] = 0;
			}
			heightdata = new float[(sizeX + 1) * (sizeZ + 1)];
			for (int i = 0; i < (sizeX + 1) * (sizeZ + 1); i++)
			{
				heightdata[i] = 0.5;
			}
			// Create index buffers
			createIndexBuffers();
			// Create terrain patches
			createVertexBuffers();
			// Create extension plane around the terrain
			createExtension();
			// Set height data
			updateVertices(0, 0, sizeX + 1, sizeZ + 1);
		}

		void IslandNode::createIndexBuffers()
		{
			int size = TILE_SIZE;
			for (int i = 0; i < 5; i++)
			{
				int factor = TILE_SIZE / size;
				// Create buffer
				unsigned short *indices = new unsigned short[size * size * 6];
				// Surface
				for (int z = 0; z < size; z++)
				{
					for (int x = 0; x < size; x++)
					{
						indices[(z * size + x) * 6 + 0] = (z * factor + 0)      * (TILE_SIZE + 1) + x * factor + 0;
						indices[(z * size + x) * 6 + 2] = (z * factor + 0)      * (TILE_SIZE + 1) + x * factor + factor;
						indices[(z * size + x) * 6 + 1] = (z * factor + factor) * (TILE_SIZE + 1) + x * factor + 0;
						indices[(z * size + x) * 6 + 3] = (z * factor + 0)      * (TILE_SIZE + 1) + x * factor + factor;
						indices[(z * size + x) * 6 + 5] = (z * factor + factor) * (TILE_SIZE + 1) + x * factor + factor;
						indices[(z * size + x) * 6 + 4] = (z * factor + factor) * (TILE_SIZE + 1) + x * factor + 0;
					}
				}
				// TODO: Skirts
				indexBuffer[i] = Modules::renderer().uploadIndices(indices, size * size * 6 * sizeof(short));
				delete[] indices;
			}
		}

		void IslandNode::createVertexBuffers()
		{
			int alignedx = ((sizeX - 1) + TILE_SIZE) & ~(TILE_SIZE - 1);
			int alignedz = ((sizeZ - 1) + TILE_SIZE) & ~(TILE_SIZE - 1);
			int size = (TILE_SIZE + 1) * (TILE_SIZE + 1) * 3;
			// Create flat vertex buffers
			for (int z = 0; z < alignedz / TILE_SIZE; z++)
			{
				for (int x = 0; x < alignedx / TILE_SIZE; x++)
				{
					// Create and fill vertex buffer
					vertexdata[x + z * EDGE_TILES] = new float[size * 2];
					for (int z2 = 0; z2 < TILE_SIZE + 1; z2++)
					{
						for (int x2 = 0; x2 < TILE_SIZE + 1; x2++)
						{
							float *vertex = &vertexdata[x + z * EDGE_TILES][(z2 * (TILE_SIZE + 1) + x2) * 3];
							vertex[0] = x * TILE_SIZE + x2;
							vertex[1] = 0;
							vertex[2] = z * TILE_SIZE + z2;
						}
					}
					// Normals
					for (int z2 = 0; z2 < TILE_SIZE + 1; z2++)
					{
						for (int x2 = 0; x2 < TILE_SIZE + 1; x2++)
						{
							float *vertex = &vertexdata[x + z * EDGE_TILES][(z2 * (TILE_SIZE + 1) + x2) * 3 + size];
							vertex[0] = 0;
							vertex[1] = 1;
							vertex[2] = 0;
						}
					}
					// TODO: skirts
					// Create mesh
					terrainpatches[x + z * EDGE_TILES] = Modules::renderer().uploadVertices(vertexdata[x + z * EDGE_TILES],
						size * sizeof(float) * 2);
				}
			}
		}
		float IslandNode::getHeight(int x, int z)
		{
			if (x < 0) x = 0;
			if (z < 0) z = 0;
			if (x > sizeX) x = sizeX;
			if (z > sizeZ) z = sizeZ;

			return heightdata[z * (sizeX + 1) + x];
		}
		void IslandNode::updateVertices(int x, int y, int width, int height)
		{
			int tilesx = sizeX / TILE_SIZE;
			int tilesz = sizeZ / TILE_SIZE;
			int size = (TILE_SIZE + 1) * (TILE_SIZE + 1) * 3;
			// Set vertex data
			// TODO: Optimize this
			/*for (int iz = 0; iz < height; iz++)
			{
				int tilez = (iz + y) / TILE_SIZE;
				int offsetz = (iz + y) % TILE_SIZE;
				if (tilez == tilesz)
				{
					tilez--;
					offsetz = TILE_SIZE;
				}
				for (int ix = 0; ix < width; ix ++)
				{
					int tilex = (ix + x) / TILE_SIZE;
					int offsetx = (ix + x) % TILE_SIZE;
					if (tilex == tilesx)
					{
						tilex--;
						offsetx = TILE_SIZE;
					}
					float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3];
					vertex[1] = heightdata[(iz + y) * (sizeX + 1) + (ix + x)];
					if ((offsetx == 0) && (tilex > 0))
					{
						tilex--;
						offsetx = TILE_SIZE;
						float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3];
						vertex[1] = heightdata[(iz + y) * (sizeX + 1) + (ix + x)];
					}
				}
				if ((offsetz == 0) && (tilez > 0))
				{
					tilez--;
					offsetz = TILE_SIZE;
					for (int ix = 0; ix < width; ix ++)
					{
						int tilex = (ix + x) / TILE_SIZE;
						int offsetx = (ix + x) % TILE_SIZE;
						if (tilex == tilesx)
						{
							tilex--;
							offsetx = TILE_SIZE;
						}
						float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3];
						vertex[1] = heightdata[(iz + y) * (sizeX + 1) + (ix + x)];
						if ((offsetx == 0) && (tilex > 0))
						{
							tilex--;
							offsetx = TILE_SIZE;
							float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3];
							vertex[1] = heightdata[(iz + y) * (sizeX + 1) + (ix + x)];
						}
					}
				}
			}*/
			for (int iz = 0; iz < height; iz++)
			{
				for (int ix = 0; ix < width; ix ++)
				{
					setHeight(x + ix, y + iz, heightdata[(iz + y) * (sizeX + 1) + (ix + x)]);
				}
			}
			// Compute normals
			for (int iz = 0; iz < height; iz++)
			{
				int tilez = (iz + y) / TILE_SIZE;
				int offsetz = (iz + y) % TILE_SIZE;
				if (tilez == tilesz)
				{
					tilez--;
					offsetz = TILE_SIZE;
				}
				for (int ix = 0; ix < width; ix ++)
				{
					int tilex = (ix + x) / TILE_SIZE;
					int offsetx = (ix + x) % TILE_SIZE;
					if (tilex == tilesx)
					{
						tilex--;
						offsetx = TILE_SIZE;
					}
					float h1 = getHeight(x + ix - 1, y + iz);
					float h2 = getHeight(x + ix + 1, y + iz);
					float h3 = getHeight(x + ix, y + iz - 1);
					float h4 = getHeight(x + ix, y + iz + 1);
					Vec3f v1(2.0f, h2 - h1, 0.0f);
					Vec3f v2(0.0f, h4 - h3, 2.0f);
					Vec3f normal = v2.cross(v1).normalized();
					/*float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3 + size];
					vertex[0] = normal.x;
					vertex[1] = normal.y;
					vertex[2] = normal.z;*/
					updateNormal(x + ix, y + iz);
				}
			}
			// Refresh vertex buffers
			for (int iz = y / TILE_SIZE; iz <= (y + height) / TILE_SIZE; iz++)
			{
				for (int ix = x / TILE_SIZE; ix <= (x + width) / TILE_SIZE; ix++)
				{
					Modules::renderer().updateVertices(vertexdata[ix + iz * EDGE_TILES],
						0, size * sizeof(float) * 2, terrainpatches[ix + iz * EDGE_TILES]);
				}
			}
		}

		void IslandNode::createExtension()
		{
			
		}

		float *IslandNode::getHeightData()
		{
			return heightdata;
		}

		void IslandNode::setHeight(int x, int z, float height)
		{
			int tilesx = sizeX / TILE_SIZE;
			int tilesz = sizeZ / TILE_SIZE;
			int size = (TILE_SIZE + 1) * (TILE_SIZE + 1) * 3;
			// Set vertex data
			// TODO: Optimize this
			int tilez = z / TILE_SIZE;
			int offsetz = z % TILE_SIZE;
			if (tilez == tilesz)
			{
				tilez--;
				offsetz = TILE_SIZE;
			}
			{
				int tilex = x / TILE_SIZE;
				int offsetx = x % TILE_SIZE;
				if (tilex == tilesx)
				{
					tilex--;
					offsetx = TILE_SIZE;
				}
				float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3];
				vertex[1] = height;
				if ((offsetx == 0) && (tilex > 0))
				{
					tilex--;
					offsetx = TILE_SIZE;
					float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3];
					vertex[1] = height;
				}
			}
			if ((offsetz == 0) && (tilez > 0))
			{
				tilez--;
				offsetz = TILE_SIZE;
				{
					int tilex = x / TILE_SIZE;
					int offsetx = x % TILE_SIZE;
					if (tilex == tilesx)
					{
						tilex--;
						offsetx = TILE_SIZE;
					}
					float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3];
					vertex[1] = height;
					if ((offsetx == 0) && (tilex > 0))
					{
						tilex--;
						offsetx = TILE_SIZE;
						float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3];
						vertex[1] = height;
					}
				}
			}
		}
		void IslandNode::updateNormal(int x, int z)
		{
			// Compute normal
			float h1 = getHeight(x - 1, z);
			float h2 = getHeight(x + 1, z);
			float h3 = getHeight(x, z - 1);
			float h4 = getHeight(x, z + 1);
			Vec3f v1(2.0f, h2 - h1, 0.0f);
			Vec3f v2(0.0f, h4 - h3, 2.0f);
			Vec3f normal = v2.cross(v1).normalized();

			int tilesx = sizeX / TILE_SIZE;
			int tilesz = sizeZ / TILE_SIZE;
			int size = (TILE_SIZE + 1) * (TILE_SIZE + 1) * 3;
			// Set vertex data
			// TODO: Optimize this
			int tilez = z / TILE_SIZE;
			int offsetz = z % TILE_SIZE;
			if (tilez == tilesz)
			{
				tilez--;
				offsetz = TILE_SIZE;
			}
			{
				int tilex = x / TILE_SIZE;
				int offsetx = x % TILE_SIZE;
				if (tilex == tilesx)
				{
					tilex--;
					offsetx = TILE_SIZE;
				}
				float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3 + size];
				vertex[0] = normal.x;
				vertex[1] = normal.y;
				vertex[2] = normal.z;
				if ((offsetx == 0) && (tilex > 0))
				{
					tilex--;
					offsetx = TILE_SIZE;
					float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3 + size];
					vertex[0] = normal.x;
					vertex[1] = normal.y;
					vertex[2] = normal.z;
				}
			}
			if ((offsetz == 0) && (tilez > 0))
			{
				tilez--;
				offsetz = TILE_SIZE;
				{
					int tilex = x / TILE_SIZE;
					int offsetx = x % TILE_SIZE;
					if (tilex == tilesx)
					{
						tilex--;
						offsetx = TILE_SIZE;
					}
					float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3 + size];
					vertex[0] = normal.x;
					vertex[1] = normal.y;
					vertex[2] = normal.z;
					if ((offsetx == 0) && (tilex > 0))
					{
						tilex--;
						offsetx = TILE_SIZE;
						float *vertex = &vertexdata[tilex + tilez * EDGE_TILES][(offsetz * (TILE_SIZE + 1) + offsetx) * 3 + size];
						vertex[0] = normal.x;
						vertex[1] = normal.y;
						vertex[2] = normal.z;
					}
				}
			}
		}
	}
}
