
/*
 * Horde3D extensions
 *
 * Copyright (C) 2009 Mathias Gottschlag, Nicolas Schulz, Volker Wiendl
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/horde3d/extensions/island.hh"
#include "egModules.h"

#include <string>

namespace eal
{
	namespace horde3d
	{
		bool initExtension()
		{
			Modules::sceneMan().registerType(SNT_IslandNode, "Island",
				IslandNode::parsingFunc, IslandNode::factoryFunc, IslandNode::renderFunc);

			// Upload default shader used for debug view
			Modules::renderer().uploadShader(vsIslandDebugView,
				fsIslandDebugView, IslandNode::debugViewShader);

			return true;
		}


		std::string safeStr(const char *str)
		{
			if (str != 0x0) return str;
			else return "";
		}

		void installExtensions()
		{
			initExtension();
		}

		namespace Island
		{
			NodeHandle addIslandNode(NodeHandle parent, const char *name, int sizeX, int sizeZ, ResHandle materialRes)
			{
				SceneNode *parentNode = Modules::sceneMan().resolveNodeHandle( parent );
				if( parentNode == 0x0 ) return 0;

				Resource *matRes =  Modules::resMan().resolveResHandle( materialRes );
				if( matRes == 0x0 || matRes->getType() != ResourceTypes::Material ) return 0;

				Modules::log().writeInfo( "Adding Island node '%s'", safeStr( name ).c_str() );

				IslandNodeTpl tpl(safeStr(name), (MaterialResource*)matRes, sizeX, sizeZ);
				SceneNode *sn = Modules::sceneMan().findType(SNT_IslandNode)->factoryFunc(tpl);
				return Modules::sceneMan().addNode(sn, *parentNode);
			}

			void setSize(NodeHandle island, int sizeX, int sizeZ)
			{
			}

			float *getData(NodeHandle island)
			{
				SceneNode *sn = Modules::sceneMan().resolveNodeHandle( island );
				if( sn != 0x0 && sn->getType() == SNT_IslandNode )
					return ((IslandNode *)sn)->getHeightData();
				else
					return 0;
			}
			void invalidate(NodeHandle island, int x, int z, int sizeX, int sizeZ)
			{
				SceneNode *sn = Modules::sceneMan().resolveNodeHandle( island );
				if( sn != 0x0 && sn->getType() == SNT_IslandNode )
					((IslandNode *)sn)->updateVertices(x, z, sizeX, sizeZ);
			}
		}
	}
}
