/*
 * Island scene node for Horde3D
 *
 * Copyright (C) 2009 Mathias Gottschlag, Nicolas Schulz, Volker Wiendl
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_HORDE3DISLAND_HH
#define STRANDED_HORDE3DISLAND_HH

#include "egPrerequisites.h"
#include "utMath.h"
#include "egMaterial.h"
#include "egTextures.h"
#include "egScene.h"

namespace eal
{
	namespace horde3d
	{
		extern const char *vsIslandDebugView;
		extern const char *fsIslandDebugView;
		
		static const int EDGE_TILES = 16;
		static const int TILE_SIZE = 32;
		static const int TILE_COUNT = 256;

		static const int SNT_IslandNode = 300;
		struct IslandNodeParams
		{
			enum List
			{
				MaterialRes = 10000,
				MeshQuality,
				SizeX,
				SizeZ,
				HeightData
			};
		};

		struct IslandNodeTpl : public SceneNodeTpl
		{
			PMaterialResource  matRes;
			int                sizeX;
			int                sizeZ;
			float              meshQuality;
			float             *heightdata;

			IslandNodeTpl(const std::string &name, MaterialResource *matRes, int sizeX, int sizeZ) :
				SceneNodeTpl(SNT_IslandNode, name), matRes(matRes), sizeX(sizeX), sizeZ(sizeZ),
				meshQuality(50.0f), heightdata(0)
			{
			}
		};

		class IslandNode : public SceneNode
		{
			public:
				static ShaderCombination debugViewShader;

				~IslandNode();

				static SceneNodeTpl *parsingFunc(std::map< std::string, std::string > &attribs);
				static SceneNode *factoryFunc(const SceneNodeTpl &nodeTpl);
				static void renderFunc(const std::string &shaderContext, const std::string &theClass, bool debugView,
					const Frustum *frust1, const Frustum *frust2, RenderingOrder::List order, int occSet);

				bool canAttach(SceneNode &parent);
				int getParami(int param);
				bool setParami(int param, int value);
				float getParamf(int param);
				bool setParamf(int param, float value);

				BoundingBox *getLocalBBox() { return &localBBox; }

				float *getHeightData();
				float getHeight(int x, int z);
				void updateVertices(int x, int y, int width, int height);

			private:
				IslandNode(const IslandNodeTpl &islandTpl);

				void createIndexBuffers();
				void createVertexBuffers();
				void createExtension();

				void setHeight(int x, int z, float height);
				void updateNormal(int x, int z);

				PMaterialResource  materialRes;
				int                sizeX;
				int                sizeZ;
				float              meshQuality;
				float             *heightdata;

				BoundingBox        localBBox;
				uint32             indexBuffer[5];
				uint32             terrainpatches[TILE_COUNT];
				float             *vertexdata[TILE_COUNT];
				uint32             extensionvertices;
				uint32             extensionindices;
		};
	}
}

#endif
