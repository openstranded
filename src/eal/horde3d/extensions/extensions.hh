/*
 * Horde3D extensions
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_HORDE3DEXTENSIONS_H
#define STRANDED_HORDE3DEXTENSIONS_H

#include <horde3d/Horde3D.h>

namespace eal
{
	namespace horde3d
	{
		void installExtensions();
		// Island scene node: simple terrain, with edges extended horizontally
		static const int SNT_IslandNode = 300;
		struct IslandNodeParams
		{
			enum List
			{
				MaterialRes = 10000,
				MeshQuality,
				SizeX,
				SizeZ,
				HeightData
			};
		};
		namespace Island
		{
			NodeHandle addIslandNode(NodeHandle parent, const char *name, int sizeX, int sizeZ, ResHandle materialRes);

			void setSize(NodeHandle island, int sizeX, int sizeZ);

			float *getData(NodeHandle island);
			void invalidate(NodeHandle island, int x, int z, int sizeX, int sizeZ);
		}
	}
}

#endif
