/*
 * Horde3D camera scene node
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_HORDE3DCAMERA_HH
#define STRANDED_HORDE3DCAMERA_HH

#include "eal/camera.hh"

namespace eal
{
	class Horde3DCamera : public Camera
	{
		public:
			Horde3DCamera(int camera);
			virtual ~Horde3DCamera();

			virtual void
			setPosition(const Vector3 &pos);

			virtual Vector3
			getPosition();

			virtual void
			setAngles(const Vector3 &angles);

			virtual Vector3
			getAngles();

			void update(float mx, float my, bool w, bool a, bool s, bool d);

			void setSun(int sun)
			{
				this->sun = sun;
			}

			int getSceneNode(void)
			{
				return camera;
			}
		private:
			int camera;
			float rotation[2];
			float position[3];
			int sun;
	};
}

#endif
