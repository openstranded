/*
 * Horde3D scene node
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/horde3d/scenenode.hh"
#include "eal/horde3d/engine.hh"

#include <horde3d/Horde3D.h>
#include <horde3d/Horde3DUtils.h>

namespace eal
{
	Horde3DSceneNode::Horde3DSceneNode()
	{
		node = 0;
	}
	Horde3DSceneNode::~Horde3DSceneNode()
	{
		if (node)
		{
		}
	}

	std::string
	Horde3DSceneNode::getModel()
	{
		return model;
	}

	void
	Horde3DSceneNode::setModel(std::string model)
	{
		this->model = model;
		// Delete old scene node
		if (node)
		{
		}
		// Create new scene node
	}

	Texture&
	Horde3DSceneNode::getTexture()
	{
	}

	void
	Horde3DSceneNode::setTexture(Texture &texture)
	{
	}

	Color
	Horde3DSceneNode::getColor()
	{
	}

	void
	Horde3DSceneNode::setColor(const Color &color)
	{
	}

	void
	Horde3DSceneNode::setPosition(const Vector3 &position)
	{
		this->position = position;
		if (node)
		{
		}
	}

	Vector3
	Horde3DSceneNode::getPosition()
	{
		return position;
	}

	void
	Horde3DSceneNode::setRotation(const RotationVector &rotation)
	{
	}

	RotationVector
	Horde3DSceneNode::getRotation()
	{
	}

	void
	Horde3DSceneNode::playAnimation (int start, int end, double speed, int mode)
	{
	}
}
