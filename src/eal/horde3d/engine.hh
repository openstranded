/*
 * Horde3D implementation of the rendering system
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_HORDE3DENGINE_HH
#define STRANDED_HORDE3DENGINE_HH

#include "eal/engine.hh"
#include "eal/horde3d/converter.hh"

namespace eal
{
	/**
	 * Horde3D implementation of the rendering system
	 */
	class Horde3DEngine : public Engine
	{
		public:
			/**
			 * Constructor. Must not be called more than once.
			 */
			Horde3DEngine();
			virtual ~Horde3DEngine();

			virtual bool
			init(GlobalSettings *settings = 0);

			virtual SceneNode*
			addSceneNode();
			virtual Environment*
			addEnvironment();

			/**
			 * Loads all not-yet-loaded graphics resources.
			 */
			void
			loadResources();

			/**
			 * Returns a pointer to the media file converter
			 */
			Horde3DConverter*
			getConverter()
			{
				return converter;
			}

			virtual bool
			update();

			virtual const std::vector<std::string>&
			getGfxExtensions();

			virtual const std::vector<std::string>&
			getSfxExtensions(SfxType type) = 0;

		private:
			int cameranode;
			std::string searchpath;
			int panelmat;
			int fontmat;

			Horde3DConverter *converter;
	};
}

#endif
