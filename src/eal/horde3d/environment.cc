/*
 * Terrain, weather, lights (Horde3D renderer)
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/horde3d/environment.hh"
#include "eal/horde3d/engine.hh"
#include "eal/horde3d/extensions/extensions.hh"
#include "eal/horde3d/camera.hh"

#include <horde3d/Horde3D.h>
#include <horde3d/Horde3DUtils.h>
#include <horde3d/Horde3DWater.h>

#include <cstring>

namespace eal
{
	Horde3DEnvironment::Horde3DEnvironment()
	{
		terrain = 0;
		terrainsize = 64;
		colormap = 0;
		sky = 0;
		water = 0;

		instances.push_back(this);
	}
	Horde3DEnvironment::~Horde3DEnvironment()
	{
		for (unsigned int i = 0; i < instances.size(); i++)
		{
			if (instances[i] == this)
			{
				instances.erase(instances.begin() + i);
				break;
			}
		}
	}
	
	void
	Horde3DEnvironment::setSize(int size)
	{
		terrainsize = size;
		if (!terrain)
			createTerrain();
		else
		{
		}
	}

	int
	Horde3DEnvironment::getSize()
	{
		return terrainsize;
	}

	void
	Horde3DEnvironment::setHeight (const Vector2 &pos, double height)
	{
		if (!terrain)
			createTerrain();

		
	}

	double
	Horde3DEnvironment::getHeight (const Vector2 &pos)
	{
		if (!terrain)
			createTerrain();

		return 0;
	}

	void
	Horde3DEnvironment::setHeightData(float *heightdata)
	{
		if (!terrain)
			createTerrain();

		float *currentdata = horde3d::Island::getData(terrain);
		for (int i = 0; i < (terrainsize + 1) * (terrainsize + 1); i++)
		{
			currentdata[i] = heightdata[i];
		}
		horde3d::Island::invalidate(terrain, 0, 0, terrainsize + 1, terrainsize + 1);
	}

	void
	Horde3DEnvironment::setColor (const Vector2 &pos, const Color &color)
	{
		if (!terrain)
			createTerrain();

		
	}

	Color
	Horde3DEnvironment::getColor (const Vector2 &pos)
	{
	}

	void Horde3DEnvironment::setColorMap (unsigned char *colormap, int size)
	{
		if (!terrain)
			createTerrain();

		// Convert color map to 32 bit array
		uint32_t *colordata = new uint32_t[size * size];
		for (int i = 0; i < size * size; i++)
		{
			colordata[i] = ((int)colormap[i * 3] << 16)
				+ ((int)colormap[i * 3 + 1] << 8)
				+ (int)colormap[i * 3 + 2];
		}

		// Upload color data
		Horde3D::unloadResource(this->colormap);
		if (!texturename)
			this->colormap = Horde3D::createTexture2D("environment_colormap2", ResourceFlags::NoTexMipmaps, size, size, false);
		else
			this->colormap = Horde3D::createTexture2D("environment_colormap1", ResourceFlags::NoTexMipmaps, size, size, false);
		texturename = 1 - texturename;
		Horde3D::updateResourceData(this->colormap, TextureResParams::PixelData, colordata, size * size * 4);
		Horde3D::setResourceParamItemi(material, MaterialResParams::Sampler, "colormap", this->colormap);
		delete[] colordata;
	}
	void Horde3DEnvironment::setColorMap (std::string path)
	{
	}

	void
	Horde3DEnvironment::setSkyCube(std::string sky)
	{
		// Delete old sky cube
		if (this->sky)
		{
			Horde3D::removeNode(this->sky);
			this->sky = 0;
		}
		// Pack skybox images
		Horde3DConverter *converter = ((Horde3DEngine*)Engine::get())->getConverter();
		if (converter->convertSkybox(sky))
		{
			// Load skybox
			std::string filename = std::string("models/skies/") + sky + ".scene.xml";
			int skybox = Horde3D::addResource(ResourceTypes::SceneGraph, filename.c_str(), 0);
			((Horde3DEngine*)Engine::get())->loadResources();
			this->sky = Horde3D::addNodes(((Horde3DCamera*)Engine::get()->getCamera())->getSceneNode(), skybox);
			Horde3D::setNodeTransform(this->sky, 0, 0, 0, 0, 0, 0, 2000, 2000, 2000);
			// Set water reflection
			filename = std::string("models/skies/") + sky + ".dds";
			ResHandle skyTexture = Horde3D::addResource( ResourceTypes::Texture, filename.c_str(), 0 );
			((Horde3DEngine*)Engine::get())->loadResources();
			Horde3D::setResourceParamItemi(waterMat, MaterialResParams::Sampler, "sky", skyTexture);
		}
	}

	void
	Horde3DEnvironment::setFog(const Color &fogcolor)
	{

	}

	void Horde3DEnvironment::createTerrain()
	{
		material = Horde3D::addResource(ResourceTypes::Material, "materials/terrain.material.xml", 0 );
		ResHandle lightMatRes = Horde3D::addResource( ResourceTypes::Material, "materials/light.material.xml", 0 );
		waterMat = Horde3D::addResource( ResourceTypes::Material, "materials/water.material.xml", 0 );
		((Horde3DEngine*)Engine::get())->loadResources();
		// Create terrain
		//int skybox = Horde3D::addResource(ResourceTypes::SceneGraph, "models/skybox/skybox.scene.xml", 0);
		//((Horde3DEngine*)Engine::get())->loadResources();
		//sky = Horde3D::addNodes(((Horde3DCamera*)Engine::get()->getCamera())->getSceneNode(), skybox);
		//Horde3D::setNodeTransform(sky, 0, 0, 0, 0, 0, 0, 2000, 2000, 2000);

		terrain = horde3d::Island::addIslandNode(RootNode, "terrain", terrainsize, terrainsize, material);
		Horde3D::setNodeTransform(terrain, -32 * terrainsize, -1600, -32 * terrainsize, 0, 0, 0, 64, 3200, 64);
		colormap = Horde3D::createTexture2D("environment_colormap", ResourceFlags::NoTexMipmaps, 128, 128, false);
		unsigned char texturedata[128 * 128 * 4];
		memset(texturedata, 128, 128 * 128 * 4);
		Horde3D::updateResourceData(colormap, TextureResParams::PixelData, texturedata, 128 * 128 * 4);
		Horde3D::setResourceParamItemi(material, MaterialResParams::Sampler, "colormap", colormap);
		texturename = 0;

		sun = Horde3D::addLightNode(RootNode, "Light1", lightMatRes, "LIGHTING", "");
		Horde3D::setNodeTransform(sun, 0, 2000, 1000, -90, 0, 0, 1, 1, 1);
		Horde3D::setNodeParamf(sun, LightNodeParams::Radius, 3000 );
		Horde3D::setNodeParami(sun, LightNodeParams::ShadowMapCount, 3);
		Horde3D::setNodeParamf(sun, LightNodeParams::ShadowSplitLambda, 0.9f);
		Horde3D::setNodeParamf(sun, LightNodeParams::ShadowMapBias, 0.001f);
		Horde3D::setNodeParamf(sun, LightNodeParams::FOV, 120);
		Horde3D::setNodeParamf(sun, LightNodeParams::Col_R, 0.9f);
		Horde3D::setNodeParamf(sun, LightNodeParams::Col_G, 0.7f);
		Horde3D::setNodeParamf(sun, LightNodeParams::Col_B, 0.75f);
		((Horde3DCamera*)Engine::get()->getCamera())->setSun(sun);

		waterres = Horde3DWater::addNoise( "waternoise", 4 );
		water = Horde3DWater::addWaterNode( RootNode, "water", waterres, waterMat );
		Horde3D::setNodeTransform( water, 0, 0, 0, 0, 0, 0, 100, 10, 100 );
	}

	void Horde3DEnvironment::updateWater(float time)
	{
		for (unsigned int i = 0; i < instances.size(); i++)
		{
			Horde3DWater::setNoiseTime( instances[i]->waterres, time );
		}
	}

	std::vector<Horde3DEnvironment*> Horde3DEnvironment::instances;
}
