/*
 * Horde3D scene node
 *
 * Copyright (C) 2009 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_HORDE3DSCENENODE_HH
#define STRANDED_HORDE3DSCENENODE_HH

#include "eal/scenenode.hh"

namespace eal
{
	class Horde3DSceneNode : public SceneNode
	{
		public:
			Horde3DSceneNode();
			virtual ~Horde3DSceneNode();

			virtual std::string
			getModel();

			virtual void
			setModel(std::string model);

			virtual Texture&
			getTexture();

			virtual void
			setTexture(Texture &texture);

			virtual Color
			getColor();

			virtual void
			setColor(const Color &color);

			virtual void
			setPosition(const Vector3 &position);

			virtual Vector3
			getPosition();

			virtual void
			setRotation(const RotationVector &rotation);

			virtual RotationVector
			getRotation();

			virtual void
			playAnimation(int start, int end, double speed, int mode);
		private:
			int node;
			std::string model;
			Vector3 position;
	};
}

#endif
