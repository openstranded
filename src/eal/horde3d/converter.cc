/*
 * Horde3D file format converter
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/horde3d/converter.hh"

#include <fstream>
#include <iostream>
#include <FreeImage.h>
#include <squish.h>

namespace eal
{
	static void writeMipmap(std::ofstream &file, int level, FIBITMAP *image)
	{
		int width = FreeImage_GetWidth(image);
		for (int i = 0; i < level; i++)
			width /= 2;
		// Resize bitmap
		FIBITMAP *mipmap = FreeImage_Rescale(image, width, width, FILTER_BILINEAR);
		// Convert bitmap data
		if (width < 4) width = 4;
		squish::u8 *targetdata = new squish::u8[width * width / 2];
		squish::u8 *currentblock = targetdata;
		for (int y = 0; y < width; y += 4)
		{
			for (int x = 0; x < width; x += 4)
			{
				squish::u8 source[16 * 4];
				for (int i = 0; i < 16; i++)
				{
					RGBQUAD color;
					FreeImage_GetPixelColor(mipmap, width - (x + (i % 4)) - 1, width - (y + i / 4) - 1, &color);
					((unsigned int*)source)[i] = *(unsigned int*)&color;
				}
				squish::Compress(source, currentblock, squish::kDxt1);
				currentblock += 8;
			}
		}
		// Write data
		file.write((const char*)targetdata, width * width / 2);
		delete[] targetdata;
		// Delete bitmap
		FreeImage_Unload(mipmap);
	}
	Horde3DConverter::Horde3DConverter(std::string basedir)
	{
		this->basedir = basedir;

		FreeImage_Initialise(false);
	}
	Horde3DConverter::~Horde3DConverter()
	{
	}

	bool
	Horde3DConverter::convertSkybox(std::string name)
	{
		// Check whether file already exists
		std::ifstream testfile((basedir + "/extra/horde3d/models/skies/" + name + ".scene.xml").c_str());
		if (testfile)
			return true;
		testfile.close();
		// Load source images
		printf("Source: %s\n", (basedir + "/skies/" + name + "_dn.jpg").c_str());
		FIBITMAP *dn = FreeImage_Load(FIF_JPEG, (basedir + "/skies/" + name + "_dn.jpg").c_str());
		if (!dn) return false;
		FIBITMAP *up = FreeImage_Load(FIF_JPEG, (basedir + "/skies/" + name + "_up.jpg").c_str());
		FIBITMAP *bk = FreeImage_Load(FIF_JPEG, (basedir + "/skies/" + name + "_bk.jpg").c_str());
		FIBITMAP *fr = FreeImage_Load(FIF_JPEG, (basedir + "/skies/" + name + "_fr.jpg").c_str());
		FIBITMAP *lf = FreeImage_Load(FIF_JPEG, (basedir + "/skies/" + name + "_lf.jpg").c_str());
		FIBITMAP *rt = FreeImage_Load(FIF_JPEG, (basedir + "/skies/" + name + "_rt.jpg").c_str());
		// Create output image
		int width = FreeImage_GetWidth(dn);
		int height = FreeImage_GetHeight(dn);
		printf("Sky: %d/%d\n", width, height);
		// Create cube map
		std::ofstream ddsfile;
		ddsfile.open((basedir + "/extra/horde3d/models/skies/" + name + ".dds").c_str(), std::ofstream::binary);
		const char *magic = "DDS ";
		ddsfile.write(magic, 4);
		int size = 124;
		ddsfile.write((const char*)&size, 4);
		int flags = 0xa1007;
		ddsfile.write((const char*)&flags, 4);
		ddsfile.write((const char*)&height, 4);
		ddsfile.write((const char*)&width, 4);
		size = width * height / 2;
		ddsfile.write((const char*)&size, 4);
		int depth = 0;
		ddsfile.write((const char*)&depth, 4);
		int mipmaps = 0;
		int mipmapsize = width;
		while (mipmapsize > 0)
		{
			mipmaps++;
			mipmapsize /= 2;
		}
		ddsfile.write((const char*)&mipmaps, 4);
		int reserved[11] = {0};
		ddsfile.write((const char*)&reserved, 44);
		// Color format
		size = 32;
		ddsfile.write((const char*)&size, 4);
		flags = 4;
		ddsfile.write((const char*)&flags, 4);
		magic = "DXT1";
		ddsfile.write(magic, 4);
		ddsfile.write((const char*)&reserved, 20);
		// Capabilities
		int caps = 0x00401008;
		ddsfile.write((const char*)&caps, 4);
		caps = 0xfe00;
		ddsfile.write((const char*)&caps, 4);
		ddsfile.write((const char*)&reserved, 8);
		ddsfile.write((const char*)&reserved, 4);
		// Image data
		FIBITMAP *rt2 = FreeImage_ConvertTo32Bits(rt);
		for (int i = 0; i < mipmaps; i++)
		{
			writeMipmap(ddsfile, i, rt2);
		}
		FIBITMAP *lf2 = FreeImage_ConvertTo32Bits(lf);
		for (int i = 0; i < mipmaps; i++)
		{
			writeMipmap(ddsfile, i, lf2);
		}
		FIBITMAP *up2 = FreeImage_RotateClassic(up, 270);
		FreeImage_Unload(up);
		up = up2;
		up2 = FreeImage_ConvertTo32Bits(up);
		for (int i = 0; i < mipmaps; i++)
		{
			writeMipmap(ddsfile, i, up2);
		}
		FIBITMAP *dn2 = FreeImage_ConvertTo32Bits(dn);
		for (int i = 0; i < mipmaps; i++)
		{
			writeMipmap(ddsfile, i, dn2);
		}
		FIBITMAP *fr2 = FreeImage_ConvertTo32Bits(fr);
		for (int i = 0; i < mipmaps; i++)
		{
			writeMipmap(ddsfile, i, fr2);
		}
		FIBITMAP *bk2 = FreeImage_ConvertTo32Bits(bk);
		for (int i = 0; i < mipmaps; i++)
		{
			writeMipmap(ddsfile, i, bk2);
		}
		ddsfile.close();
		// Free used memory
		FreeImage_Unload(dn2);
		FreeImage_Unload(up2);
		FreeImage_Unload(bk2);
		FreeImage_Unload(fr2);
		FreeImage_Unload(lf2);
		FreeImage_Unload(rt2);
		FreeImage_Unload(dn);
		FreeImage_Unload(up);
		FreeImage_Unload(bk);
		FreeImage_Unload(fr);
		FreeImage_Unload(lf);
		FreeImage_Unload(rt);
		// Create material file
		std::ofstream material((basedir + "/extra/horde3d/models/skies/" + name + ".material.xml").c_str());
		material << "<Material>\n";
		material << "\t<Shader source=\"shaders/skybox.shader\"/>\n";
		material << "\t<Sampler name=\"albedoMap\" map=\"models/skies/" << name << ".dds\" />\n";
		material << "</Material>\n";
		material.close();
		// Create model file
		std::ofstream model((basedir + "/extra/horde3d/models/skies/" + name + ".scene.xml").c_str());
		model << "<Model name=\"skybox_" << name << "\" geometry=\"models/skybox/skybox.geo\">\n";
		model << "\t<Mesh name=\"Box01\" material=\"models/skies/" << name << ".material.xml\" tx=\"0\""
			" ty=\"0\" tz=\"-1\" rx=\"-90\" ry=\"0\" rz=\"-180\" sx=\"2\" sy=\"2\" sz=\"2\""
			" batchStart=\"0\" batchCount=\"36\" vertRStart=\"0\" vertREnd=\"23\" />\n";
		model << "</Model>\n";
		model.close();
		return true;
	}
}
