/*
 * Base class for graphics/sound
 *
 * Copyright (C) 2009 David Kolossa, Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_ENGINE_HH
#define STRANDED_ENGINE_HH

#include "eal/eal.hh"
#include "settings.hh"
#include "filesystem.hh"

namespace eal
{
	class SceneNode;
	class Camera;
	class Environment;
	
	/**
	 * Base interface for the sound/graphics engine.
	 */
	class Engine
	{
		public:
			/**
			 * Retrieves a pointer to the engine.
			 */
			static Engine*
			get();

			/**
			 * Destroys the world
			 */
			virtual ~Engine();

			/**
			 * Creates a window, initializes sound.
			 */
			virtual bool
			init(GlobalSettings *settings = 0) = 0;

			/**
			 * Returns a reference to the GUI
			 */
			GUI*
			getGUI();

			/**
			 * Returns a reference to the camera
			 */
			Camera*
			getCamera();

			/**
			 * Returns a reference to the environment
			 */
			Environment*
			getEnvironment();

			/**
			 * Creates a scene node
			 */
			virtual SceneNode*
			addSceneNode() = 0;

			/**
			 * Creates an environment
			 */
			virtual Environment*
			addEnvironment() = 0;

			/**
			 * Returns the current game settings.
			 */
			GlobalSettings *
			getSettings()
			{
				return settings;
			}

			/**
			 * Return the supported file formats for GFX.
			 * Note that this method also specifies the priority of
			 * the extensions. The objects with a lower vector index
			 * have a higher priority.
			 */
			virtual const std::vector<std::string>&
			getGfxExtensions(GfxType type) = 0;

			/**
			 * Return the supported file formats for SFX.
			 * Note that this method also specifies the priority of
			 * the extensions. The objects with a lower vector index
			 * have a higher priority.
			 */
			virtual const std::vector<std::string>&
			getSfxExtensions(SfxType type) = 0;

			/**
			 * Renders one frame and refills sound buffers if needed
			 */
			virtual bool update() = 0;


		protected:
			Engine ();

			GUI *gui;
			Camera *camera;
			Environment *environment;

			GlobalSettings *settings;

			static Engine *instance;
	};
}

#endif

