/*
 * This file defines the color class
 *
 * Copyright (C) 2009 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_COLOR_HH
#define STRANDED_COLOR_HH

#include <stdint.h>

namespace eal
{
	/*
	 * This is the RGB-Color class
	 */
	class Color
	{
		public:
			Color(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha = 255);
			Color(uint32_t color = 0);
			Color(const Color &other);
			~Color ();

			void
			setRed(uint8_t red);
			uint8_t
			getRed() const;

			void
			setGreen(uint8_t green);
			uint8_t
			getGreen() const;

			void
			setBlue(uint8_t blue);
			uint8_t
			getBlue() const;

			void
			set(uint32_t color);
			uint32_t
			get() const;

		private:
			union
			{
				uint8_t rgba[4];
				uint32_t color;
			};
	};
}


#endif /* STRANDED_COLOR_HH */

