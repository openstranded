/*
 * This file defines the color class
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/color.hh"

namespace eal
{
	Color::Color(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha)
	{
		rgba[0] = red;
		rgba[1] = blue;
		rgba[2] = green;
		rgba[3] = alpha;
	}
	Color::Color(uint32_t color)
	{
		this->color = color;
	}
	Color::Color(const Color &other)
	{
		color = other.color;
	}
	Color::~Color()
	{
	}

	void
	Color::setRed(uint8_t red)
	{
		rgba[0] = red;
	}
	uint8_t
	Color::getRed() const
	{
		return rgba[0];
	}

	void
	Color::setGreen(uint8_t green)
	{
		rgba[1] = green;
	}
	uint8_t
	Color::getGreen() const
	{
		return rgba[1];
	}
	void
	Color::setBlue(uint8_t blue)
	{
		rgba[2] = blue;
	}
	uint8_t
	Color::getBlue() const
	{
		return rgba[2];
	}


	void
	Color::set(uint32_t color)
	{
		this->color = color;
	}
	uint32_t
	Color::get() const
	{
		return color;
	}
}
