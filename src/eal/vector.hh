/*
 * This file defines position vectors
 *
 * Copyright (C) 2009 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_VECTOR_HH
#define STRANDED_VECTOR_HH

#include <string>

namespace eal
{
	/*
	 * A two-dimensional vector to define positions on the ground
	 */
	class Vector2
	{
		public:
			Vector2 (double x = 0, double y = 0);
			~Vector2 ();

			/*
			 * Vector addition
			 */
			Vector2
			operator+ (Vector2 &v);

			/*
			 * Vector subtraction
			 */
			Vector2
			operator- (Vector2 &v);		

			double x, y;
	};



	/*
	 * A three-dimensional vector
	 */
	class Vector3
	{
		public:
			Vector3 (double x = 0, double y = 0, double z = 0);
			~Vector3 ();

			/*
			 * Vector addition
			 */
			Vector3
			operator+ (Vector3 &v);

			/*
			 * Vector subtraction
			 */
			Vector3
			operator- (Vector3 &v);

			double x, y, z;
	};


	/*
	 * A rotation vector
	 */
	class RotationVector
	{
		public:
			RotationVector (double pitch = 0, double yaw = 0, double roll = 0);
			~RotationVector ();

			double pitch, yaw, roll;
	};
}


#endif /* STRANDED_VECTOR_HH */

