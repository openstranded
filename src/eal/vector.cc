/*
 * This file defines position vectors
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/vector.hh"

namespace eal
{
	Vector2::Vector2 (double x, double y) : x (x), y (y)
	{
	}
	Vector2::~Vector2 ()
	{
	}

	Vector2
	Vector2::operator+ (Vector2 &v)
	{
		return Vector2 (x + v.x, y + v.y);
	}

	Vector2
	Vector2::operator- (Vector2 &v)
	{
		return Vector2 (x - v.x, y - v.y);
	}


	Vector3::Vector3 (double x, double y, double z) : x (x), y (y), z (z)
	{
	}
	Vector3::~Vector3 ()
	{
	}

	Vector3
	Vector3::operator+ (Vector3 &v)
	{
		return Vector3 (x + v.x, y + v.y, z + v.z);
	}

	Vector3
	Vector3::operator- (Vector3 &v)
	{
		return Vector3 (x - v.x, y - v.y, z - v.z);
	}
}
