/*
 * Terrain, weather, lights (Irrlicht renderer)
 *
 * Copyright (C) 2009 David Kolossa, Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_IRRLICHTENVIRONMENT_HH
#define STRANDED_IRRLICHTENVIRONMENT_HH

#include "eal/environment.hh"

namespace irr
{
	namespace scene
	{
		class ISceneNode;
	}
}

namespace eal
{
	class IrrlichtTerrainSceneNode;

	class IrrlichtEnvironment : public Environment
	{
		public:
			IrrlichtEnvironment();
			virtual ~IrrlichtEnvironment();

			virtual void
			setSize (int size);

			virtual int
			getSize ();

			virtual void
			setHeight (const Vector2 &pos, double height);

			virtual double
			getHeight (const Vector2 &pos);

			virtual void
			setHeightData(float *heightdata);

			virtual void
			setColor (const Vector2 &pos, const Color &color);

			virtual Color
			getColor (const Vector2 &pos);

			virtual void
			setColorMap (unsigned char *colormap, int size);
			virtual void
			setColorMap (std::string path);

			virtual void
			setSkyCube(std::string sky);

			virtual void
			setFog(const Color &fogcolor);
		private:
			void
			createTerrain();

			IrrlichtTerrainSceneNode *terrain;
			int terrainsize;

			irr::scene::ISceneNode *sky;
			irr::scene::ISceneNode *water;
	};
}

#endif
