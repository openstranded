/*
 * Irrlicht implementation of the rendering system
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_IRRLICHTENGINE_HH
#define STRANDED_IRRLICHTENGINE_HH

#include "eal/engine.hh"

namespace irr
{
	class IrrlichtDevice;
	namespace scene
	{
		class ISceneManager;
		class ISceneNode;
	}
}

namespace eal
{
	/**
	 * Irrlicht implementation of the rendering system
	 */
	class IrrlichtEngine : public Engine
	{
		public:
			/**
			 * Constructor. Must not be called more than once.
			 */
			IrrlichtEngine();
			virtual ~IrrlichtEngine();

			virtual bool
			init(GlobalSettings *settings = 0);

			/**
			 * Returns a pointer to the Irrlicht scene manager
			 */
			irr::scene::ISceneManager*
			getSceneManager();

			virtual SceneNode*
			addSceneNode();
			virtual Environment*
			addEnvironment();

			virtual bool
			update();

			virtual const std::vector<std::string>&
			getGfxExtensions(GfxType type);

			virtual const std::vector<std::string>&
			getSfxExtensions(SfxType type);

		private:
			irr::IrrlichtDevice *irrlicht;

			int lastfps;
	};
}

#endif
