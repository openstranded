/*
 * Irrlicht camera scene node
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/irrlicht/camera.hh"

#include <irrlicht/irrlicht.h>

namespace eal
{
	IrrlichtCamera::IrrlichtCamera(irr::scene::ICameraSceneNode *camera)
	{
		this->camera = camera;
	}
	IrrlichtCamera::~IrrlichtCamera()
	{
	}

	void
	IrrlichtCamera::setPosition(const Vector3 &pos)
	{
		camera->setPosition (irr::core::vector3df (pos.x, pos.y, pos.z) );
	}

	Vector3
	IrrlichtCamera::getPosition()
	{
		irr::core::vector3df pos = camera->getPosition ();
		return Vector3 (pos.X, pos.Y, pos.Z);
	}

	void
	IrrlichtCamera::setAngles(const Vector3 &angles)
	{
		camera->setRotation (irr::core::vector3df (angles.x, angles.y, angles.z) );
	}

	Vector3
	IrrlichtCamera::getAngles()
	{
		irr::core::vector3df rot = camera->getRotation ();
		return Vector3 (rot.X, rot.Y, rot.Z);
	}
}
