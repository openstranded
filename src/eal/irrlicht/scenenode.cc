/*
 * Irrlicht scene node
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/irrlicht/scenenode.hh"
#include "eal/irrlicht/engine.hh"

#include <irrlicht/irrlicht.h>
using namespace irr;

namespace eal
{
	IrrlichtSceneNode::IrrlichtSceneNode()
	{
		node = 0;
	}
	IrrlichtSceneNode::~IrrlichtSceneNode()
	{
		if (node)
			node->remove();
	}

	std::string
	IrrlichtSceneNode::getModel()
	{
		return model;
	}

	void
	IrrlichtSceneNode::setModel(std::string model)
	{
		this->model = model;
		// Delete old scene node
		if (node)
			node->remove();
		// Create new scene node
		scene::ISceneManager *smgr = ((IrrlichtEngine*)Engine::get())->getSceneManager();
		scene::IAnimatedMesh *mesh = smgr->getMesh(model.c_str());
		node = smgr->addAnimatedMeshSceneNode(mesh);
	}

	Texture&
	IrrlichtSceneNode::getTexture()
	{
	}

	void
	IrrlichtSceneNode::setTexture(Texture &texture)
	{
	}

	Color
	IrrlichtSceneNode::getColor()
	{
	}

	void
	IrrlichtSceneNode::setColor(const Color &color)
	{
	}

	void
	IrrlichtSceneNode::setPosition(const Vector3 &position)
	{
		this->position = position;
		if (node)
		{
			node->setPosition(core::vector3df(position.x, position.y, position.z));
		}
	}

	Vector3
	IrrlichtSceneNode::getPosition()
	{
		return position;
	}

	void
	IrrlichtSceneNode::setRotation(const RotationVector &rotation)
	{
	}

	RotationVector
	IrrlichtSceneNode::getRotation()
	{
	}

	void
	IrrlichtSceneNode::playAnimation (int start, int end, double speed, int mode)
	{
	}
}
