/*
 * An irrlicht terrain scene node that supports manipulating the terrain
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded.
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/irrlicht/terrainscenenode.hh"
#include "eal/irrlicht/engine.hh"

#include <iostream>

using namespace irr;

namespace eal
{
	IrrlichtTerrainSceneNode::IrrlichtTerrainSceneNode(irr::scene::ISceneNode* parent, irr::scene::ISceneManager* mgr,
		irr::s32 id, int sizex, int sizez, float *heightdata, int colorsizex, int colorsizey)
		: scene::ISceneNode(parent, mgr, id)
	{
		//material.Wireframe = true;
		material.Lighting = false;
		// Reset member variables
		this->sizex = sizex;
		this->sizez = sizez;
		for (int i = 0; i < TILE_COUNT; i++)
		{
			mesh[i] = 0;
			vertices[i] = 0;
			dirty[i] = true;
			level[i] = 0;
		}
		// TODO: Create index buffers
		for (int i = 0; i < 5; i++)
		{
			lod[i] = 0;
		}

		int size = TILE_SIZE;
		for (int i = 0; i < 5; i++)
		{
			int factor = TILE_SIZE / size;
			// Create buffer
			lod[i] = new scene::CIndexBuffer(video::EIT_16BIT);
			lod[i]->reallocate(size * size * 6);// + 128 * 4 * 6);
			// Surface
			for (int z = 0; z < size; z++)
			{
				for (int x = 0; x < size; x++)
				{
					lod[i]->setValue((z * size + x) * 6 + 0, (z * factor + 0)      * (TILE_SIZE + 1) + x * factor + 0);
					lod[i]->setValue((z * size + x) * 6 + 2, (z * factor + 0)      * (TILE_SIZE + 1) + x * factor + factor);
					lod[i]->setValue((z * size + x) * 6 + 1, (z * factor + factor) * (TILE_SIZE + 1) + x * factor + 0);
					lod[i]->setValue((z * size + x) * 6 + 3, (z * factor + 0)      * (TILE_SIZE + 1) + x * factor + factor);
					lod[i]->setValue((z * size + x) * 6 + 5, (z * factor + factor) * (TILE_SIZE + 1) + x * factor + factor);
					lod[i]->setValue((z * size + x) * 6 + 4, (z * factor + factor) * (TILE_SIZE + 1) + x * factor + 0);
				}
			}
			lod[i]->set_used(size * size * 6);
			//lod[i]->setHardwareMappingHint(scene::EHM_DYNAMIC);
			// TODO: Skirts
			size /= 2;
		}

		// TODO: Culling
		box = core::aabbox3d<f32>(-10000.0f, -10000.0f, -10000.0f, 10000.0f, 10000.0f, 10000.0f);

		// Create initial meshes
		rebuildMeshes();

		if (heightdata)
			setHeightData(heightdata);

		// Material
		colormap = mgr->getVideoDriver()->addTexture(core::dimension2d<s32>(colorsizex, colorsizey), "colormap");
		uint32_t *colormapdata = (uint32_t*)colormap->lock(false);
		for (int i = 0; i < colorsizex * colorsizey; i++)
			colormapdata[i] = 0;
		colormap->unlock();
		material.setTexture(0, colormap);

		std::string detailmap = std::string("mods/") + Engine::get()->getSettings()->getModName() + "/sys/gfx/terraindirt.bmp";
		material.setTexture(1, mgr->getVideoDriver()->getTexture(detailmap.c_str()));
		material.MaterialType = video::EMT_DETAIL_MAP;
	}
	IrrlichtTerrainSceneNode::~IrrlichtTerrainSceneNode()
	{
		SceneManager->getVideoDriver()->removeTexture(colormap);
	}

	void IrrlichtTerrainSceneNode::OnRegisterSceneNode()
	{
		if (IsVisible)
			SceneManager->registerNodeForRendering(this);

		scene::ICameraSceneNode *camera = SceneManager->getActiveCamera();

		core::vector3df localcampos = camera->getPosition();
		core::matrix4 invtrans;
		AbsoluteTransformation.getInverse(invtrans);
		invtrans.transformVect(localcampos);
		localcampos.Y = 0;

		ISceneNode::OnRegisterSceneNode();
		// Update LOD
		for (int z = 0; z < EDGE_TILES; z++)
		{
			for (int x = 0; x < EDGE_TILES; x++)
			{
				if (!mesh[z * EDGE_TILES + x]) continue;
				core::vector3df patchposition(x * TILE_SIZE + TILE_SIZE / 2, 0, z * TILE_SIZE + TILE_SIZE / 2);
				float distance = (patchposition - localcampos).getLength();
				int newlevel = 0;
				if (distance > 4 * TILE_SIZE)
				{
					newlevel = 4;
				}
				else if (distance > 3 * TILE_SIZE)
				{
					newlevel = 3;
				}
				else if (distance > 2 * TILE_SIZE)
				{
					newlevel = 2;
				}
				else if (distance > TILE_SIZE)
				{
					newlevel = 1;
				}
				else
				{
					newlevel = 0;
				}
				if (level[z * EDGE_TILES + x] != newlevel)
				{
					level[z * EDGE_TILES + x] = newlevel;
					mesh[z * EDGE_TILES + x]->setIndexBuffer(lod[newlevel]);
					//mesh[z * EDGE_TILES + x]->setDirty(scene::EBT_VERTEX_AND_INDEX);
				}
			}
		}
	}

	void IrrlichtTerrainSceneNode::render()
	{
		if (!IsVisible || !SceneManager->getActiveCamera())
			return;

		video::IVideoDriver* driver = SceneManager->getVideoDriver();

		// Draw mesh
		driver->setTransform(video::ETS_WORLD, AbsoluteTransformation);
		driver->setMaterial(material);
		for (int i = 0; i < TILE_COUNT; i++)
		{
			if (mesh[i])
			{
				driver->drawMeshBuffer(mesh[i]);
			}
		}
	}
	const core::aabbox3d<f32>& IrrlichtTerrainSceneNode::getBoundingBox() const
	{
		return box;
	}
	u32 IrrlichtTerrainSceneNode::getMaterialCount() const
	{
		return 1;
	}
	video::SMaterial& IrrlichtTerrainSceneNode::getMaterial(u32 i)
	{
		return material;
	}

	void IrrlichtTerrainSceneNode::setSize(int sizex, int sizez)
	{
	}
	void IrrlichtTerrainSceneNode::getSize(int *sizex, int *sizez)
	{
		if (sizex)
			*sizex = this->sizex;
		if (sizez)
			*sizez = this->sizez;
	}
	void IrrlichtTerrainSceneNode::setHeight(int x, int z, float height)
	{
		int bufferx = x / TILE_SIZE;
		int bufferz = z / TILE_SIZE;
		int offsetx = x % TILE_SIZE;
		int offsetz = z % TILE_SIZE;
		scene::IVertexBuffer *buffer = vertices[bufferx + bufferz * EDGE_TILES];
		if (buffer)
		{
			((video::S3DVertex2TCoords*)buffer->pointer())[offsetz * (TILE_SIZE + 1) + offsetx].Pos.Y = height;
			mesh[bufferx + bufferz * EDGE_TILES]->setDirty();
		}
		// Some points are shared between multiple patches
		if ((offsetx == 0) && (bufferx > 0))
		{
			scene::IVertexBuffer *buffer = vertices[bufferx - 1 + bufferz * EDGE_TILES];
			if (buffer)
			{
				((video::S3DVertex2TCoords*)buffer->pointer())[offsetz * (TILE_SIZE + 1) + offsetx + TILE_SIZE].Pos.Y = height;
				mesh[bufferx - 1 + bufferz * EDGE_TILES]->setDirty();
			}
		}
		if ((offsetz == 0) && (bufferz > 0))
		{
			scene::IVertexBuffer *buffer = vertices[bufferx + (bufferz - 1) * EDGE_TILES];
			if (buffer)
			{
				((video::S3DVertex2TCoords*)buffer->pointer())[(offsetz + TILE_SIZE) * (TILE_SIZE + 1) + offsetx].Pos.Y = height;
				mesh[bufferx + (bufferz - 1) * EDGE_TILES]->setDirty();
			}
		}
		if ((offsetx == 0) && (bufferx > 0) && (offsetz == 0) && (bufferz > 0))
		{
			offsetx += TILE_SIZE;
			offsetz += TILE_SIZE;
			bufferx--;
			bufferz--;
			scene::IVertexBuffer *buffer = vertices[bufferx + bufferz * EDGE_TILES];
			if (buffer)
			{
				((video::S3DVertex2TCoords*)buffer->pointer())[offsetz * (TILE_SIZE + 1) + offsetx].Pos.Y = height;
				mesh[bufferx + bufferz * EDGE_TILES]->setDirty();
			}
		}
	}
	float IrrlichtTerrainSceneNode::getHeight(float x, float z)
	{
		core::vector3df localpos(x, 0, z);
		core::matrix4 invtrans;
		updateAbsolutePosition();
		AbsoluteTransformation.getInverse(invtrans);
		invtrans.transformVect(localpos);
		x = localpos.X;
		z = localpos.Z;
		if ((x < 0) || (x >= sizex)) return 0;
		if ((z < 0) || (z >= sizez)) return 0;
		int ix = x;
		int iz = z;
		int bufferx = ix / TILE_SIZE;
		int bufferz = iz / TILE_SIZE;
		int offsetx = ix % TILE_SIZE;
		int offsetz = iz % TILE_SIZE;
		scene::IVertexBuffer *buffer = vertices[bufferx + bufferz * EDGE_TILES];
		// TODO: Proper interpolation
		if (x - ix + z - iz < 1)
		{
			// First triangle
			return ((video::S3DVertex2TCoords*)buffer->pointer())[offsetz * (TILE_SIZE + 1) + offsetx + TILE_SIZE].Pos.Y * 3200 - 1600;
		}
		else
		{
			// Second triangle
			return ((video::S3DVertex2TCoords*)buffer->pointer())[offsetz * (TILE_SIZE + 1) + offsetx + TILE_SIZE].Pos.Y * 3200 - 1600;
		}
		return 0;
	}

	void IrrlichtTerrainSceneNode::rebuildMeshes()
	{
		int alignedx = ((sizex - 1) + TILE_SIZE) & ~(TILE_SIZE - 1);
		int alignedz = ((sizez - 1) + TILE_SIZE) & ~(TILE_SIZE - 1);
		// Create flat vertex buffers
		for (int z = 0; z < alignedz / TILE_SIZE; z++)
		{
			for (int x = 0; x < alignedx / TILE_SIZE; x++)
			{
				// Create and fill vertex buffer
				vertices[x + z * EDGE_TILES] = new scene::CVertexBuffer(video::EVT_2TCOORDS);
				scene::IVertexBuffer *buffer = vertices[x + z * EDGE_TILES];
				buffer->reallocate((TILE_SIZE + 1) * (TILE_SIZE + 1)); // + skirts
				for (int z2 = 0; z2 < TILE_SIZE + 1; z2++)
				{
					for (int x2 = 0; x2 < TILE_SIZE + 1; x2++)
					{
						video::S3DVertex2TCoords &vertex = ((video::S3DVertex2TCoords*)buffer->pointer())[z2 * (TILE_SIZE + 1) + x2];
						vertex.Pos = core::vector3df(x * TILE_SIZE + x2, 0, z * TILE_SIZE + z2);
						vertex.Normal = core::vector3df(0, 1, 0);
						vertex.Color = video::SColor(255, 255, 255, 255);
						vertex.TCoords
							= core::vector2df((float)(z * TILE_SIZE + z2) / alignedz, (float)(x * TILE_SIZE + x2) / alignedx);
						vertex.TCoords2 = vertex.TCoords * 64;
					}
				}
				buffer->set_used((TILE_SIZE + 1) * (TILE_SIZE + 1));
				buffer->setHardwareMappingHint(scene::EHM_STATIC);
				// Create mesh
				mesh[x + z * EDGE_TILES] = new scene::CDynamicMeshBuffer(video::EVT_2TCOORDS, video::EIT_16BIT);
				mesh[x + z * EDGE_TILES]->setVertexBuffer(buffer);
				mesh[x + z * EDGE_TILES]->setIndexBuffer(lod[0]);
				mesh[x + z * EDGE_TILES]->setDirty();
			}
		}
	}

	void IrrlichtTerrainSceneNode::setHeightData(float *data)
	{
		// TODO: Optimize this
		for (int z = 0; z <= sizez; z++)
		{
			for (int x = 0; x <= sizex; x++)
			{
				setHeight(x, z, data[z + x * (sizez + 1)]);
			}
		}
	}

	void IrrlichtTerrainSceneNode::setColorMapSize(int width, int height)
	{
		video::ITexture *oldcolormap = colormap;
		colormap = SceneManager->getVideoDriver()->addTexture(core::dimension2d<s32>(width, height), "colormap");
		uint32_t *colormapdata = (uint32_t*)colormap->lock(false);
		for (int i = 0; i < width * height; i++)
			colormapdata[i] = 0;
		colormap->unlock();
		material.setTexture(0, colormap);
		SceneManager->getVideoDriver()->removeTexture(oldcolormap);
	}
	void IrrlichtTerrainSceneNode::setColor(int x, int z, uint32_t color)
	{
		uint32_t *colormapdata = (uint32_t*)colormap->lock(false);
		colormapdata[x + z * colormap->getSize().Width] = color;
		colormap->unlock();
	}
	void IrrlichtTerrainSceneNode::setColorData(uint32_t *colordata)
	{
		uint32_t *colormapdata = (uint32_t*)colormap->lock(false);
		for (int i = 0; i < colormap->getSize().Width * colormap->getSize().Height; i++)
			colormapdata[i] = colordata[i];
		colormap->unlock();
	}
}
