/*
 * Irrlicht camera scene node
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_IRRLICHTCAMERA_HH
#define STRANDED_IRRLICHTCAMERA_HH

#include "eal/camera.hh"

namespace irr
{
	namespace scene
	{
		class ICameraSceneNode;
	}
}

namespace eal
{
	class IrrlichtCamera : public Camera
	{
		public:
			IrrlichtCamera(irr::scene::ICameraSceneNode *camera);
			virtual ~IrrlichtCamera();

			virtual void
			setPosition(const Vector3 &pos);

			virtual Vector3
			getPosition();

			virtual void
			setAngles(const Vector3 &angles);

			virtual Vector3
			getAngles();
		private:
			irr::scene::ICameraSceneNode *camera;
	};
}

#endif
