/*
 * An irrlicht terrain scene node that supports manipulating the terrain
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded.
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_IRRLICHTTERRAINSCENENODE_HH
#define STRANDED_IRRLICHTTERRAINSCENENODE_HH

#include <irrlicht/irrlicht.h>
#include <stdint.h>

namespace eal
{
	static const int EDGE_TILES = 16;
	static const int TILE_SIZE = 64;
	static const int TILE_COUNT = 256;

	class IrrlichtTerrainSceneNode : public irr::scene::ISceneNode
	{
		public:
			IrrlichtTerrainSceneNode(irr::scene::ISceneNode* parent, irr::scene::ISceneManager* mgr,
				irr::s32 id, int sizex = 128, int sizez = 128, float *heightdata = 0,
				int colorsizex = 128, int colorsizey = 128);
			virtual ~IrrlichtTerrainSceneNode();

			virtual void OnRegisterSceneNode();

			virtual void render();
			virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const;
			virtual irr::u32 getMaterialCount() const;
			virtual irr::video::SMaterial& getMaterial(irr::u32 i);

			void setSize(int sizex, int sizez);
			void getSize(int *sizex, int *sizez);
			void setHeight(int x, int z, float height);
			float getHeight(float x, float z);

			void setHeightData(float *data);

			void setColorMapSize(int width, int height);
			void setColor(int x, int z, uint32_t color);
			void setColorData(uint32_t *colordata);
		private:
			void rebuildMeshes();

			irr::core::aabbox3d<irr::f32> box;

			int sizex;
			int sizez;
			irr::scene::IDynamicMeshBuffer *mesh[TILE_COUNT];
			irr::scene::IVertexBuffer *vertices[TILE_COUNT];
			bool dirty[TILE_COUNT];
			int level[TILE_COUNT];
			irr::scene::IIndexBuffer *lod[5];

			irr::video::SMaterial material;

			irr::video::ITexture *colormap;
	};
}

#endif
