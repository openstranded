/*
 * Irrlicht implementation of the rendering system
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/irrlicht/engine.hh"
#include "eal/irrlicht/camera.hh"
#include "eal/irrlicht/scenenode.hh"
#include "eal/irrlicht/environment.hh"

#include <irrlicht/irrlicht.h>
#include <string>
using namespace irr;

namespace eal
{
	IrrlichtEngine::IrrlichtEngine() : Engine()
	{
		irrlicht = 0;
		lastfps = 0;
		instance = this;
	}
	IrrlichtEngine::~IrrlichtEngine()
	{
		if (irrlicht)
		{
			irrlicht->drop ();
		}
	}

	bool
	IrrlichtEngine::init(GlobalSettings *settings)
	{
		this->settings = settings;
		// Create window
		irrlicht = irr::createDevice (irr::video::EDT_OPENGL,
			irr::core::dimension2d<irr::s32>(1024, 768), 32);

		irr::scene::ISceneManager* smgr = irrlicht->getSceneManager();

		//irr::scene::ICameraSceneNode *camera = smgr->addCameraSceneNode ();
		irr::scene::ICameraSceneNode *camera = smgr->addCameraSceneNodeFPS();
		camera->setPosition(irr::core::vector3df(-1000, 1000, -1000));
		camera->setTarget(irr::core::vector3df(0, 0, 0));
		camera->setFarValue(1000);
		this->camera = new IrrlichtCamera(camera);

		return true;
	}

	irr::scene::ISceneManager*
	IrrlichtEngine::getSceneManager()
	{
		if (!irrlicht) return 0;
		return irrlicht->getSceneManager();
	}

	SceneNode*
	IrrlichtEngine::addSceneNode()
	{
		return new IrrlichtSceneNode;
	}
	Environment*
	IrrlichtEngine::addEnvironment()
	{
		return new IrrlichtEnvironment;
	}

	bool
	IrrlichtEngine::update()
	{
		if (!irrlicht->run ())
			return false;

		irr::video::IVideoDriver *driver = irrlicht->getVideoDriver();
		if (driver->getFPS () != lastfps)
		{
			lastfps = driver->getFPS ();
			wchar_t caption[256];
			swprintf (caption, 256, L"OpenStranded - FPS: %d - Polygons: %d", lastfps, driver->getPrimitiveCountDrawn());
			irrlicht->setWindowCaption (caption);
		}
		irr::scene::ISceneManager* smgr = irrlicht->getSceneManager();
		// Render scene
		driver->beginScene (true, true, irr::video::SColor (255, 100, 101, 140) );
		smgr->drawAll ();
		driver->endScene ();
		return true;
	}

	const std::vector<std::string>&
	IrrlichtEngine::getGfxExtensions(GfxType type)
	{
		//
		// The allowed extensions for plain images
		//
		static std::vector<std::string> imageExtensions(8, "");

		// Main image file formats for OpenStranded.
		// You should use one of these formats for your image files in
		// OpenStranded since they are supported by all engines.
		// PNG is recommended as it is a free standard and provides a
		// high quality with small files.
		imageExtensions[0] = "png";
		imageExtensions[1] = "tga";
		imageExtensions[2] = "jpg";
		imageExtensions[3] = "bmp";
		
		// These obscure file formats are unlikely to be used and not
		// recommended. Anyway, since Irrlicht can handle them we'll
		// include them with low priority because they will certainly
		// be helpful for someone :)
		imageExtensions[4] = "ppm"; // Portable Pixmaps
		imageExtensions[5] = "psd"; // Adobe Photoshop
		imageExtensions[6] = "pcx"; // Zsoft Paintbrush
		imageExtensions[7] = "wal"; // Quake 2 textures


		//
		// The allowed extensions for meshes/models
		//
		static std::vector<std::string> meshExtensions(2, "");

		// Main mesh file formats for OpenStranded
		// As for the images, these are recommended for your meshes in
		// OpenStranded because they are supported by all engines.
		meshExtensions[0] = "b3d";
		meshExtensions[1] = "3ds";
		
		// FIXME: Add the other formats. I don't know which priorities
		// would be reasonable



		//
		// Return one of these vectors, based on the requested type
		//
		switch(type)
		{
			case GFX_TEXTURE:		// FALLTHROUGH!
			case GFX_ICON:			//    |
			case GFX_SPRITE:		//    |
			case GFX_SKY:			//    V
				return imageExtensions; // *splat*
				break;			//---------

			case GFX_MODEL:
				return meshExtensions;
				break;

			case GFX_NONE:
			default:
				return std::vector<std::string>();
				break;
		}
			
	}

	const std::vector<std::string>&
	IrrlichtEngine::getSfxExtensions(SfxType type)
	{
		//
		// The vector for sound files.
		//
		static std::vector<std::string> soundExtensions(3, "");

		// Main sound file formats for OpenStranded.
		// You should use one of these formats for OpenStranded material
		// as they are supported by all engines.
		// OGG Vorbis is recommended to use because it is a free
		// standard with no patents and still has decent quality at low
		// sizes.
		soundExtensions[0] = "ogg";
		soundExtensions[1] = "mp3";
		soundExtensions[2] = "wav";

		return soundExtensions;
	}
		
}

