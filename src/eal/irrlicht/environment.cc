/*
 * Terrain, weather, lights (Irrlicht renderer)
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/irrlicht/environment.hh"
#include "eal/irrlicht/terrainscenenode.hh"
#include "eal/irrlicht/engine.hh"

#include <irrlicht/irrlicht.h>
using namespace irr;

namespace eal
{
	IrrlichtEnvironment::IrrlichtEnvironment()
	{
		terrain = 0;
		terrainsize = 64;
		sky = 0;
	}
	IrrlichtEnvironment::~IrrlichtEnvironment()
	{
	}
	
	void
	IrrlichtEnvironment::setSize(int size)
	{
		terrainsize = size;
		if (!terrain)
			createTerrain();
		else
			terrain->setSize(terrainsize, terrainsize);
	}

	int
	IrrlichtEnvironment::getSize()
	{
		return terrainsize;
	}

	void
	IrrlichtEnvironment::setHeight (const Vector2 &pos, double height)
	{
		if (!terrain)
			createTerrain();

		terrain->setHeight(pos.x, pos.y, height);
	}

	double
	IrrlichtEnvironment::getHeight (const Vector2 &pos)
	{
		if (!terrain)
			createTerrain();

		return terrain->getHeight(pos.x, pos.y);
	}

	void
	IrrlichtEnvironment::setHeightData(float *heightdata)
	{
		terrain->setHeightData(heightdata);
	}

	void
	IrrlichtEnvironment::setColor (const Vector2 &pos, const Color &color)
	{
		if (!terrain)
			createTerrain();

		terrain->setColor(pos.x, pos.y, color.get());
	}

	Color
	IrrlichtEnvironment::getColor (const Vector2 &pos)
	{
	}

	void IrrlichtEnvironment::setColorMap (unsigned char *colormap, int size)
	{
		if (!terrain)
			createTerrain();

		// Convert color map to 32 bit array
		uint32_t *colordata = new uint32_t[size * size];
		for (int i = 0; i < size * size; i++)
		{
			colordata[i] = ((int)colormap[i * 3] << 16)
				+ ((int)colormap[i * 3 + 1] << 8)
				+ (int)colormap[i * 3 + 2];
		}

		// Upload color data
		terrain->setColorMapSize(size, size);
		terrain->setColorData(colordata);
		delete[] colordata;
	}
	void IrrlichtEnvironment::setColorMap (std::string path)
	{
	}

	void
	IrrlichtEnvironment::setSkyCube(std::string sky)
	{
		// Delete old sky cube
		if (this->sky)
		{
			this->sky->remove();
			this->sky = 0;
		}
		// Load textures
		scene::ISceneManager *smgr = ((IrrlichtEngine*)Engine::get())->getSceneManager();
		video::IVideoDriver *driver = smgr->getVideoDriver();
		std::string directory = std::string("mods/") + Engine::get()->getSettings()->getModName() + "/skies/";
		video::ITexture *top = driver->getTexture((directory + sky + "_up.jpg").c_str());
		video::ITexture *bottom = driver->getTexture((directory + sky + "_dn.jpg").c_str());
		video::ITexture *left = driver->getTexture((directory + sky + "_lf.jpg").c_str());
		video::ITexture *right = driver->getTexture((directory + sky + "_rt.jpg").c_str());
		video::ITexture *front = driver->getTexture((directory + sky + "_fr.jpg").c_str());
		video::ITexture *back = driver->getTexture((directory + sky + "_bk.jpg").c_str());
		if (!top || !bottom || !left || !right || !front || !back)
			return;
		this->sky = smgr->addSkyBoxSceneNode(top, bottom, right, left, front, back);
	}

	void
	IrrlichtEnvironment::setFog(const Color &fogcolor)
	{
		scene::ISceneManager *smgr = ((IrrlichtEngine*)Engine::get())->getSceneManager();
		video::SColor color(0, fogcolor.getRed(), fogcolor.getGreen(), fogcolor.getBlue());
		smgr->getVideoDriver()->setFog(color, true, 1500, 3000, 0, true);
	}

	void IrrlichtEnvironment::createTerrain()
	{
		// Create terrain
		scene::ISceneManager *smgr = ((IrrlichtEngine*)Engine::get())->getSceneManager();
		terrain = new IrrlichtTerrainSceneNode(smgr->getRootSceneNode(), smgr, 0, terrainsize, terrainsize);
		terrain->drop();
		terrain->setScale(core::vector3df(64, 3200, 64));
		terrain->setPosition(core::vector3df(-32 * terrainsize, -1600, -32 * terrainsize));
		terrain->setMaterialFlag(video::EMF_FOG_ENABLE, true);
		// Create water
		scene::IAnimatedMesh *watermesh = smgr->addHillPlaneMesh("watermesh",
			core::dimension2d<f32>(1, 1), core::dimension2d<u32>(1, 1));
		water = smgr->addAnimatedMeshSceneNode(watermesh);
		water->setScale(core::vector3df(10000, 1, 10000));
		water->setMaterialFlag(video::EMF_FOG_ENABLE, true);
		video::SMaterial &material = water->getMaterial(0);
		material.Lighting = false;
		video::ITexture *watertex = smgr->getVideoDriver()->addTexture(core::dimension2d<s32>(2, 2), "watertex");
		uint32_t *data = (uint32_t*)watertex->lock(false);
		for (int i = 0; i < 2 * 2; i++)
			data[i] = 0x00529079;
		watertex->unlock();
		material.setTexture(0, watertex);
	}
}
