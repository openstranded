/*
 * This file defines OpenStranded Sounds
 *
 * Copyright (C) 2009 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_SOUND_HH
#define STRANDED_SOUND_HH

#include <string>

namespace eal
{
	class Sound
	{
		public:
			/*
			 * creates a sound object
			 */
			Sound (std::string filePath);

			~Sound ();
	 };
}


#endif /* STRANDED_SOUND_HH */

