/*
 * Base class for graphics/sound
 *
 * Copyright (C) 2009 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eal/engine.hh"

namespace eal
{
	Engine*
	Engine::get ()
	{
		return instance;
	}

	Engine::Engine ()
	{
		gui = 0;
		camera = 0;
		environment = 0;
	}

	Engine::~Engine ()
	{
	}

	GUI*
	Engine::getGUI ()
	{
		return gui;
	}

	Camera*
	Engine::getCamera ()
	{
		return camera;
	}

	Environment*
	Engine::getEnvironment ()
	{
		return environment;
	}

	Engine *Engine::instance = 0;
}
