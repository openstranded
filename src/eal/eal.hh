/*
 * This file includes basic definitions of the Engine Abstraction Layer
 * (EAL)
 *
 * Copyright (C) 2009 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_EAL_HH
#define STRANDED_EAL_HH

#include "vector.hh"
#include "scenenode.hh"
#include "sound.hh"

namespace eal
{
	/*
	 * This is the class representing the Graphical User Interface
	 */
	class GUI
	{
	};
}

#endif /* STRANDED_EAL_HH */

