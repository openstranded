/*
 * See entity.hh for a brief description of the entity class.
 *
 * Copyright (C) 2008 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "output.hh"
#include "entity.hh"
#include "kingdom.hh"
#include "eal/engine.hh"

Entity::Entity (ID type):
	typeID(type)
{}

void
Entity::load(Type *type)
{
	Out::msg.printf("Entity: %s\n", type->getName().c_str());
	std::string model = type->getModel();
	if (model != "")
	{
		scenenode = eal::Engine::get()->addSceneNode();
		scenenode->setModel(model);
	}
}

void
Entity::setPosition(eal::Vector3 position)
{
	this->position = position;
	if (scenenode)
		scenenode->setPosition(position);
}

eal::Vector3
Entity::getPosition()
{
	return position;
}
