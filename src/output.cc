/*
 * See output.hh for more information about this file
 *
 * Copyright (C) 2009 David Kolossa
 *
 * This file is part of OpenStranded.
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include <string>
#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cassert>
#ifndef WIN32
	// Directory handling
	#include <dirent.h>
	#include <sys/stat.h>
	#include <sys/types.h>
#endif
#include <ctime>

#include "filesystem.hh"
#include "output.hh"

// This constant is used for allocating memory in Printer::printf
#define FORMAT_BUFSIZE 100

//
// Printer class
//

Out::Printer::Printer(void printer(std::string))
{
	this->printer = printer;
}

Out::Printer&
Out::Printer::operator<<(std::string msg)
{
	this->printer(msg);
	return *this;
}

Out::Printer&
Out::Printer::operator<<(const char *msg)
{
	this->printer(msg);
	return *this;
}

Out::Printer&
Out::Printer::operator<<(char msg)
{
	this->printer(std::string(1, msg));
	return *this;
}

Out::Printer&
Out::Printer::operator<<(int msg)
{
	std::ostringstream tmp;
	tmp << msg;
	this->printer(tmp.str());
	return *this;
}

Out::Printer&
Out::Printer::operator<<(short msg)
{
	std::ostringstream tmp;
	tmp << msg;
	this->printer(tmp.str());
	return *this;
}

Out::Printer&
Out::Printer::operator<<(long msg)
{
	std::ostringstream tmp;
	tmp << msg;
	this->printer(tmp.str());
	return *this;
}

Out::Printer&
Out::Printer::operator<<(long long msg)
{
	std::ostringstream tmp;
	tmp << msg;
	this->printer(tmp.str());
	return *this;
}

Out::Printer&
Out::Printer::operator<<(unsigned int msg)
{
	std::ostringstream tmp;
	tmp << msg;
	this->printer(tmp.str());
	return *this;
}

Out::Printer&
Out::Printer::operator<<(unsigned short msg)
{
	std::ostringstream tmp;
	tmp << msg;
	this->printer(tmp.str());
	return *this;
}

Out::Printer&
Out::Printer::operator<<(unsigned long msg)
{
	std::ostringstream tmp;
	tmp << msg;
	this->printer(tmp.str());
	return *this;
}

Out::Printer&
Out::Printer::operator<<(unsigned long long msg)
{
	std::ostringstream tmp;
	tmp << msg;
	this->printer(tmp.str());
	return *this;
}

Out::Printer&
Out::Printer::operator<<(float msg)
{
	std::ostringstream tmp;
	tmp << msg;
	this->printer(tmp.str());
	return *this;
}

Out::Printer&
Out::Printer::operator<<(double msg)
{
	std::ostringstream tmp;
	tmp << msg;
	this->printer(tmp.str());
	return *this;
}

int
Out::Printer::vprintf(const char *format, va_list args)
{
	int result;
	int length = std::strlen(format) + FORMAT_BUFSIZE;
	char *buffer = new char[length];

	result = vsprintf(buffer, format, args);

	if (result)
	{
		std::ostringstream tmp;
		tmp << buffer;
		this->printer(tmp.str());
	}
	delete[] buffer;
	return result;
}

int
Out::Printer::printf(const char *format, ...)
{
	int result;
	va_list args;
	va_start(args, format);
	result = this->vprintf(format, args);
	va_end(args);
	return result;
}



//
// Out class
//

Out::Printer Out::fatal = Printer(Out::fatalPrinter);
Out::Printer Out::error = Printer(Out::errorPrinter);
Out::Printer Out::warning = Printer(Out::warningPrinter);
Out::Printer Out::msg = Printer(Out::msgPrinter);
Out::Printer Out::debug = Printer(Out::debugPrinter);

int Out::isWriting = Out::NONE;
int Out::consoleFlags = Out::NONE;
int Out::loggingFlags = Out::NONE;

FILE *Out::logFile = 0;


Out::Out()
{
}

void
Out::setLogFile(std::string fileName)
{
	if (Out::logFile != 0)
	{
		fclose(Out::logFile);
		Out::logFile = 0;
	}
	std::string path = "";
	#ifndef WIN32
		path += std::getenv("HOME");
		path += "/.openstranded/";
		if(opendir(path.c_str()) == NULL)
		{
			FileSystem::initUserDirectory();
		}
			
	#else
		// TODO: Add Windows directory code.
		path = "./";
	#endif

	

	path += fileName;
	Out::logFile = std::fopen(path.c_str(), "a");
	if(Out::logFile == 0)
	{
		std::cerr << "FATAL: Log file could not be opened" << std::endl;
		return;
	}
	
	/*
	 * Timestamp code taken from PeakEngine and modified
	 * <http://peakengine.sourceforge.net/>
	 */
	std::time_t rawtime;
	std::time(&rawtime);
	struct std::tm *timeinfo = std::localtime(&rawtime);
	char timestr[20];
	std::strftime(timestr, 20, "%x %X", timeinfo);
	std::fprintf(Out::logFile, "================\nOpened logfile on %s\n================", timestr);
	std::fflush(Out::logFile);
}

int
Out::getConsoleFlags()
{
	return consoleFlags;
}

void
Out::setConsoleFlags(int newflags)
{
	consoleFlags = newflags;
}

int
Out::getLoggingFlags()
{
	return loggingFlags;
}

void
Out::setLoggingFlags(int newflags)
{
	loggingFlags = newflags;
}

void
Out::fatalPrinter(std::string msg)
{
	if (!(consoleFlags & FATAL) && !(loggingFlags & FATAL))
		return;

	// If another command didn't finish its line, fix it for it
	if((isWriting | FATAL) != FATAL)
	{
		std::cerr << std::endl;
		isWriting = 0;
		debug << "Printed an unfinished line" << endl;
	}

	// Only write prefix one time per line
	if((isWriting & FATAL) == 0)
	{
		msg = "FATAL: " + msg;
	}

	// If msg is a finished line reset flags, else set flags
	if(msg.find('\n') == std::string::npos)
	{
		isWriting = FATAL;
	}
	else
	{
		isWriting = 0;
	}

	if(consoleFlags & FATAL)
	{
		std::cerr << msg;
	}

	if(loggingFlags & FATAL)
	{
		assert(Out::logFile != 0);
		const char *logMsg = msg.c_str();
		fwrite(logMsg, std::strlen(logMsg), sizeof(char),  Out::logFile);
		fflush(Out::logFile);
	}
}

void
Out::errorPrinter(std::string msg)
{
	if (!(consoleFlags & ERROR) && !(loggingFlags & ERROR))
		return;

	// If another command didn't finish its line, fix it for it
	if((isWriting | ERROR) != ERROR)
	{
		std::cerr << std::endl;
		debug << "Printed an unfinished line" << endl;
		isWriting = 0;
	}

	// Only write prefix one time per line
	if((isWriting & ERROR) == 0)
	{
		msg = "ERROR: " + msg;
	}

	// If msg is a finished line reset consoleFlags, else set flags
	if(msg.find('\n') == std::string::npos)
	{
		isWriting = ERROR;
	}
	else
	{
		isWriting = 0;
	}

	if(consoleFlags & ERROR)
	{
		std::cerr << msg;
	}

	if(loggingFlags & ERROR)
	{
		assert(Out::logFile != 0);
		const char *logMsg = msg.c_str();
		fwrite(logMsg, std::strlen(logMsg), sizeof(char),  Out::logFile);
		fflush(Out::logFile);
	}
}

void
Out::warningPrinter(std::string msg)
{
	if (!(consoleFlags & WARNING) && !(loggingFlags & WARNING))
		return;

	// If another command didn't finish its line, fix it for it
	if((isWriting | WARNING) != WARNING)
	{
		std::cerr << std::endl;
		isWriting = 0;
		debug << "Printed an unfinished line" << endl;
	}

	// Only write prefix one time per line
	if((isWriting & WARNING) == 0)
	{
		msg = "WARNING: " + msg;
	}

	// If msg is a finished line reset flags, else set flags
	if(msg.find('\n') == std::string::npos)
	{
		isWriting = WARNING;
	}
	else
	{
		isWriting = 0;
	}

	if(consoleFlags & WARNING)
	{
		std::cerr << msg;
	}

	if(loggingFlags & WARNING)
	{
		assert(Out::logFile != 0);
		const char *logMsg = msg.c_str();
		fwrite(logMsg, std::strlen(logMsg), sizeof(char),  Out::logFile);
		fflush(Out::logFile);
	}
}

void
Out::msgPrinter(std::string msg)
{
	// If another command didn't finish its line, fix it for it
	if((isWriting | MSG) != MSG)
	{
		std::cerr << std::endl;
		isWriting = 0;
		debug << "Printed an unfinished line" << endl;
	}

	// If msg is a finished line reset flags, else set flags
	if(msg.find('\n') == std::string::npos)
	{
		isWriting = MSG;
	}
	else
	{
		isWriting = 0;
	}

	std::cout << msg;
}

void
Out::debugPrinter(std::string msg)
{
	if (!(consoleFlags & DEBUG) && !(loggingFlags & DEBUG))
		return;

	// If another command didn't finish its line, fix it for it
	if((isWriting | DEBUG) != DEBUG)
	{
		std::cerr << std::endl;
		isWriting = 0;
		debug << "Printed an unfinished line" << endl;
	}

	// Only write prefix one time per line
	if((isWriting & DEBUG) == 0)
	{
		msg = "DEBUG: " + msg;
	}

	// If msg is a finished line reset flags, else set flags
	if(msg.find('\n') == std::string::npos)
	{
		isWriting = DEBUG;
	}
	else
	{
		isWriting = 0;
	}

	if(consoleFlags & DEBUG)
	{
		std::cerr << msg;
	}

	if(loggingFlags & DEBUG)
	{
		assert(Out::logFile != 0);
		const char *logMsg = msg.c_str();
		fwrite(logMsg, std::strlen(logMsg), sizeof(char),  Out::logFile);
		fflush(Out::logFile);
	}
}

