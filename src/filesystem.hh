/*
 * This file defines functions to look up file paths.
 *
 * Copyright (C) 2009 David Kolossa
 *
 * This file is part of OpenStranded.
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_FILESYSTEM_HH
#define STRANDED_FILESYSTEM_HH

#include <string>
#include <vector>

enum GfxType
{
	GFX_NONE,
	GFX_TEXTURE,
	GFX_MODEL,
	GFX_ICON,
	GFX_SPRITE,
	GFX_SKY	
};

enum SfxType
{
	SFX_NONE,
	SFX_SPEECH,
	SFX_EFFECT
};

struct DirectoryInfo
{
	std::string path;

	DirectoryInfo(std::string path);
};

struct FileInfo
{
	std::string path;
	int error;

	FileInfo(std::string path, int error);

	bool
	exists();
};

class FileSystem
{
	private:
		static std::string modName;

	public:
		//
		// Initialization
		//
		static void
		setModName(std::string name);

		static bool
		initUserDirectory();

		//
		// Directories
		//
		static DirectoryInfo*
		getUserDirectoryInfo();

		static DirectoryInfo*
		getGameDirectoryInfo();

		static DirectoryInfo*
		getModDirectoryInfo();



		//
		// General files
		//
		static FileInfo*
		getSaveFileInfo(std::string name);

		static FileInfo*
		getMapFileInfo(std::string name);

		//
		// GFX files, wrappers for getGameFileInfo()
		//
		static FileInfo*
		getGfxFileInfo(std::string name, GfxType type);

		static FileInfo*
		getTextureFileInfo(std::string name);

		static FileInfo*
		getModelFileInfo(std::string name);

		static FileInfo*
		getIconFileInfo(std::string name);

		static FileInfo*
		getSpriteFileInfo(std::string name);

		static FileInfo*
		getSkyFileInfo(std::string name);



		//
		// SFX files
		//
		static FileInfo*
		getSfxFileInfo(std::string name, SfxType type);

		static FileInfo*
		getSpeechFileInfo(std::string name);

		static FileInfo*
		getEffectFileInfo(std::string name);

	private:
		/**
		 * Return a vector with all files in a directory.
		 * The returned pointer to the vector needs to be freed by the
		 * calling function.
		 * @param dirPath The path of the directory
		 * @return A vector with the files of the filesystem without
		 * . and ..
		 */
		static std::vector<std::string> *
		getDirectoryVector(std::string dirPath);

		/**
		 * Check whether a filename string with a name and an extension
		 * exists in a vector of const char*.
		 * This method is case-insensitive on Windows and case-sensitive
		 * in all other cases. The vector for the extensions also 
		 * defines a priority on which element is returned. The element 
		 * with the smaller index is returned when it comes to 
		 * conflicts.
		 * @param dirVector A pointer to a vector as returned by 
		 * getDirectoryVector()
		 * @param extensions A vector with the extensions ordered by
		 * priority
		 * @param name The name of the file which is searched
		 * @return The matching string, "" if there is none.
		 */
		static std::string
		getFileNameFromDirectoryVector(std::vector<std::string> *dirVector, const std::vector<std::string> &extensions, std::string name);

		/**
		 * Check whether name has a directory in it and extract it.
		 * For example: "path/to/file" returns "path/to/".
		 * A simple "file" would return "".
		 */
		static std::string
		getDirectoryPart(std::string name);

		/**
		 * The reverse of getDirectoryPart(), this returns the file part.
		 * For example: "path/to/file" returns "file".
		 * "path/to/directory/" returns "".
		 */
		static std::string
		getFilePart(std::string name);
};

#endif /* STRANDED_FILESYSTEM_HH */

