/*
 *This file includes the basic classes for map/savegame loading
 *
 * Copyright (C) 2008 Mathias Gottschlag
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_MAP_HH
#define STRANDED_MAP_HH

#include "entity.hh"
#include "eal/environment.hh"
#include <string>
#include <list>

class Map
{
	protected:
		std::list<Entity*> entities;
		eal::Environment *environment;
	public:
		Map ();
		~Map ();

		/**
		 * Loads a map from the given path.
		 * @return true if the map was loaded successfully
		 */
		bool
		load (std::string path);
		/**
		 * Saves a map to the given path, including all entities currently on the
		 * map.
		 * @return true if the map was saved successfully
		 */
		bool
		save (std::string path);
		/**
		 * Destroys all loaded entities and additional data.
		 */
		void
		destroy ();
};

#endif
