/*
 * This file includes the base class definition for entity kingdoms.
 * Kingdoms group entities roughly by their most basic similiarities.
 * The name 'Kingdom' is derived from the biological term.
 *
 * Copyright (C) 2008 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_KINGDOM_HH
#define STRANDED_KINGDOM_HH

#include <string>
#include <map>
#include "entity.hh"
#include "type.hh"

class Kingdom;

typedef unsigned int ID;


/**
 * This is a base class for all kingdoms.
 * Originally implemented kingdoms were, roughly summarized, the following:
 * 	Units as the only mobile and intelligent (or controllable) entities,
 * 	Items as storable tools or resources,
 * 	Objects as immobile structures, flora, rocks or similiar, and
 * 	Infos as meta-information for technical purposes
 * The OpenStranded Kingdom system is, however, designed to make it possible
 * to implement new kingdoms at will.
 */
class Kingdom
{
	private:
		std::string name;
	
		/*
		 * Kingdoms manage all their entities and types
		 * with pointers in local lists (STL Maps)
		 * where they are mapped to certain IDs.
		 * Using pointers may not be optimal OOP,
		 * but other methods aren't as efficient here.
		 */
		std::map <ID, Entity*> entityList;
		std::map <ID, Type*> typeList;

		ID highestEntityID;

	public:
		/**
		 * The constructor.
		 * @param name The name of the kingdom
		 */
		Kingdom (const char *name);

		/**
		 * The destructor.
		 * Entities and Types are most likely to be allocated on the heap, 
		 * so this destructor is used to free them again
		 */
		virtual
		~Kingdom ();

		/**
		 * Looks up a certain entity from the kingdom's list by its ID.
		 * @param index The ID of the desired entity
		 * @return A pointer to the entity, NULL if none is found
		 */
		Entity*
		getEntity (ID index);

		/**
		 * Looks up a certain type from the kingdom's list by its ID
		 * @param index The ID of the desired type
		 * @return A pointer to the type, NULL if none is found
		 */
		Type*
		getType (ID index);

		/**
		 * Checks whether an entity with a certain ID exists
		 * @param entity ID of the entity
		 * @return true if the entity exists, false if not
		 */
		bool
		entityExists (ID entity);

		/**
		 * Checks whether a type with a certain ID exists
		 * @param type ID of the type
		 * @return true if the type exists, false if not
		 */
		bool
		typeExists (ID type);

		/**
		 * Append an entity to the list.
		 * This method automatically assigns an ID which is by 
		 * one higher than the currently highest entity ID
		 * @param type The ID of the type of the entity
		 * @return A pointer to the appended entity
		 */
		Entity*
		appendEntity (ID type);

		/**
		 * Insert an entity into a specific position of the list.
		 * This method assigns the specified ID to the entity.
		 * @param type The ID of the type of the entity
		 * @param entity The desired ID of the entity in the kingdom
		 * @return A pointer to the inserted entity
		 */
		Entity*
		insertEntity (ID type, ID entity);

		/**
		 * Insert a type.
		 * TypeIDs need to be certain and definite towards the outside,
		 * so there is only an insertType method defining a certain ID.
		 * @param name The name of the type
		 * @param type The desired ID of the type
		 * @return A pointer to the inserted type
		 */
		Type*
		insertType (const char* name, ID type);

		/**
		 * Insert a type without a name.
		 * @param type The desired ID of the type
		 * @return A pointer to the inserted type
		 */
		Type*
		insertType (ID type);

		/*
		 * These are some virtual functions used by other methods.
		 * They differ from kingdom to kingdom.
		 * Basically, they should just implement the 
		 * entity's and type's constructors/destructors for this kingdom
		 */

		virtual Entity*
		createEntity (ID type);

		virtual void
		destroyEntity (Entity *entity);

		virtual Type*
		createType (const char* name);

		virtual void
		destroyType (Type *type);



		//
		// Static class members
		//

		static std::map <ID, Kingdom*> kingdomList;

		static void
		initKingdomList (std::string mod = "Stranded II");

		static void
		uninitKingdomList ();

		static void
		parseEntityTypes(std::string mod, std::string name);
};

#endif /* STRANDED_KINGDOM_HH */
