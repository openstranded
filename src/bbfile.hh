/*
 * This file includes the class declaration of a Blitz Basic file
 *
 * Copyright (C) 2008 David Kolossa
 *
 * This file is part of OpenStranded.
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_BBFILE_HH
#define STRANDED_BBFILE_HH

#include <stdint.h>
#include <fstream>

/**
 * This is a wrapper class for Blitz3d files.
 * It is used to provide compatibility with files created by
 * Blitz Basic 3d (e.g. save files)
 */
class BBFile
{
	private:
		/**
		 * The file object for access to the Blitz Basic file
		 */
		std::fstream file;

	public:
		/**
		 * The constructor.
		 * Opens a Blitz Basic compatible file.
		 * @param filePath Path to the file to be opened
		 */
		BBFile (std::string filePath);

		/**
		 * The destructor.
		 * Closes the opened file.
		 */
		~BBFile ();



		//
		// Check for file stream flags
		//

		/**
		 * Check fail flag.
		 */
		inline bool
		fail() { return file.fail(); }

		/**
		 * Check EOF flag.
		 */
		inline bool
		eof() { return file.eof(); }

		/**
		 * Check bad flag.
		 */
		inline bool
		bad() { return file.bad(); }

		/**
		 * Check good flag.
		 */
		inline bool
		good() { return file.good(); }



		//
		// Overloading the ! operator
		//
		bool
		operator!();



		//
		// Overloading the extraction operator
		//

		/**
		 * Read a string from the file.
		 * Note: You can't read lines using this operator, use readLine() instead.
		 * @param strBuf The buffer into which the string is written
		 * @return A reference to the BBFile object
		 * @see readLine()
		 * @see readString()
		 */
		BBFile&
		operator>> (std::string &strBuf);

		/**
		 * Read a uint8_t (In B3d: Byte) from the file.
		 * @param byteBuf The buffer into which the uint8_t is written
		 * @return A reference to the BBFile object
		 * @see readByte()
		 */
		BBFile&
		operator>> (uint8_t &byteBuf);

		/**
		 * Read a uint16_t (In B3d: Short) from the file.
		 * @param shortBuf The buffer into which the uint16_t is written
		 * @return A reference to the BBFile object
		 * @see readShort()
		 */
		BBFile&
		operator>> (uint16_t &shortBuf);

		/**
		 * Read an int32_t (In B3d: Int) from the file.
		 * @param intBuf The buffer into which the int32_t is written
		 * @return A reference to the BBFile object
		 * @see readInt()
		 */
		BBFile&
		operator>> (int32_t &intBuf);

		/**
		 * Read a float (In B3d: Float) from the file.
		 * @param floatBuf Th buffer into which the float is written
		 * @return A reference to the BBFile object
		 * @see readFloat()
		 */
		BBFile&
		operator>> (float &floatBuf);



		//
		// Overloading the insertion operator
		//

		/**
		 * Write a string to the file.
		 * Note: You can't write lines with that operator, use writeLine() instead.
		 * @param strBuf The buffer from which the string is read
		 * @return A reference to the BBFile object
		 * @see writeString()
		 * @see writeLine()
		 */
		BBFile&
		operator<< (std::string &strBuf);

		/**
		 * Write a uint8_t (In B3d: Byte) to the file.
		 * @param byteBuf The buffer from which the uint8_t is read
		 * @return A reference to the BBFile object
		 * @see writeByte()
		 */
		BBFile&
		operator<< (uint8_t &byteBuf);

		/**
		 * Write a uint16_t (In B3d: Short) to the file.
		 * @param shortBuf The buffer from which the uint16_t is read
		 * @return A reference to the BBFile object
		 * @see writeShort()
		 */
		BBFile&
		operator<< (uint16_t &shortBuf);

		/**
		 * Write a int32_t (In B3d: Integer) to the file.
		 * @param intBuf The buffer from which the int32_t is read
		 * @return A reference to the BBFile object
		 * @see writeInt()
		 */
		BBFile&
		operator<< (int32_t &intBuf);

		/**
		 * Write a float (In B3d: Float) to the file.
		 * @param floatBuf The buffer from which the float is read
		 * @return A reference to the BBFile object
		 * @see writeFloat()
		 */
		BBFile&
		operator<< (float &floatBuf);



		//
		// Actual file reading methods
		//

		/**
		 * Reads a line.
		 * A line is terminated by the ASCII symbols CR (0x0d) and LF (0x0a).
		 * The read data is removed from the stream.
		 * @return The read string
		 */
		std::string
		readLine ();

		/**
		 * Reads a string.
		 * A string is preceded by its length as an int32_t. This method reads the
		 * string and removes it (including length indicator) from the stream.
		 * @return The read string
		 */
		std::string
		readString ();

		/**
		 * Reads a uint8_t (B3d: Byte) value from the stream and removes it afterwards.
		 * @return The read uint8_t value.
		 */
		uint8_t
		readByte ();

		/**
		 * Reads a uint16_t (B3d: Short) value from the stream and removes it afterwards.
		 * @return The read short value.
		 */
		uint16_t
		readShort ();

		/**
		 * Reads an int32_t (B3d: Int) value from the stream and removes it afterwards.
		 * @return The read int32_t value.
		 */
		int32_t
		readInt ();

		/**
		 * Reads a float value from the stream and removes it afterwards.
		 * @return The read float value.
		 */
		float
		readFloat ();

		/**
		 * Reads raw data from the stream.
		 * @param buffer Buffer to which the bytes are written
		 * @param size Amount of data to be read
		 */
		void
		readData(void *buffer, uint32_t size);

		//
		// Actual file writing methods
		//

		/**
		 * Writes a string terminated by the ASCII symbols CR (0x0d) and LF (0x0a)
		 * to the stream.
		 * @param lineWritten The line to be written
		 */
		void
		writeLine (std::string &lineWritten);

		/**
		 * Writes a string to the stream.
		 * @param strWritten The string to be written
		 */
		void
		writeString (std::string &strWritten);

		/**
		 * Writes a uint8_t (B3d: Byte) to the stream.
		 * @param byteWritten The uint8_t to be written
		 */
		void
		writeByte (uint8_t &byteWritten);

		/**
		 * Writes a uint16_t (B3d: Short) to the stream.
		 * @param shortWritten The uint16_t to be written
		 */
		void
		writeShort (uint16_t &shortWritten);

		/**
		 * Writes a int32_t (B3d: Int) to the stream.
		 * @param intWritten The int32_t to be written
		 */
		void
		writeInt (int32_t &intWritten);

		/**
		 * Writes a float to the stream.
		 * @param floatWritten The float to be written
		 */
		void
		writeFloat (float &floatWritten);



		//
		// Input validation
		//

		/**
		 * Checks whether a string is a valid B3d line
		 * @param line The string to be checked
		 * @return true if line is a valid b3d line, false if not
		 */
		bool
		validLine (std::string &line);

};

#endif /* STRANDED_BBFILE_HH */
