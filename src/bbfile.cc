/*
 * This file includes the implementation of the functions of the
 * BBFile class.
 *
 * Copyright (C) 2008 David Kolossa
 *
 * This file is part of OpenStranded.
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "bbfile.hh"

BBFile::BBFile (std::string filePath)
{
	file.open (filePath.c_str(), std::fstream::binary | std::fstream::in | std::fstream::out);
	if(!file)
	{
		// Try again. Read-only...
		file.open (filePath.c_str(), std::fstream::binary | std::fstream::in);
	}	
}

BBFile::~BBFile ()
{
	file.close ();
}

bool
BBFile::operator! ()
{
	return !(this->file);
}

BBFile&
BBFile::operator>> (std::string &strBuf)
{
	strBuf = this->readString ();
	return *this;
}

BBFile&
BBFile::operator>> (uint8_t &byteBuf)
{
	byteBuf = this->readByte ();
	return *this;
}

BBFile&
BBFile::operator>> (uint16_t &shortBuf)
{
	shortBuf = this->readShort ();
	return *this;
}

BBFile&
BBFile::operator>> (int32_t &intBuf)
{
	intBuf = this->readInt ();
	return *this;
}

BBFile&
BBFile::operator>> (float &floatBuf)
{
	floatBuf = this->readFloat ();
	return *this;
}

BBFile&
BBFile::operator<< (std::string &strBuf)
{
	this->writeString (strBuf);
	return *this;
}

BBFile&
BBFile::operator<< (uint8_t &byteBuf)
{
	this->writeByte (byteBuf);
	return *this;
}

BBFile&
BBFile::operator<< (uint16_t &shortBuf)
{
	this->writeShort (shortBuf);
	return *this;
}

BBFile&
BBFile::operator<< (int32_t &intBuf)
{
	this->writeInt (intBuf);
	return *this;
}

BBFile&
BBFile::operator<< (float &floatBuf)
{
	this->writeFloat (floatBuf);
	return *this;
}

std::string
BBFile::readLine ()
{
	std::string lineRead;

	getline (file, lineRead, '\r');
	file.get ();	// remove \n on stream

	return lineRead;
}

std::string
BBFile::readString ()
{
	std::string stringRead;
	int32_t size;

	size = this->readInt ();
	for (int32_t i=0; i < size; i++)
	{
		stringRead += (char) file.get ();
	}

	return stringRead;
}

uint8_t
BBFile::readByte ()
{
	uint8_t byteRead;

	file.read ((char*) &byteRead, sizeof(uint8_t));
	return byteRead;
}

uint16_t
BBFile::readShort ()
{
	uint16_t shortRead;

	file.read ((char*) &shortRead, sizeof(uint16_t));
	return shortRead;
}

int32_t
BBFile::readInt ()
{
 	int32_t intRead;

 	file.read ((char*) &intRead, sizeof(int32_t));
	return intRead;

}

float
BBFile::readFloat ()
{
	float floatRead;

	file.read ((char*) &floatRead, sizeof(float));
	return floatRead;
}

void
BBFile::readData(void *buffer, uint32_t size)
{
	file.read((char*)buffer, size);
}

void
BBFile::writeLine (std::string &lineWritten)
{
	file.write (lineWritten.c_str(), lineWritten.size ());
	file.write ("\r\n", 2 * sizeof(char));
}

void
BBFile::writeString (std::string &strWritten)
{
	int32_t size = (int32_t) strWritten.size ();

	this->writeInt (size);
	file.write (strWritten.c_str (), size);
}

void
BBFile::writeByte (uint8_t &byteWritten)
{
	file.write ((char*) &byteWritten, sizeof(uint8_t));
}

void
BBFile::writeShort (uint16_t &shortWritten)
{
	file.write ((char*) &shortWritten, sizeof(uint16_t));
}

void
BBFile::writeInt (int32_t &intWritten)
{
	file.write ((char*) &intWritten, sizeof(int32_t));
}

void
BBFile::writeFloat (float &floatWritten)
{
	file.write ((char*) &floatWritten, sizeof(float));
}

bool
BBFile::validLine (std::string &line)
{
	return ((line.find ('\r') == std::string::npos) && (line.find ('\n') == std::string::npos));
}
