/*
 * See kingdom.hh for a brief description of kingdoms.
 *
 * Copyright (C) 2008 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "output.hh"
#include "kingdom.hh"
#include "object.hh"
#include "unit.hh"
#include "item.hh"
#include "defparse/defparse.hh"

#include <iostream>

#ifdef unix
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <dirent.h>
	#include <fcntl.h>
	#include <string.h>
#else
	#include <windows.h>
#endif


std::map <ID, Kingdom*> Kingdom::kingdomList;


Kingdom::Kingdom(const char *name):
	name(std::string(name))
{
	highestEntityID = 0;
}


Kingdom::~Kingdom ()
{
	std::map <ID, Type*>::iterator type;
	for (type = typeList.begin (); type != typeList.end (); type++)
		destroyType (type->second);

	std::map <ID, Entity*>::iterator entity;
	for (entity = entityList.begin (); entity != entityList.end (); entity++)
		destroyEntity (entity->second);
}


Entity*
Kingdom::getEntity (ID index)
{
	if (entityExists (index))
		return entityList[index];
	else
		return NULL;
}


Type*
Kingdom::getType (ID index)
{
	if (typeExists (index))
		return typeList[index];
	else
		return NULL;
}


bool
Kingdom::entityExists (ID entity)
{
	if (entityList.find (entity) != entityList.end ())
		return true;
	else
		return false;
}


bool
Kingdom::typeExists (ID type)
{
	if (typeList.find (type) != typeList.end ())
		return true;
	else
		return false;
}


Entity*
Kingdom::appendEntity (ID type)
{
	return entityList[++highestEntityID] = createEntity (type);
}


Entity*
Kingdom::insertEntity (ID type, ID entity)
{
	if (!entityExists (entity))
		entityList[entity] = createEntity (type);
	
	if (entity > highestEntityID)
		highestEntityID = entity;

	return entityList[entity];
}


Type*
Kingdom::insertType (const char* name, ID type)
{
	if (!typeExists (type))
		typeList[type] = createType (name);
	return typeList[type];
}


Type*
Kingdom::insertType (ID type)
{
	return this->insertType ("", type);
}


Entity*
Kingdom::createEntity (ID type)
{
	return new Entity (type);
}


void
Kingdom::destroyEntity (Entity *entity)
{
	delete entity;
}


Type*
Kingdom::createType (const char* name)
{
	return new Type (name);
}


void
Kingdom::destroyType (Type *type)
{
	delete type;
}

/* 
 * Functions for the kingdomList
 * TODO: In terms of beauty and modularity,
 * these functions should be outsourced into another file
 */
void
Kingdom::initKingdomList (std::string mod)
{
	kingdomList[S2_OBJECT] = new ObjectKingdom;
	kingdomList[S2_UNIT] = new UnitKingdom;
	kingdomList[S2_ITEM] = new ItemKingdom;

	// Load entity types
	parseEntityTypes(mod, "object");
}


void
Kingdom::uninitKingdomList ()
{
	std::map <ID, Kingdom*>::iterator iter;
	for (iter = kingdomList.begin (); iter != kingdomList.end (); iter++)
		delete (iter->second);
}

void
Kingdom::parseEntityTypes(std::string mod, std::string name)
{
	std::string sysdir = std::string("mods/") + mod + "/sys/";
	// Search for entity files within sys/
	#ifdef __unix__
	DIR *dir = opendir(sysdir.c_str());
	if (!dir)
	{
		std::cerr << "Could not read sys/.\n";
		std::cerr << sysdir << "\n";
		return;
	}

	while (1)
	{
		struct dirent *dirent = readdir(dir);
		if (!dirent) break;
		if (strcmp(dirent->d_name, "..") && strcmp(dirent->d_name, "."))
		{
			std::string filename = sysdir + dirent->d_name;
			int file = open(filename.c_str(), O_RDONLY);
			if (!file)
			{
				Out::msg.printf("Could not open file \"%s\".\n", filename.c_str());
				continue;
			}
			struct stat stat;
			fstat(file, &stat);
			close(file);

			if (stat.st_mode & S_IFREG)
			{
				if (!strncmp(dirent->d_name, name.c_str(), strlen(name.c_str()))
					&& !strcmp(dirent->d_name + strlen(dirent->d_name) - 4, ".inf"))
				{
					Out::msg.printf("Parsing %s\n", filename.c_str());
					def::object::parse(filename.c_str());
				}
			}
		}
	}
	closedir(dir);
	#else
	std::string pattern = sysdir + name + "s_*.inf";
	WIN32_FIND_DATA finddata;
	HANDLE findhandle = FindFirstFile(pattern.c_str(), &finddata);
	if (findhandle != INVALID_HANDLE_VALUE)
	{
		def::object::parse(finddata.cFileName);
		while (FindNextFile(findhandle, &finddata))
		{
			def::object::parse(finddata.cFileName);
		}
		FindClose(findhandle);
	}
	#endif
}
