/*
 * See settings.hh for information about this file.
 *
 * Copyright (C) 2008 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <iostream>
#include "tinyxml/tinyxml.h"
#include "settings.hh"
#include "s2string.hh"
#include "output.hh"

GlobalSettings::GlobalSettings(std::string configPath) :
	configPath(configPath)
{
	this->initializeStandardValues();
	this->readConfigFile();
}

GlobalSettings::~GlobalSettings()
{}

void
GlobalSettings::initializeStandardValues()
{
	this->setResolutionX(1024);
	this->setResolutionY(768);
	this->setColorDepth(32);
	this->setFullScreen(false);

	#ifdef HAVE_IRRLICHT
	this->setRenderer("Irrlicht");
	#elif defined(HAVE_HORDE3D)
	this->setRenderer("Horde3D");
	#else
	this->setRenderer("");
	#endif

	this->setViewRange(2);

	this->setTerrainDetail(1);
	this->setWaterDetail(3);
	this->setSkyDetail(1);
	this->setEffectDetail(1);
	this->setGrassDetail(2);

	this->setMusicVolume(1.0);
	this->setSoundVolume(1.0);

	this->setPp2d(true);
	this->setLightFx(true);
	this->setWindSwayFx(true);
	this->setFog(true);
	this->setMultiTex(true);
	this->setMotionBlur(true);
	this->setMotionBlurIntensity(1.0);

	this->setPlayerName("Mr. Stranded");
	this->setPort(39369);
}

bool
GlobalSettings::updateConfigFile()
{
	TiXmlDocument xml;

	// Create XML declaration
	TiXmlDeclaration *declaration = new TiXmlDeclaration("1.0", "utf-8", "");
	xml.LinkEndChild(declaration);

	// Create <config> environment
	TiXmlElement *root = new TiXmlElement("config");
	xml.LinkEndChild(root);

	// Traverse setting map
	for (std::map<std::string, std::string>::iterator i = this->settings.begin(); i != this->settings.end(); i++)
	{
		std::string substrings[2];
		int flag;
		std::string key = std::string(i->first);
		std::string value = std::string(i->second);

		flag = s2str::explode(key, substrings, "_", 2);
		if (inFlag(flag, EXPLODE_GREATER) || inFlag(flag, EXPLODE_LOWER) )
		{
			Out::debug << "Malformed setting name, skipped it." << Out::endl;
			continue;
		}
		else if (inFlag(flag, EXPLODE_NO_TOKEN))
		{
			// TODO: Catch special case of categoryless setting
			Out::debug << "Not Implemented: Categoryless setting, skipped it" << Out::endl;
			continue;
		}
		else
		{
			// Check for existing category
			TiXmlElement *categoryElement = NULL;
			TiXmlNode *categoryNode = NULL;
			while((categoryNode = root->IterateChildren(categoryNode)) != 0)
			{
				categoryElement = categoryNode->Clone()->ToElement();
				if (std::string(categoryElement->Attribute("name")) == substrings[0])
					break;
				delete categoryElement;
				categoryElement = NULL;
			}
			delete categoryElement;
			categoryElement = NULL;

			// No matching category found: Create it
			if (categoryNode == NULL)
			{
				categoryElement = new TiXmlElement("category");
				categoryElement->SetAttribute("name", substrings[0].c_str());
				root->LinkEndChild(categoryElement);
			}
			else
			{
				categoryElement = categoryNode->ToElement();
			}	


			// Create setting
			TiXmlElement *element = new TiXmlElement("setting");
			element->SetAttribute("name", substrings[1].c_str());

			// Set the type of the setting
			// FIXME: This is both BUGGY and UGLY
			if (s2str::isInteger(value))
			{
				element->SetAttribute("type", "int");
			}
			else if (s2str::isDecimal(value))
			{
				element->SetAttribute("type", "float");
			}
			else if (s2str::isBool(value))
			{
				element->SetAttribute("type", "bool");
			}
			else
			{
				element->SetAttribute("type", "string");
			}

			categoryElement->LinkEndChild(element);

			// Store value in setting
			TiXmlText *text = new TiXmlText(value.c_str());
			element->LinkEndChild(text);
		
		}
	}

	if (!xml.SaveFile(this->configPath.c_str()))
	{
		Out::fatal << "Error when writing config file: " << xml.ErrorDesc() << Out::endl;
		return false;
	}

	return true;
}

bool
GlobalSettings::readConfigFile()
{
	TiXmlDocument xml(this->configPath.c_str());
	// Check for regular file problems.
	if (!xml.LoadFile())
	{
		Out::fatal << "Error when reading config file: " << xml.ErrorDesc() << Out::endl;
		return false;
	}

	TiXmlHandle handle(&xml);
	TiXmlElement *root;

	root = handle.FirstChild("config").ToElement();

	// Make sure that the top-level of our XML file is <config>
	if (!root)
	{
		Out::fatal << "Invalid Markup: Top level element in config file is not <config> or missing" << Out::endl;
		return false;
	}

	for (TiXmlElement *e = root->FirstChild("category")->ToElement(); e; e = e->NextSiblingElement())
	{
		for (TiXmlElement *f = e->FirstChild("setting")->ToElement(); f; f = f->NextSiblingElement())
		{
			std::string category = std::string(e->Attribute("name"));
			std::string type = std::string(f->Attribute("type"));
			std::string name = std::string(f->Attribute("name"));
			std::string value = std::string(f->GetText());
			if (type == "int")
			{	
				if (s2str::isInteger(value))
				{
					this->setSetting(category + "_" + name, value);
				}
				else
				{
					Out::error << "Invalid int value for setting with name \"" << name << "\" in config file!" << Out::endl;
				}
				
			}
			else if (type == "float")
			{
				if (s2str::isDecimal(value))
				{
					this->setSetting(category + "_" + name, value);
				}
				else
				{
					Out::error << "Invalid float value for setting with name \"" << name << "\" in config file!" << Out::endl;
				}
			}
			else if (type == "bool")
			{
				if (s2str:: isBool(value))
				{
					this->setSetting(category + "_" + name, value);
				}
				else
				{
					Out::error << "Invalid bool value for setting with name \"" << name << "\" in config file!" << Out::endl;
				}
			}
			else if (type == "string")
			{
				this->setSetting(category + "_" + name, value);
			}
			else
			{
				Out::error << "Invalid type of setting with name \"" << name << "\" in config file!" << Out::endl;
			}
		}
	}

	return true;
}

std::string
GlobalSettings::getSetting(const std::string &setting)
{
	std::map<std::string, std::string>::iterator settingIterator;
	if (settings.empty())
		return "";

	settingIterator = settings.find(setting);
	if (settingIterator != settings.end())
		return settingIterator->second;
	else
		return "";
}

std::string
GlobalSettings::getTemporary(const std::string &setting)
{
	std::map<std::string, std::string>::iterator settingIterator;
	if (tempSettings.empty())
		return "";

	settingIterator = tempSettings.find(setting);
	if (settingIterator != tempSettings.end())
		return settingIterator->second;
	else
		return "";
}

void
GlobalSettings::setSetting(const std::string &setting, const std::string &settingString)
{
	settings[setting] = settingString;
}

void
GlobalSettings::setTemporary(const std::string &setting, const std::string &settingString)
{
	tempSettings[setting] = settingString;
}


