/*
 * This is a module for testing the lexer's functionality
 *
 * Copyright (C) 2008-2009 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "s2script.h"
#include "s2s.tab.h"

extern int yylex_init (void **scanner);
extern int yylex (s2s_val *yylval, void *scanner);
extern int yylex_destroy (void *scanner);

int yywrap ()
{
  return 1;
}

int main (int argc, char* argv[])
{
#	define LEX_KEYWORD(K) if (yyresult == K) printf ("Keyword: %s\n", #K)

  void *scanner;
	yylex_init (&scanner);
	s2s_val yylval;
	int yyresult = 1;

  while (yyresult != 0) {
    yyresult = yylex (&yylval, scanner);
    if (yyresult == STRING)
      printf ("String: \"%s\"\n", yylval.val.string);
    else if (yyresult == NUM)
      printf ("Numerical value: %d\n", (int) yylval.val.num);
    else if (yyresult == FLOAT)
      printf ("Floating-point value: %.2f\n", yylval.val.fnum);
    else if (yyresult == VAR)
      printf ("Variable: $%s\n", yylval.val.string);
    else if (yyresult == FUNCNAME)
      printf ("Function: %s\n", yylval.val.string);
		else LEX_KEYWORD (AND);
		else LEX_KEYWORD (OR);
		else LEX_KEYWORD (XOR);
		else LEX_KEYWORD (IF);
		else LEX_KEYWORD (ELSEIF);
		else LEX_KEYWORD (ELSE);
		else LEX_KEYWORD (LOOP);
		else LEX_KEYWORD (EQ);
		else LEX_KEYWORD (GT);
		else LEX_KEYWORD (LT);
		else LEX_KEYWORD (GE);
		else LEX_KEYWORD (LE);
		else LEX_KEYWORD (NEQ);
		else LEX_KEYWORD (ASSOC_ADD);
		else LEX_KEYWORD (ASSOC_SUB);
		else LEX_KEYWORD (ASSOC_MUL);
		else LEX_KEYWORD (ASSOC_DIV);
		else LEX_KEYWORD (INCREMENT);
		else LEX_KEYWORD (DECREMENT);
    else
      printf ("Character: %c\n", yyresult);
  }
#	undef LEX_KEYWORD
	yylex_destroy (scanner);
	return 0;
}
