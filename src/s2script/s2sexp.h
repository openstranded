/*
 * This header defines functions used by the parser for expression evaluation
 *
 * Copyright (C) 2009 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __S2SEXP_H__
#define __S2SEXP_H__
#include "s2script.h"

/*
 * Arithmetics
 */

S2S_FUNC s2s_val
exp_add (s2s_val val1, s2s_val val2);

S2S_FUNC s2s_val
exp_subtract (s2s_val val1, s2s_val val2);

S2S_FUNC s2s_val
exp_multiply (s2s_val val1, s2s_val val2);

S2S_FUNC s2s_val
exp_divide (s2s_val val1, s2s_val val2);

S2S_FUNC s2s_val
exp_negate (s2s_val val);
/*
 * Logic operators
 */

S2S_FUNC s2s_val
exp_and (s2s_val val1, s2s_val val2);

S2S_FUNC s2s_val
exp_or (s2s_val val1, s2s_val val2);

S2S_FUNC s2s_val
exp_xor (s2s_val val1, s2s_val val2);

/*
 * Comparison
 */

/**
 * Compare two s2s_val values
 * @param val1 First value to compare
 * @param val2 Second value to compare
 * @return -1 if val1<val2; 0 if val1==val2; +1 if val1>val2
 */
S2S_FUNC s2s_val
exp_compare (s2s_val val1, s2s_val val2);

#endif /* __S2SEXP_H__ */
