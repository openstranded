/*
 * This header declares the input/output functions used by the parser
 *
 * Copyright (C) 2009 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef S2S_YYIO_H
#define S2S_YYIO_H

#include "s2script.h"

// Format string printing functions

S2S_FUNC int
yyprintf(const char *format, ...);

S2S_FUNC int
yywarnf(const char *format, ...);

S2S_FUNC int
yyerrorf(const char *format, ...);

S2S_FUNC int
yydebugf(const char *format, ...);

// bison requires the following function with exactly these parameters
S2S_FUNC void
yyerror(void *scanner, const char *message);

#endif /* S2S_YYIO_H */
