/*
 * This file declares structures and functions commonly used by
 * the script parser
 *
 * Copyright (C) 2008-2009 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _S2S_SCRIPT_H_
#define _S2S_SCRIPT_H_

#ifdef __cplusplus
#define S2S_FUNC extern "C"
#else
#define S2S_FUNC
#include <stdlib.h>
typedef enum {
	false = 0,
	true = 1,
} bool;
#endif

/*******************************
 * Type definitions            *
 *******************************/

// S2S type indicators
typedef enum s2stype
{
  S2S_STRING_T,
  S2S_INT_T,
  S2S_FLOAT_T,
	S2S_ERROR_T,
	S2S_VOID_T,
	S2S_OBJECT_T,
} s2stype;

typedef struct
{
  // type indicates which field of val should be used
	// and what meaning it has
	// if type is S2S_VOID_T, ignore val completely
	s2stype type;
  union
  {
    char *string; // use if type is S2S_STRING_T or S2S_ERROR_T
    long num; // use if type is S2S_INT_T
    double fnum; // use if type is S2S_FLOAT_T
		struct {
			void *ptr; // pointer to custom structure/object
			// descriptive type identifier string
			// use the same identifier for objects of the same class
			char *type;
		} object; // use if type is S2S_OBJECT_T
  } val;
} s2s_val;

#define YYSTYPE s2s_val

// Argument stacks (implemented externally)
typedef struct s2s_argstack_t
s2s_argstack_t;

// Function pointers
typedef s2s_val
(*s2s_func_t) (s2s_argstack_t*);

// Variable lists (implemented externally)
typedef struct s2s_varlist_t
s2s_varlist_t;

/**********************************
 * Function definitions           *
 **********************************/

/*
 * functions to handle s2s_val objects
 */
static inline bool
s2s_val_isstring (const s2s_val val)
{
	return val.type == S2S_STRING_T;
}

static inline bool
s2s_val_isint (const s2s_val val)
{
	return val.type == S2S_INT_T;
}

static inline bool
s2s_val_isfloat (const s2s_val val)
{
	return val.type == S2S_FLOAT_T;
}

static inline bool
s2s_val_isnumeric (const s2s_val val)
{
	return s2s_val_isint (val) || s2s_val_isfloat (val);
}

static inline bool
s2s_val_iserror (const s2s_val val)
{
	return val.type == S2S_ERROR_T;
}

static inline bool
s2s_val_isvoid (const s2s_val val)
{
	return val.type == S2S_VOID_T;
}

static inline bool
s2s_val_isobject (const s2s_val val)
{
	return val.type == S2S_OBJECT_T;
}

static inline s2s_val
s2s_val_buildstring (char* s)
{
	s2s_val result;
	result.type = S2S_STRING_T;
	result.val.string = s;
	return result;
}

static inline s2s_val
s2s_val_buildint (long int n)
{
	s2s_val result;
	result.type = S2S_INT_T;
	result.val.num = n;
	return result;
}

static inline s2s_val
s2s_val_buildfloat (double f)
{
	s2s_val result;
	result.type = S2S_FLOAT_T;
	result.val.fnum = f;
	return result;
}

static inline s2s_val
s2s_val_builderror (char* s)
{
	s2s_val result;
	result.type = S2S_ERROR_T;
	result.val.string = s;
	return result;
}

static inline s2s_val
s2s_val_buildvoid ()
{
	s2s_val result;
	result.type = S2S_VOID_T;
	return result;
}
	
static inline s2s_val
s2s_val_buildobject (void *o, char* t)
{
	s2s_val result;
	result.type = S2S_OBJECT_T;
	result.val.object.ptr = o;
	result.val.object.type =t;
	return result;
}

S2S_FUNC s2s_val
s2s_val_copy (const s2s_val val);

S2S_FUNC void
s2s_val_free (s2s_val *val);

/*
 * Functions to deal with the scripting function list
 */

/**
 * Registers the given function in the parser's scripting function list
 * @param name The function's name (which is used to identify it later)
 * @param function Pointer to a function of type s2s_func_t
 */
S2S_FUNC void
s2s_register_function (char *name, s2s_func_t function);

/**
 * Find a scripting function that has been registered earlier
 * @param name The function name to look for
 */
S2S_FUNC s2s_func_t
s2s_find_function (char *name);

/**
 * Delete all entries from the scripting function list
 */
S2S_FUNC void
s2s_free_funcs ();

/*
 * Functions to deal with argument stacks
 */

/**
 * Create a new argument stack
 * @return Pointer to the new argument stack
 */
S2S_FUNC s2s_argstack_t *
s2s_new_argstack ();

/**
 * Pop the topmost element from an argument stack
 * @param stack Pointer to the argument stack to pop from
 * @return An s2s_val object representing the popped element
 */
S2S_FUNC s2s_val
s2s_argstack_pop (s2s_argstack_t *stack);

/**
 * Push a new element on an argument stack
 * @param stack Pointer to the stack to push
 * @param value The s2s_val object to push
 */
S2S_FUNC void
s2s_argstack_push (s2s_argstack_t *stack, s2s_val value);

/**
 * Return the size of an argument stack
 * @param stack The argument stack in question
 * @return The number of elements contained by the stack
 */
S2S_FUNC size_t
s2s_argstack_size (s2s_argstack_t *stack);
/**
 * Destroy an argument stack
 * @param stack The stack to destroy
 */
S2S_FUNC void
s2s_free_argstack (s2s_argstack_t *stack);

/*
 * Functions to deal with variable lists
 */

/**
 * Create a new variable list
 * @return The variable list created
 */
S2S_FUNC s2s_varlist_t *
s2s_new_varlist ();

/**
 * Destroy a variable list
 * @param varlist The list to destroy
 */
S2S_FUNC void
s2s_free_varlist (s2s_varlist_t *varlist);

/**
 * Insert a new variable into a variable list
 * @param varlist The list to add the variable to
 * @param name A string which identifies the variable
 * @return Pointer to the new variable's value, modifiable
 */
S2S_FUNC s2s_val*
s2s_new_var (s2s_varlist_t *varlist, char *name);

/**
 * Look for a variable by its name
 * @param varlist The list containing the variable
 * @param name The variable's name
 * @return Pointer to the found variable's value, NULL if not found
 */
S2S_FUNC s2s_val*
s2s_find_var (s2s_varlist_t *varlist, char *name);

/**
 * Remove a variable from a list
 * @param varlist The variable list to remove from
 * @param name The variable's name
 */
S2S_FUNC void
s2s_free_var (s2s_varlist_t *varlist, char *name);
#endif /* _S2S_SCRIPT_H_ */
