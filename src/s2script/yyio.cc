/*
 * See yyio.h for more information about this file
 *
 * Copyright (C) 2009 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdarg>
#include "yyio.h"
#include "../output.hh"

S2S_FUNC int
yyprintf(const char *format, ...)
{
	int result;
	va_list args;
	va_start(args, format);
	result = Out::msg.vprintf(format, args);
	va_end(args);
	return result;
}

S2S_FUNC int
yywarnf(const char *format, ...)
{
	int result;
	va_list args;
	va_start(args, format);
	result = Out::warning.vprintf(format, args);
	va_end(args);
	return result;
}

S2S_FUNC int
yyerrorf(const char *format, ...)
{
	int result;
	va_list args;
	va_start(args, format);
	result = Out::error.vprintf(format, args);
	va_end(args);
	return result;
}

S2S_FUNC int
yydebugf(const char *format, ...)
{
	int result;
	va_list args;
	va_start(args, format);
	result = Out::debug.vprintf(format, args);
	va_end(args);
	return result;
}

S2S_FUNC void
yyerror(void *scanner, const char *message)
{
}
