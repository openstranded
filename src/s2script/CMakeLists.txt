SET(CC cc)
SET(LEX flex)
SET(BISON bison)

SET(DIST_ARCHIVE s2sparser.tar.gz)
SET(DIST_FILES
	s2s.y
	s2s.l
	lextest.c
	parsetest.c
	listtest.c
	s2script.h
	s2script.cc
)

SET(PARSER_MODULES
	s2s.tab.c
	lex.yy.c
	s2script.cc
	s2sexp.c
)

add_custom_command(
	OUTPUT lex.yy.c
	COMMAND ${LEX} s2s.l
	DEPENDS s2s.l s2s.tab.h
)
	
add_custom_command(
	OUTPUT s2s.tab.h s2s.tab.c
	COMMAND ${BISON} -v s2s.y
	DEPENDS s2s.y
)

add_library(s2s SHARED ${PARSER_MODULES})

add_executable(lexer lex.yy.c lextest.c)

add_executable(parser parsetest.c)

target_link_libraries(parser s2s)

add_executable(listtest s2script.cc listtest.c)

add_custom_target(dist
	tar -czf ${DIST_ARCHIVE} ${DIST_FILES}
)
