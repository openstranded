/*
 * This is the lexical analyzer of openstranded's script parser
 *
 * Copyright (C) 2008-2009 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

%{
  #include "s2script.h"
  #include "s2s.tab.h"
  #define STRBUF_INITSIZE 16
%}

%option reentrant bison-bridge
%option noyywrap nounput

IDENTIFIER    [a-zA-Z_][a-zA-Z0-9_]*
INTEGERNUM    [0-9]+
WHITESPACE    [\|\n \t]
ANYCHAR       [^\|\n \t]

%x QUOTE
%x BLOCKCOMMENT
%x LINECOMMENT
%%
	int strcount, strsize;
	char* strbuf;

\${IDENTIFIER}		{
			yylval->type = S2S_STRING_T;
			yylval->val.string = (char*) malloc (strlen (yytext));
			strcpy (yylval->val.string, yytext + 1);
			return VAR;
			}

<INITIAL>\"		{
			BEGIN(QUOTE);
			strcount = 0;
			strsize = STRBUF_INITSIZE;
			strbuf = (char*) malloc (strsize);
			}

<QUOTE>\"		{
			BEGIN(INITIAL);
			yylval->type = S2S_STRING_T;
			yylval->val.string = realloc (strbuf, strcount + 1);
			yylval->val.string[strcount] = '\0';
			return STRING;
			}

<QUOTE>(.|\n)		{
			strbuf[strcount++] = yytext[0];
			if (strcount >= strsize)
			  strbuf = realloc (strbuf, strsize += STRBUF_INITSIZE);
			}

<INITIAL>(\/\/)		{
			BEGIN(LINECOMMENT);
			}

<INITIAL>\/\*		{
			BEGIN(BLOCKCOMMENT);
			}

<LINECOMMENT>(\n|\|)	{
			BEGIN(INITIAL);
			}

<BLOCKCOMMENT>\*\/	{
			BEGIN(INITIAL);
			}

<LINECOMMENT,BLOCKCOMMENT>(.|\n) {}

(\&\&)|(and)		return AND;
(\|\|)|(or)		return OR;
(xor)			return XOR;

(if)			return IF;
(elseif)	return ELSEIF;
(else)		return ELSE;
(loop)		return LOOP;	

{IDENTIFIER}		{
			yylval->type = S2S_STRING_T;
			yylval->val.string = (char*) malloc (strlen (yytext) + 1);
			strcpy (yylval->val.string, yytext);
			return FUNCNAME;
			}

{INTEGERNUM}?\.{INTEGERNUM} {
			yylval->type = S2S_FLOAT_T;
			yylval->val.fnum = atof (yytext);
			return FLOAT;
			}

{INTEGERNUM}		{
			yylval->type = S2S_INT_T;
			yylval->val.num = atol (yytext);
			return NUM;
			}

(\+\+)		return INCREMENT;
(--)			return DECREMENT;
(\+=)			return ASSOC_ADD;
(-=)			return ASSOC_SUB;
(\*=)			return ASSOC_MUL;
(\/=)			return ASSOC_DIV;

(==)			return EQ;

(\!=|\<\>)			return NEQ;

\<			return LT;

\>			return GT;

(<=)|(=<)		return LE;

(>=)|(=>)		return GE;


{ANYCHAR}		{
			return yytext[0];
			}

{WHITESPACE}		{}
