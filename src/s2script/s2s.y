/*
 * This is the script parser's grammar file
 *
 * Copyright (C) 2008-2009 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

%{
	#include <stdio.h>
	#include "s2script.h"
	#include "s2slib.h"
	#include "yyio.h"
  extern int yylex (s2s_val *yylval, void *scanner);
%}

%defines
%pure-parser
%parse-param {void *scanner}
%lex-param {void *scanner}

%token VAR FUNCNAME
%token NUM FLOAT STRING
%token IF ELSEIF ELSE LOOP LOCAL
%token INCREMENT DECREMENT

%left '=' ASSOC_ADD ASSOC_SUB ASSOC_MUL ASSOC_DIV
%left OR AND XOR
%left EQ NEQ LT GT LE GE
%left '+' '-'
%left '*' '/'

%%

input: 
	statementlist { printf ("End of input\n"); }

statementlist:
	/* empty */ { printf ("Begin of input\n>>> "); }
	| statementlist statement { printf ("Complete statement\n>>> "); }
;


/*
	 Kinds of statements I can think of just now:
	 * func1 "foo";
	 * func2 ("bar");
	 * @func3 ();

	 * local $var;
	 * $var = "value";
   * $var += 5;
   * $var++;

	 * if (expr) { foo; }
	   elseif (expr) { bar; }
		 else { foobar; }
	 * loop ("count", 10) { foobar; }
 */

statement:
	'@' FUNCNAME args ';' { printf ("Silenced function call\n"); }
	| FUNCNAME args ';' { printf ("Function call\n"); }
	| funccall ';' { printf ("Function call\n"); }
	| assignment ';' { printf ("Assignment statement\n"); }
	| conditional
	| loop '{' statementlist '}' { printf ("End of loop block\n"); }
;

assignment:
	VAR '=' expr { printf ("Simple assignment\n"); }
	| VAR ASSOC_ADD expr { printf ("Addition assignment\n"); }
	| VAR ASSOC_SUB expr { printf ("Substraction assignment\n"); }
	| VAR ASSOC_MUL expr { printf ("Multiplication assignment\n"); }
	| VAR ASSOC_DIV expr { printf ("Division assignment\n"); }
	| INCREMENT VAR { printf ("Prefix Increment\n"); }
	| DECREMENT VAR { printf ("Prefix Decrement"); }
	| VAR INCREMENT { printf ("Postfix Increment\n"); }
	| VAR DECREMENT { printf ("Postfix Decrement\n"); }
;

funccall:
	FUNCNAME '(' args ')' { printf ("Returning function call\n"); }
	| '@' FUNCNAME '(' args ')' { printf ("Silenced returning function call\n"); }
;

args:
	/* empty */	{ printf ("Empty arglist\n"); }
	| expr ',' args { printf ("One more argument\n"); }
	| expr { printf ("One argument\n"); }
;

conditional:
	ifcond elseifconds elsecond

ifcond:
	IF '(' expr ')' '{' statementlist '}'

elseifconds:
	/* empty */
	| elseifconds ELSEIF '(' expr ')' '{' statementlist '}'
;

elsecond:
	ELSE '{' statementlist '}'

loop:
	LOOP '(' args ')' { printf ("Begin of loop block\n"); }

expr:
	NUM		{
		$$ = $1;
		printf ("Integer literal: %d\n", (int) $$.val.num);
	}
	| FLOAT		{
		$$ = $1;
		printf ("Float literal: %f\n", $$.val.fnum);
	}
	| STRING		{
		$$ = $1;
		printf ("String literal: %s\n", $$.val.string);
	}
	| '-' expr { printf ("Negation\n"); }
	| '(' expr ')' { printf ("Expression in Parantheses\n"); }
	| VAR { printf ("Variable: %s\n", $1.val.string); }
	| funccall { printf ("Function call in expression\n"); }
	| binop
;

binop:
	expr '+' expr {
		$$ = exp_add ($1, $3);
		s2s_val_free (&$1); s2s_val_free (&$3);
		if (s2s_val_iserror ($$)) 
			{ yyerror (scanner, $$.val.string); s2s_val_free (&$$); YYERROR; }
		else if (s2s_val_isint ($$))
			{ printf ("Integer Addition: %ld\n", $$.val.num); }
		else if (s2s_val_isfloat ($$))
			{ printf ("Float Addition: %lf\n", $$.val.fnum); }
		else if (s2s_val_isstring ($$))
			{ printf ("Concatenation: %s\n", $$.val.string); }
	}
	| expr '-' expr {
		$$ = exp_subtract ($1, $3);
		s2s_val_free (&$1); s2s_val_free (&$3);
		if (s2s_val_iserror ($$))
			{ yyerror (scanner, $$.val.string); s2s_val_free (&$$); YYERROR; }
		else if (s2s_val_isint ($$))
			{ printf ("Integer Subtraction: %ld\n", $$.val.num); }
		else if (s2s_val_isfloat ($$))
			{ printf ("Float Subtraction: %lf\n", $$.val.fnum); }
	}
	| expr '*' expr { printf ("Multiplication\n"); }
	| expr '/' expr { printf ("Division\n"); }
	| expr AND expr { printf ("Logical and\n"); }
	| expr OR expr { printf ("Logical or\n"); }
	| expr XOR expr { printf ("Logical xor\n"); }
	| comparison { printf ("Comparison\n"); }
;

comparison:
	expr EQ expr { printf ("Equality test\n"); }
	| expr NEQ expr { printf ("Inequality test\n"); }
	| expr GT expr { printf ("Greater than test\n"); }
	| expr LT expr { printf ("Lower than test\n"); }
	| expr GE expr { printf ("Greater or equal test\n"); }
	| expr LE expr { printf ("Lower or equal test\n"); }
;
