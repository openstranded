#include <stdio.h>
#include "s2script.h"

s2s_val
echo (s2s_argstack_t* stack)
{
	size_t stacksize = s2s_argstack_size (stack);
	int i;
	s2s_val element;
	for (i=0; i<stacksize; i++)
	{
		element = s2s_argstack_pop (stack);
		if (s2s_val_isstring(element))
			printf ("%s", element.val.string);
		else if (s2s_val_isint(element))
			printf ("%ld", element.val.num);
		else if (s2s_val_isfloat(element))
			printf ("%lf", element.val.fnum);
		if (i+1<stacksize)
			printf (", ");
	}
	printf ("\n");
	return s2s_val_buildvoid ();
}

void
stacktest ()
{
	printf ("Testing argument stacks...\n");
	printf ("Testing equivalent of 'echo 1, 1.5, \"Hello World\";':\n");

	// pass arguments from right to left
	s2s_val argument;

	s2s_argstack_t *argstack = s2s_new_argstack ();
	
	argument = s2s_val_buildstring ("Hello World");
	s2s_argstack_push (argstack, argument);

	argument = s2s_val_buildfloat (1.5);
	s2s_argstack_push (argstack, argument);

	argument = s2s_val_buildint (1);
	s2s_argstack_push (argstack, argument);

	echo (argstack);
	s2s_free_argstack (argstack);

	printf ("Done testing argument stacks.\n\n");
}

void
functest ()
{
	printf ("Testing function lists...\n");
	
	printf ("Adding 'echo' to function list\n");
	s2s_register_function ("echo", &echo);
	
	printf ("Testing equivalent of 'echo \"Hello World\";'\n");
	s2s_argstack_t *argstack = s2s_new_argstack ();
	s2s_val argument = s2s_val_buildstring ("Hello World");
	s2s_argstack_push (argstack, argument);
	s2s_func_t function = s2s_find_function ("echo");
	argument = (*function) (argstack);
	s2s_free_argstack (argstack);
	
	printf ("Done testing function lists.\n\n");
}

void
vartest ()
{
	s2s_val testval = s2s_val_buildstring ("Hello World");
	
	printf ("Testing variable lists...\n");

	printf ("Creating variable list...\n");
	s2s_varlist_t *varlist = s2s_new_varlist ();
	
	printf ("Executing equivalent of '$foo = \"Hello World\";'\n");
	s2s_val *var = s2s_new_var (varlist, "foo");
	*var = s2s_val_copy (testval);
	
	printf ("Executing equivalent of 'echo $foo;'\n");
	var = s2s_find_var (varlist, "foo");
	printf ("%s\n", var->val.string);

	printf ("Deallocating variable list...\n");
	s2s_free_varlist (varlist);

	printf ("Done testing variable lists.\n\n");
}

int
main (int argc, char** argv)
{
	printf ("Beginning tests...\n\n");
	stacktest ();
	functest ();
	vartest ();
	printf ("All tests done\n");
	return 0;
}
