/*
 * This module implements the functions declared in s2script.h
 *
 * Copyright (C) 2009 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <map>
#include <stack>
#include <cstring>
#include <string>
#include "s2script.h"

/*
 * s2s_val methods
 */

S2S_FUNC s2s_val
s2s_val_copy (const s2s_val value)
{
	if (s2s_val_isstring (value) || s2s_val_iserror (value))
	{
		int len = std::strlen (value.val.string) + 1;
		char *newstring = new char[len];
		strcpy (newstring, value.val.string);
		s2s_val newval = {value.type, {newstring}};
		return newval;
	}
	return value;
}

S2S_FUNC void
s2s_val_free (s2s_val *value)
{
	if (s2s_val_isstring (*value) || s2s_val_iserror (*value))
		delete[] value->val.string;
	value->type = S2S_VOID_T;
}

/*
 * function list
 */

static std::map<std::string, s2s_func_t>
s2s_funclist;

S2S_FUNC void
s2s_register_function (char *name, s2s_func_t function)
{
	if (s2s_funclist.count (std::string(name)) == 0)
		s2s_funclist[std::string(name)] = function;
}

S2S_FUNC s2s_func_t
s2s_find_function (char *name)
{
	if (s2s_funclist.count (std::string(name)) == 1)
		return s2s_funclist[std::string(name)];
	else
		return NULL;
}

S2S_FUNC void
s2s_free_funcs ()
{
	s2s_funclist.clear ();
}

/*
 * variable list functions
 */

typedef std::map<std::string, s2s_val>
s2s_varlist_map;

S2S_FUNC s2s_varlist_t *
s2s_new_varlist ()
{
	return (s2s_varlist_t*) new s2s_varlist_map;
}

S2S_FUNC void
s2s_free_varlist (s2s_varlist_t *varlist)
{
	// Deallocate all string variables before calling the map's destructor
	s2s_varlist_map::iterator iter;
	for (iter = ((s2s_varlist_map*) varlist)->begin ();
			iter != ((s2s_varlist_map*) varlist)->end (); iter++)
		s2s_val_free (&iter->second);
	delete (s2s_varlist_map*) varlist;
}

S2S_FUNC s2s_val*
s2s_new_var (s2s_varlist_t *varlist, char *name)
{
	return & ((*(s2s_varlist_map*) varlist)[std::string(name)]);
}

S2S_FUNC s2s_val*
s2s_find_var (s2s_varlist_t *varlist, char *name)
{
	if (((s2s_varlist_map*) varlist)->count (std::string(name)) == 0)
		return NULL;
	return & ((*(s2s_varlist_map*) varlist)[std::string(name)]);
}

S2S_FUNC void
s2s_free_var (s2s_varlist_t *varlist, char *name)
{
	if (((s2s_varlist_map*) varlist)->count(std::string(name)) == 0) return;
	s2s_val value = (*(s2s_varlist_map*) varlist)[std::string(name)];
	s2s_val_free (&value);
	((s2s_varlist_map*) varlist)->erase (std::string(name));
}

/*
 * argument stack functions
 */

typedef std::stack<s2s_val>
s2s_stack;

S2S_FUNC s2s_argstack_t *
s2s_new_argstack ()
{
	return (s2s_argstack_t*) new s2s_stack;
}

S2S_FUNC s2s_val
s2s_argstack_pop (s2s_argstack_t *argstack)
{
	s2s_val top = ((s2s_stack*) argstack)->top ();
	((s2s_stack*) argstack)->pop ();
	return top;
}

S2S_FUNC void
s2s_argstack_push (s2s_argstack_t *argstack, s2s_val value)
{
	((s2s_stack*) argstack)->push (value);
}

S2S_FUNC size_t
s2s_argstack_size (s2s_argstack_t *argstack)
{
	return ((s2s_stack*) argstack)->size ();
}

S2S_FUNC void
s2s_free_argstack (s2s_argstack_t *argstack)
{
	delete (s2s_stack*) argstack;
}
