/*
 * This is a module for testing the script parser's functionality
 *
 * Copyright (C) 2008-2009 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "s2script.h"

extern int yyparse (void *scanner);
extern int yylex_init (void **scanner);
extern int yylex_destroy (void *scanner);

void
yyerror (void *scanner, const char *s)
{
	fprintf (stderr, "%s\n", s);
}

int 
main (int argc, char *argv[])
{
	void *scanner;
	yylex_init (&scanner);
	int result = yyparse (scanner);
	yylex_destroy (scanner);
	return result;
}
