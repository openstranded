/*
 * This module implements the functions declared in s2sexp.h
 *
 * Copyright (C) 2009 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "s2sexp.h"

// the following macro will be used several times in this module
// depending on the type of its s2s_val argument,
// it will return either its floating point component 
// or its integer component
#define s2s_val_getnumeric(x) \
	(s2s_val_isfloat (x)	? x.val.fnum : x.val.num)


S2S_FUNC s2s_val
exp_add (s2s_val val1, s2s_val val2)
{
	if (s2s_val_isnumeric (val1) && s2s_val_isnumeric (val2))
	{
		// add two numbers
		if (s2s_val_isfloat (val1) || s2s_val_isfloat (val2))
			// result will be floating point
		{
			double result;
			// at least one operand is floating point, 
			// but either may be integer
			result  = (double) s2s_val_getnumeric (val1) + 
				(double) s2s_val_getnumeric (val2);
			return s2s_val_buildfloat (result);
		}
		else
			// both operands are integer, result will be integer
		{
			long result = val1.val.num + val2.val.num;
			return s2s_val_buildint (result);
		}
	}
	else if (s2s_val_isstring (val1) && s2s_val_isstring (val2))
		// add/concatenate two strings
	{
		// allocate enough memory to hold both strings
		int len1 = strlen (val1.val.string);
		int len2 = strlen (val2.val.string);
		char *result = (char*) malloc ((len1+len2+1) * sizeof(char));
		
		// copy val1 into result, then append val2 to result
		strcpy (result, val1.val.string);
		strcat (result, val2.val.string);

		return s2s_val_buildstring (result);
	}

	// Return error if val1 and val2 are incompatible
	s2s_val err = s2s_val_builderror ("Trying to add two incompatible types");
	return s2s_val_copy (err);
}

S2S_FUNC s2s_val
exp_subtract (s2s_val val1, s2s_val val2)
{
	// check types
	if (!s2s_val_isnumeric (val1) || !s2s_val_isnumeric (val2))
	{
		s2s_val err = s2s_val_builderror (
				"Trying to subtract non-numeric types");
		return s2s_val_copy (err);
	}
	if (s2s_val_isfloat (val1) || s2s_val_isfloat (val2))
		// result will be float
	{
		double result;
		// either value may be integer
		result  = (double) s2s_val_getnumeric (val1) -
			(double) s2s_val_getnumeric (val2);
		return s2s_val_buildfloat (result);
	}
	else
		// result will be integer
	{
		long result = val1.val.num - val2.val.num;
		return s2s_val_buildint (result);
	}
}

S2S_FUNC s2s_val
exp_multiply (s2s_val val1, s2s_val val2)
{
	// check types
	if (!s2s_val_isnumeric (val1) || !s2s_val_isnumeric (val2))
	{
		s2s_val err = s2s_val_builderror (
				"Trying to multiply non-numeric types");
		return s2s_val_copy (err);
	}
	if (s2s_val_isfloat (val1) || s2s_val_isfloat (val2))
		// result will be float
	{
		double result;
		// either value may be integer
		result = (double) s2s_val_getnumeric (val1) *
		 	(double) s2s_val_getnumeric (val2);
		return s2s_val_buildfloat (result);
	}
	else
		// result will be integer
	{
		long result = val1.val.num * val2.val.num;
		return s2s_val_buildint (result);
	}
}

S2S_FUNC s2s_val
exp_divide (s2s_val val1, s2s_val val2)
{
	// check types
	if (!s2s_val_isnumeric (val1) || !s2s_val_isnumeric (val2))
	{
		s2s_val err = s2s_val_builderror (
				"Trying to divide non-numeric types");
		return s2s_val_copy (err);
	}
	if (s2s_val_isfloat (val1) || s2s_val_isfloat (val2))
		// result will be float
	{
		double result;
		// either value may be integer
		result = (double) s2s_val_getnumeric (val1) /
		 	(double) s2s_val_getnumeric (val2);
		return s2s_val_buildfloat (result);
	}
	else
		// result will be integer
	{
		long result = val1.val.num / val2.val.num;
		return s2s_val_buildint (result);
	}
}

S2S_FUNC s2s_val
exp_negate (s2s_val val)
{
	return exp_subtract (s2s_val_buildint (0), val);
}

S2S_FUNC s2s_val
exp_and (s2s_val val1, s2s_val val2)
{
	// check argument types
	if (!s2s_val_isint (val1) || !s2s_val_isint (val2))
	{
		s2s_val err = s2s_val_builderror (
				"Trying to logically and noninteger types");
		return s2s_val_copy (err);
	}
	long result = val1.val.num && val2.val.num;
	return s2s_val_buildint (result);
}

S2S_FUNC s2s_val
exp_or (s2s_val val1, s2s_val val2)
{
	// check argument types
	if (!s2s_val_isint (val1) || !s2s_val_isint (val2))
	{
		s2s_val err = s2s_val_builderror (
				"Trying to logically or noninteger types");
		return s2s_val_copy (err);
	}
	long result = val1.val.num || val2.val.num;
	return s2s_val_buildint (result);
}

S2S_FUNC s2s_val
exp_xor (s2s_val val1, s2s_val val2)
{	
	// check argument types
	if (!s2s_val_isint (val1) || !s2s_val_isint (val2))
	{
		s2s_val err = s2s_val_builderror (
				"Trying to logically xor noninteger types");
		return s2s_val_copy (err);
	}
	long result;
	if (val1.val.num == 0 && val2.val.num == 0)
		result = 1;
	else if (val1.val.num != 0 && val2.val.num != 0)
		result = 1;
	else
		result = 0;
	return s2s_val_buildint (result);
}

S2S_FUNC s2s_val
exp_compare (s2s_val val1, s2s_val val2)
{
	if (s2s_val_isnumeric (val1) && s2s_val_isnumeric (val2))
	{
		// compare two numbers
		long result;
		// check if val1 < val2
		if (s2s_val_getnumeric (val1) < s2s_val_getnumeric (val2))
			result = -1;
		// check if val1 > val2
		else if (s2s_val_getnumeric (val1) > s2s_val_getnumeric (val2))
			result = 1;
		// val1 equals val2
		else
			result = 0;
	}
	else if (s2s_val_isstring (val1) && s2s_val_isstring (val2))
		// compare two strings
	{
		long result = strcmp (val1.val.string, val2.val.string);
		return s2s_val_buildint (result);
	}

	// Return error if val1 and val2 are incompatible
	s2s_val err = s2s_val_builderror (
			"Trying to compare two incompatible types");
	return s2s_val_copy (err);
}
