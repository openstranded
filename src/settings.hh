/*
 * This file defines the class used to store the global game settings.
 *
 * Copyright (C) 2009 David Kolossa
 *
 * This file is part of OpenStranded.
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_SETTINGS_HH
#define STRANDED_SETTINGS_HH

#include <stdint.h>
#include <string>
#include <sstream>
#include <map>
#include "s2string.hh"

/**
 * This class stores and manages the global settings.
 */
class GlobalSettings
{
	private:
		/**
		 * The path to the configuration file
		 */
		std::string configPath;

		/**
		 * This map holds all the settings which come and go to a
		 * config file
		 */
		std::map <std::string, std::string> settings;

		/**
		 * This map holds all other global settings which are not stored
		 * in a config file.
		 */
		std::map <std::string, std::string> tempSettings;
		
	public:
		GlobalSettings(std::string configPath);
		~GlobalSettings();

		/**
		 * Update the configuration file.
		 * This is called whenever the configuration is saved.
		 */
		bool
		updateConfigFile();

		/**
		 * Read the configuration from the config file.
		 * This is called when starting the game or discarding 
		 * configurations in the configuration dialogue.
		 */
		bool
		readConfigFile();

		/**
		 * Initialize all values to standard values.
		 * You can call this if the reading of the config file fails.
		 * This is also the constructor's behaciour on reading errors.
		 */
		void
		initializeStandardValues();



		//
		// Standard getting and setting methods
		//

		/**
		 * Get a certain setting string.
		 * @param setting The setting which should be returned
		 * @return The setting string
		 */
		std::string
		getSetting(const std::string &setting);

		/**
		 * Set a certain setting to a certain seting string.
		 * This setting will be saved to a configuration file the next
		 * time updateConfigFile() is called  and will therefore be
		 * available after the next starts of the game.
		 * @param setting The setting which should be set
		 * @param settingString The new setting string
		 */
		void
		setSetting(const std::string &setting, const std::string &settingString);

		/**
		 * Get a temporary setting.
		 */
		std::string
		getTemporary(const std::string &setting);

		/**
		 * Set a temporary setting.
		 * This setting will not be saved to the configuration file.
		 */
		void
		setTemporary(const std::string &setting, const std::string &settingString);


		//
		// Screen resolution and color depth
		//

		/**
		 * Get the X component of the screen resolution.
		 * @return The X component of the screen resolution
		 */
		inline int
		getResolutionX()
		{ 
			int tmp;
			std::istringstream(settings["resolution_x"]) >> tmp;
			return tmp;
		}

		/**
		 * Get the Y component of the screen resolution.
		 * @return The Y component of the screen resolution
		 */
		inline int
		getResolutionY()
		{
			int tmp;
			std::istringstream(settings["resolution_y"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the X component of the screen resolution.
		 * @param resolutionX The new value for the X component
		 * of the screen resolution.
		 */
		inline void
		setResolutionX(int resolutionX)
		{ 
			std::ostringstream tmp;
			tmp << resolutionX;
			settings["resolution_x"] = tmp.str();
		}

		/**
		 * Set the Y component of the screen resolution.
		 * @param resolutionY The new value for the Y component
		 * of the screen resolution.
		 */
		inline void
		setResolutionY(int resolutionY)
		{
			std::ostringstream tmp;
			tmp << resolutionY;
			settings["resolution_y"] = tmp.str();
		}

		/**
		 * Get the color depth.
		 * The color depth is measured in bits per pixel (bpp)
		 * including alpha channel.
		 * @return The current color depth in bpp
		 */
		inline int
		getColorDepth()
		{
			int tmp;
			std::istringstream(settings["resolution_colordepth"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the color depth.	
		 * The color depth is measured in bits per pixel (bpp)
		 * including alpha channel.
		 * @param colorDepth The new value for the color depth
		 */
		inline void
		setColorDepth(int colorDepth)
		{
			std::ostringstream tmp;
			tmp << colorDepth;
			settings["resolution_colordepth"] = tmp.str();
		}

		/**
		 * Check whether fullscreen mode is enabled.
		 * @returns true if activated, false if not
		 */
		inline bool
		getFullScreen()
		{
			bool tmp;
			s2str::stob(settings["resolution_fullscreen"], tmp);
			return tmp;	
		} 

		/**
		 * Set fullscreen mode.
		 * @param true to enable, false to disable
		 */
		inline void
		setFullScreen(bool fullScreen)
		{
			settings["resolution_fullscreen"] = (fullScreen ? "true" : "false");
		}

		/** 
		 * Toggle fullscreen mode.
		 * This is equivalent to setFullScreen(!getFullScreen())
		 */
		inline void
		toggleFullScreen()
		{
			setFullScreen(!getFullScreen());
		}

		/**
		 * Get renderer.
		 * Possible values:
		 * Irrlicht - The Irrlicht3d engine.
		 * Horde3D - The Horde3D engine.
		 * @return The renderer string.
		 */
		inline std::string
		getRenderer()
		{
			return settings["view_renderer"];
		}

		/**
		 * Set renderer.
		 * @param The renderer string.
		 * @see getRenderer()
		 */
		inline void
		setRenderer(std::string renderer)
		{
			this->settings["view_renderer"] = renderer;
		}
		


		//
		// View range
		//
		
		/**
		 * Get the view range.
		 * Possible values:
		 * 0 - very low,
		 * 1 - low,
		 * 2 - normal,
		 * 3 - far,
		 * 4 - very far.
		 * @return The player's current view range
		 */
		inline int
		getViewRange()
		{
			int tmp;
			std::istringstream(settings["view_range"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the view range.
		 * See getViewRange() for details about the values.
		 * @param viewRange The player's new view range value
		 * @see getViewRange()
		 */
		inline void
		setViewRange(int viewRange)
		{
			std::ostringstream tmp;
			tmp << viewRange;
			settings["view_range"] = tmp.str();
		}



		//
		// Terrain detail
		//

		/**
		 * Get the terrain detail level.
		 * Possible values:
		 * 0 - low,
		 * 1 - normal,
		 * 2 - high.
		 * @return The current terrain detail level.
		 */
		inline int
		getTerrainDetail()
		{
			int tmp;
			std::istringstream(settings["detail_terrain"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the terrain detail level.
		 * See getTerrainDetail() for details about the values.
		 * @param terrainDetail The new detail level.
		 * @see getTerrainDetail()
		 */
		inline void
		setTerrainDetail(int terrainDetail)
		{
			std::ostringstream tmp;
			tmp << terrainDetail;
			settings["detail_terrain"] = tmp.str();
		}



		//
		// Water detail
		//
	
		/**
		 * Get the water detail level.
		 * Possible values:
		 * 0 - minimal,
		 * 1 - very low,
		 * 2 - low,
		 * 3 - normal,
		 * 4 - high,
		 * 5 - very high,
		 * 6 - maximal.
		 * @return The current water detail level.
		 */
		inline int
		getWaterDetail()
		{
			int tmp;
			std::istringstream(settings["detail_water"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the water detail level.
		 * See getWaterDetail() for details about the values.
		 * @param waterDetail The new water detail level.
		 * @see getWaterDetail()
		 */
		inline void
		setWaterDetail(int waterDetail)
		{
			std::ostringstream tmp;
			tmp << waterDetail;
			settings["detail_water"] = tmp.str();
		}



		//
		// Sky detail
		//

		/**
		 * Get the sky detail level.
		 * Possible values:
		 * 0 - low,
		 * 1 - normal,
		 * 2 - high.
		 * @return The current sky detail level
		 */
		inline int
		getSkyDetail()
		{
			int tmp;
			std::istringstream(settings["detail_sky"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the sky detail level.
		 * See getSkyDetail() for information about the values.
		 * @param skyDetail The new sky detail level
		 * @see getSkyDetail()
		 */
 
		inline void
		setSkyDetail(int skyDetail)
		{
			std::ostringstream tmp;
			tmp << skyDetail;
			settings["detail_sky"] = tmp.str();
		}



		//
		// Effect detail
		//

		/**
		 * Get the effect detail level.
		 * Possible values:
		 * 0 - low,
		 * 1 - normal,
		 * 2 - high.
		 * @return The current effect detail level
		 */
		inline int
		getEffectDetail()
		{
			int tmp;
			std::istringstream(settings["detail_effects"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the effect detail level.
		 * See getEffectDetail() for information about the values.
		 * @param effectDetail The new effect detail level
		 * @see getEffectDetail()
		 */
		inline void
		setEffectDetail(int effectDetail)
		{
			std::ostringstream tmp;
			tmp << effectDetail;
			settings["detail_effects"] = tmp.str();
		}



		//
		// Music volume
		//

		/**
		 * Get the music volume.
		 * The possible values range from 0.0 (no music) to 1.0
		 * (full volume).
		 * @return The current music volume
		 */
		inline double
		getMusicVolume()
		{
			double tmp;
			std::istringstream(settings["volume_music"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the music volume.
		 * See getMusicVolume() for infomation abot the values.
		 * @param musicVolume The new music volume
		 * @see getMusicVolume()
		 */
		inline void
		setMusicVolume(double musicVolume)
		{
			std::ostringstream tmp;
			tmp << musicVolume;
			settings["volume_music"] = tmp.str();
		}



		//
		// Sound volume
		//

		/**
		 * Get the sound effect volume.
		 * The possible values range from 0.0 (no sound) to 1.0
		 * (full volume).
		 * @return The current sound volume
		 */
		inline double
		getSoundVolume()
		{
			double tmp;
			std::istringstream(settings["volume_sound"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the sound effect volume.
		 * See getSoundVolume() for information about the values.
		 * @param soundVolume The new sound volume
		 * @see getSoundVolume()
		 */
		inline void
		setSoundVolume(double soundVolume)
		{
			std::ostringstream tmp;
			tmp << soundVolume;
			settings["volume_sound"] = tmp.str();
		}



		//
		// Grass detail
		//

		/**
		 * Get the amount of grass.
		 * Possible values:
		 * 0 - none,
		 * 1 - few,
		 * 2 - normal,
		 * 3 - much,
		 * 4 - very much.
		 * @return The current amount of grass.
		 */
		inline int
		getGrassDetail()
		{
			int tmp;
			std::istringstream(settings["detail_grass"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the amount of grass.
		 * See getGrassDetail() for information about the values.
		 * @param grassDetail The new amount of grass
		 * @see getGrassDetail()
		 */
		inline void
		setGrassDetail(int grassDetail)
		{
			std::ostringstream tmp;
			tmp << grassDetail;
			settings["detail_grass"] = tmp.str();
		}



		//
		// 2d post processing
		//

		/**
		 * Check whether 2d post processing is enabled.
		 * @return true if activated, false if not
		 */
		inline bool
		getPp2d()
		{
			bool tmp;
			s2str::stob(settings["effects_pp2d"], tmp);
			return tmp;
		}

		/**
		 * Set 2d post processing.
		 * @param 2dFX true to enable, and false to disable
		 */
		inline void
		setPp2d(bool Pp2d)
		{
			settings["effects_pp2d"] = (Pp2d ? "true" : "false");
		}

		/**
		 * Toggle 2d post processing.
		 * This is equivalent to set2dFX ( !get2dFX() )
		 */
		inline void
		togglePp2d()
		{
			setPp2d(!getPp2d());
		}



		//
		// Light effects
		//

		/**
		 * Check whether light effects are enabled.
		 * @return true if enabled, false if not
		 */
		inline bool
		getLightFx()
		{
			bool tmp;
			s2str::stob(settings["effects_light"], tmp);
			return tmp;
		}

		/**
		 * Set light effects.
		 * @param lightFX true to enable, false to disable
		 */
		inline void
		setLightFx(bool lightFx)
		{
			settings["effects_light"] = (lightFx ? "true" : "false");
		}

		/**
		 * Toggle light effects.
		 * This is equivalent to setLightFX ( !getLightFX() )
		 */
		inline void
		toggleLightFx()
		{
			setLightFx(!getLightFx());
		}



		//
		// Wind swaying motion
		//

		/**
		 * Check whether wind-swaying objects are enabled.
		 * @return true if enabled, false if not
		 */
		inline bool
		getWindSwayFx()
		{
			bool tmp;
			s2str::stob(settings["effects_windsway"], tmp);
			return tmp;
		}

		/**
		 * Set wind-swaying objects.
		 * @param windSwayFX true to enable, false to disable
		 */
		inline void
		setWindSwayFx(bool windSwayFx)
		{
			settings["effects_windsway"] = (windSwayFx ? "true" : "false");
		}

		/**
		 * Toggle wind-swaying objects.
		 * This is equivalent to setWindSwayFX ( !getWindSwayFX() )
		 */
		inline void
		toggleWindSwayFx()
		{
			setWindSwayFx(!getWindSwayFx());
		}



		//
		// Player's name
		//

		/**
		 * Get the player name.
		 * This originally should have been used in a multiplayer mode, but it
		 * is still left in the config files, so we will store this information
		 * here as well.
		 * @return The current player name
		 */
		inline std::string
		getPlayerName() { return settings["multiplayer_playername"]; }


		/**
		 * Set the player name.
		 * @param playerName The new player name
		 * @see getPlayerName()
		 */
		inline void
		setPlayerName(const std::string &playerName) { settings["multiplayer_playername"] = playerName; }



		//
		// Port number
		//

		/**
		 * Get the port number.
		 * This originally should have been used in a multiplayer mode, but it
		 * is still left in the config files, so we will store this information
		 * here as well.
		 * @return The current port number
		 */
		inline uint16_t
		getPort()
		{
			uint16_t tmp;
			std::istringstream(settings["multiplayer_port"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the port number.
		 * @param port The new port number
		 * @see getPort()
		 */
		inline void
		setPort(uint16_t port)
		{
			std::ostringstream tmp;
			tmp << port;
			settings["multiplayer_port"] = tmp.str();
		}



		//
		// Fog effects
		//

		/**
		 * Check whether fog effects are enabled.
		 * @return true if enabled, false if not
		 */
		inline bool
		getFog()
		{
			bool tmp;
			s2str::stob(settings["effects_fog"], tmp);
			return tmp;
		}

		/**
		 * Set fog effects.
		 * @param fog true to enable, false to disable
		 */
		inline void
		setFog(bool fog)
		{
			settings["effects_fog"] = (fog ? "true" : "false");
		}

		/**
		 * Toggle fog effects.
		 * Equivalent to setFog ( !getFog() )
		 */
		inline void
		toggleFog()
		{
			setFog(!getFog());
		}



		//
		// Multitexturing
		//

		/**
		 * Check whether multitexturing ("hwmultitex") is enabled.
		 * @return true if enabled, false if not
		 */
		inline bool
		getMultiTex()
		{
			bool tmp;
			s2str::stob(settings["effects_multitex"], tmp);
			return tmp;
		}

		/**
		 * Set multitexturing.
		 * @param multiTex true to enable, false to disable
		 */
		inline void
		setMultiTex(bool multiTex)
		{
			settings["effects_multitex"] = (multiTex ? "true" : "false");
		}

		/**
		 * Toggle multitexturing.
		 * This is equivalent to setMultiTex ( !getMultiTex )
		 */
		inline void
		toggleMultiTex()
		{
			setMultiTex(!getMultiTex());
		}



		//
		// Motion blur
		//

		/**
		 * Check whether motion blur is enabled.
		 * @return true if enabled, false if not
		 */
		inline bool
		getMotionBlur()
		{
			bool tmp;
			s2str::stob(settings["effects_motionblur"], tmp);
			return tmp;
		}

		/**
		 * Set motion blur.
		 * @param motionBlur true to enable, false to disable
		 */
		inline void
		setMotionBlur(bool motionBlur)
		{
			settings["effects_motionblur"] = (motionBlur ? "true" : "false");
		}

		/**
		 * Toggle motion blur.
		 * This is equivalent to setMotionBlur ( !getMotionBlur() )
		 */
		inline void
		toggleMotionBlur()
		{
			setMotionBlur(!getMotionBlur());
		}

		/**
		 * Get motion blur intensity.
		 * Possible values range from 0.0 (no blurring) to 1.0
		 * (intense blurring).
		 * @return The current motion blur intensity
		 */
		inline double
		getMotionBlurIntensity()
		{
			double tmp;
			std::istringstream(settings["effects_mbintensity"]) >> tmp;
			return tmp;
		}

		/**
		 * Set the motion blur intensity.
		 * See getMotionBlurIntensity() for information about the values.
		 * @param motionBlurIntensity The new intensity
		 */
		inline void
		setMotionBlurIntensity(double motionBlurIntensity)
		{
			std::ostringstream tmp;
			tmp << motionBlurIntensity;
			settings["effects_mbintensity"] = tmp.str();
		}



		//
		// "Temporary" settings (these are not stored in a file)
		//

		/**
		 * Get the mod name.
		 * @return The currently loaded mod.
		 */
		inline std::string
		getModName()
		{
			return tempSettings["modname"];
		}

		/**
		 * Set the mod name.
		 * This should only be set at the start of the game.
		 * @param modName The new mod name.
		 */
		inline void
		setModName(std::string modName)
		{
			tempSettings["modname"] = modName;
		}
};

#endif /* STRANDED_SETTINGS_HH */
