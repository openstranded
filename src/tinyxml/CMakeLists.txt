SET (TINYXML_SRC
	tinyxml.cpp
	tinyxmlparser.cpp
	tinyxmlerror.cpp
	tinystr.cpp
)

add_library(tinyxml STATIC ${TINYXML_SRC})
