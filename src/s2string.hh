/*
 * This file includes function definitions for several useful string
 * operations.
 *
 * Copyright (C) 2008 David Kolossa
 *
 * This file is part of OpenStranded.
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_S2STRING_HH
#define STRANDED_S2STRING_HH

#include <vector>
#include <string>
#include <sstream>

/*
 * Some constants passed in warnFlag
 */
#define EXPLODE_OK		0
#define EXPLODE_LOWER		1
#define EXPLODE_GREATER		2
#define EXPLODE_NO_TOKEN	4

#define inFlag(flag, value) (((flag) & (value)) == (value))

/**
 * This namespace holds functions for easy manipulation of
 * strings.
 */
namespace s2str
{

	/**
	 * This function divides a string into substrings by a token.
	 * Inspired by the PHP function with the same name.
	 * @param str The string which should be divided
	 * @param buf The buffer in which the substrings should be saved
	 * @param token The token which should split the string
	 * @param count The maximum number of strings to be written into buf
	 * @return 0 on success, larger than 0 on error
	 */
	int
	explode(std::string str, std::string *buf, const char *token, size_t count);

	/**
	 * Converts a string into a float.
	 * This functions does some checks on the string so use string
	 * streams if you think you already have a valid float string.
	 * @param str The string to be conversed
	 * @param buf the float in which the result is stored
	 * @return true if string is valid, if not, it sets the float to 0.0 and returns false
	 */
	bool
	stof(std::string str, float &buf);

	/**
	 * Converts a string into an int.
	 * This function checks the number whether it is a valid integer to allow
	 * user-friendly error-reporting for typos.
	 * If you don't need those checks use a string stream instead.
	 * @param str The string to be conversed
	 * @param buf The integer in which the result is stored
	 * @return true if str was valid and conversed, if not, buf is set to 0 and false is returned
	 */
	bool
	stoi(std::string str, int &buf);

	/**
	 * Converts a string into a bool.
	 * This function checks for several possible keywords.
	 * These are:
	 * true - false, on - off
	 * In addition, 0 becomes false, every other number becomes true
	 * @param str The string to be conversed
	 * @param buf The bool in which the result is stored
	 * @return true if str was valid and conversed, false if not
	 */
	bool
	stob(std::string str, bool &buf);

	/**
	 * Check whether a string represents an integer.
	 * @param str The string to be checked
	 * @return true if str is an integer, false if not
	 */
	bool
	isInteger(std::string &str);

	/**
	 * Check whether a string represents an decimal fraction.
	 * @param str The string to be checked
	 * @return true if str is an decimal, false if not
	 */
	bool
	isDecimal(std::string &str);

	/**
	 * Check whether a string represents a boolean value.
	 * @param str The string to be checked
	 * @return true if str is a bool, false if not
	 */
	bool
	isBool(std::string &str);

	/**
	 * Convert a string to lowercase.
	 * @param str The string to be conversed.
	 * @return The lowercase string.
	 */
	std::string
	toLowercase(std::string &str);

	/**
	 * Convert a string to uppercase.
	 * @param str The string top be conversed.
	 * @return The uppercase string.
	 */
	std::string
	toUppercase(std::string &str);
}

#endif /* STRANDED_S2STRING_HH */
