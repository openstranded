/*
 * This file includes the implementation of the item specific classes
 *
 * Copyright (C) 2008 David Kolossa
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include "item.hh"

/*
 * These are the declarations of the ItemEntity class
 */

ItemEntity::ItemEntity (ID type):
	Entity(type)
{}


/*
 * These are the declarations of the ItemType class
 */

ItemType::ItemType (const char* name):
	Type(name)
{}


/*
 * ItemKingdom class methods
 */

ItemKingdom::ItemKingdom ():
	Kingdom ("item")
{}


Entity*
ItemKingdom::createEntity (ID type)
{
	return new ItemEntity (type);
}


Type*
ItemKingdom::createType (const char* name)
{
	return new ItemType (name);
}
