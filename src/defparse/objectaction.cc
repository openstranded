/*
* This file implements object specific functions and classes
* from the defaction.hh header
*
* Copyright (C) 2008 Hermann Walth
*
* This file is part of OpenStranded
*
* OpenStranded is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* OpenStranded is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include "defaction.hh"
#include "../object.hh"

def::object::ActionList::ActionList ():
def::ActionList ()
{
	this->listItem ("id") = new def::IDAction (S2_OBJECT);
	this->listItem ("name") = new def::NameAction (S2_OBJECT);
	this->listItem ("model") = new def::ModelAction (S2_OBJECT);
	this->listItem ("icon") = new def::IconAction (S2_OBJECT);

	this->listItem ("x") = new def::XAction (S2_OBJECT);
	this->listItem ("y") = new def::YAction (S2_OBJECT);
	this->listItem ("z") = new def::ZAction (S2_OBJECT);
	this->listItem ("scale") = new def::ScaleAction (S2_OBJECT);

	this->listItem ("r") = new def::RAction (S2_OBJECT);
	this->listItem ("g") = new def::GAction (S2_OBJECT);
	this->listItem ("b") = new def::BAction (S2_OBJECT);
	this->listItem ("color") = new def::ColorAction (S2_OBJECT);

	this->listItem ("autofade") = new def::AutofadeAction (S2_OBJECT);
	this->listItem ("alpha") = new def::AlphaAction (S2_OBJECT);
	this->listItem ("shine") = new def::ShineAction (S2_OBJECT);
}
