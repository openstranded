/*
 * This header includes general functions and typedefs to parse Stranded
 * game-information *.inf files.
 *
 * Copyright (C) 2008 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_DEFPARSE_HH
#define STRANDED_DEFPARSE_HH

#include <string>
#include <fstream>
#include <map>

namespace def
{
	// used by def::lex and def::parse to pass information
	extern std::string text; 
	
	/*
	 * The definition lexer, which reads *.inf files and breaks lines down,
	 * returns the lval and stores the rval into def::text.
	 * The lexer is controlled by def::parse
	 */
	std::string
	lex (std::ifstream &fin);

	namespace object
	{
		/*
		 * The parser. This function is the main interface
		 * to the "rest of the world"
		 * It lets the lexer get data and
		 * searches for Action classes to evaluate this data
		 */
		void
		parse (const char *filename);
	} /* namespace object */

} /* namespace def */


#endif /* STRANDED_DEFPARSE_HH */
