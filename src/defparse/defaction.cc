/*
* This file implements general functions and classes
* from the defaction.hh header
*
* Copyright (C) 2008 Hermann Walth
*
* This file is part of OpenStranded
*
* OpenStranded is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* OpenStranded is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include "../filesystem.hh"
#include "../output.hh"
#include "../s2string.hh"
#include "../kingdom.hh"
#include "../error.hh"
#include "defaction.hh"

ID currentType = 0;

/*
 * Action List methods
 */

def::ActionList::ActionList ():
std::map <std::string, def::Action*> () {}


// Destructor, deletes all list's elements
def::ActionList::~ActionList ()
{
	std::map <std::string, def::Action*>::iterator iter;
	for (iter = this->begin (); iter != this->end (); iter++)
		delete (iter->second);
}

// Wrapper operator[] method for const char* strings
def::Action*&
def::ActionList::listItem (const char* index)
{
	return (*this) [std::string (index)];
	//return (this->find (std::string (index)))->second;
}


/*
 * Action classes method definitions
 */

def::Action::Action (ID kingdom)
{
	this->kingdomID = kingdom;
	this->lastType = 0;
}


def::IDAction::IDAction (ID kingdom):
Action (kingdom) {}

def::NameAction::NameAction (ID kingdom):
Action (kingdom) {}

def::ModelAction::ModelAction (ID kingdom):
Action (kingdom) {}

def::IconAction::IconAction (ID kingdom):
Action (kingdom) {}

def::XAction::XAction (ID kingdom):
Action (kingdom) {}

def::YAction::YAction (ID kingdom):
Action (kingdom) {}

def::ZAction::ZAction (ID kingdom):
Action (kingdom) {}

def::ScaleAction::ScaleAction (ID kingdom):
Action (kingdom) {}

def::RAction::RAction (ID kingdom):
Action (kingdom) {}

def::GAction::GAction (ID kingdom):
Action (kingdom) {}

def::BAction::BAction (ID kingdom):
Action (kingdom) {}

def::ColorAction::ColorAction (ID kingdom):
Action (kingdom) {}

def::AutofadeAction::AutofadeAction (ID kingdom):
Action (kingdom) {}

def::AlphaAction::AlphaAction (ID kingdom):
Action (kingdom) {}

def::ShineAction::ShineAction (ID kingdom):
Action (kingdom) {}



void
def::IDAction::operator() (std::string rtext)
{
	ID newID;
	std::istringstream (rtext, std::ios::in) >> newID;

	currentType = newID;

	if (Kingdom::kingdomList.find (kingdomID) != Kingdom::kingdomList.end ())
	{
		if (!Kingdom::kingdomList[kingdomID]->typeExists(newID))
			Kingdom::kingdomList[kingdomID]->insertType (newID);
		else
		{
			Out::fatal << "Type #" << newID << " has already been declared!" << Out::endl;
		}
	}
	else
	{
		Out::fatal << "Kingdom does not exist!" << Out::endl;
	}
}



void
def::NameAction::operator() (std::string rtext)
{
	if (currentType == 0)
	{
		Out::warning << "No type ID specified, assignment of value \"name\" not possible" << Out::endl;
		return;
	}

	if (this->lastType != currentType)
		Kingdom::kingdomList[kingdomID]->getType (currentType) ->setName (rtext);
	else
		Out::warning << "Double assignment of value \"name\" in object type #" << currentType << Out::endl;
}



void
def::ModelAction::operator() (std::string rtext)
{
	if (currentType == 0)
	{
		Out::warning << "No type ID specified, assignment of value \"model\" not possible" << Out::endl;
		return;
	}

	// Spot legacy file paths, transform and give a deprecation warning
	// Since Stranded II had all files given relative to the mod directory,
	// we use the "gfx\" piece to spot legacy file paths
	std::string::size_type pos;
	if((pos = rtext.find("gfx\\")) != std::string::npos)
	{
		Out::warning << "Deprecated model file path format in type # " << currentType << Out::endl;

		// Remove old directory part
		rtext = rtext.substr(pos+4);

		// Remove file extension
		pos = rtext.find_last_of('.');
		rtext = rtext.substr(0, pos);
	}

	// Remove backslashes
	for (unsigned int i = 0; i < rtext.size(); i++)
		if (rtext[i] == '\\') rtext[i] = '/';

	if (this->lastType != currentType)
	{
		FileInfo *modelInfo = FileSystem::getModelFileInfo(rtext);
		Kingdom::kingdomList[kingdomID]->getType (currentType)->setModel (modelInfo->path);
		delete modelInfo;
	}
	else
	{
		Out::warning << "Double assignment of value \"model\" in type #" << currentType << Out::endl;
	}

}



void
def::IconAction::operator() (std::string rtext)
{
	if (currentType == 0)
		Out::warning << "No type ID specified, assignment of value \"icon\" not possible" << Out::endl;

	if (this->lastType != currentType)
		 Kingdom::kingdomList[kingdomID]->getType (currentType) ->setIconPath (rtext);
	else
		Out::warning << "Double assignment of value \"icon\" in type #" << currentType << Out::endl;
}



void
def::XAction::operator() (std::string rtext)
{
	float newScaleX;
	std::istringstream (rtext) >> newScaleX;

	if (currentType == 0)
	{
		Out::warning << "No type ID specified, assignment of value \"x\" not possible" << Out::endl;
		return;
	}

	if (newScaleX <= 0)
	{
		Out::warning << "Invalid value \"x\"; fallback to 1.0 in type #" << currentType << Out::endl;
		newScaleX = 1.0;
	}

	if (this->lastType != currentType)
		 Kingdom::kingdomList[kingdomID]->getType (currentType) ->setScaleX (newScaleX);
	else
		Out::warning << "Double assignment of value \"x\" in type #" << currentType << Out::endl;
}



void
def::YAction::operator() (std::string rtext)
{
	float newScaleY;
	std::istringstream (rtext) >> newScaleY;

	if (currentType == 0)
	{
		Out::warning << "No type ID specified, assignment of value \"y\" not possible" << Out::endl;
		return;
	}

	if (newScaleY <= 0)
	{
		Out::warning << "Invalid value \"y\"; fallback to 1.0 in  type #" << currentType << Out::endl;
		newScaleY = 1.0;
	}

	if (this->lastType != currentType)
		Kingdom::kingdomList[kingdomID]->getType (currentType) ->setScaleY (newScaleY);
	else
		Out::warning << "Double assignment of value \"y\" in Object type #" << currentType << Out::endl;
}



void
def::ZAction::operator() (std::string rtext)
{
	float newScaleZ;
	std::istringstream (rtext) >> newScaleZ;

	if (currentType == 0)
	{
		Out::warning << "No type ID specified, assignment of value \"z\" not possible" << Out::endl;
		return;
	}

	if (newScaleZ <= 0)
	{
		Out::warning << "Invalid value \"z\"; fallback to 1.0 in type #" << currentType << Out::endl;
		newScaleZ = 1.0;
	}

	if (this->lastType != currentType)
		 Kingdom::kingdomList[kingdomID]->getType (currentType) ->setScaleZ (newScaleZ);
	else
		Out::warning << "Double assignment of value \"z\" in type #" << currentType << Out::endl;
}



void
def::ScaleAction::operator() (std::string rtext)
{
	std::string scaleValues[3];
	int warnFlag;
	float endValue;


	if (currentType == 0)
	{
		Out::warning << "No type ID specified, assignment of value \"scale\" not possible" << Out::endl;
		return;
	}

	warnFlag = s2str::explode (rtext, scaleValues, ",", 3);

	// Display error message when number of arguments wasn't either 1 or 3
	if (warnFlag != EXPLODE_OK && !inFlag (warnFlag, EXPLODE_NO_TOKEN))
	{
		Out::warning << "Warning: Wrong number of arguments for value \"scale\"" << Out::endl;
	}


	if (this->lastType != currentType)
	{
		// Catch special case when only one value was given and needs to be applied
		// to all x, y and z
		if (inFlag (warnFlag, EXPLODE_NO_TOKEN))
		{
			if (!s2str::stof (scaleValues[0], endValue))
			{
				Out::warning << "Invalid value in \"scale\"; fallback to 1.0 in type #" << currentType << Out::endl;
				endValue = 1.0;
			}
			Kingdom::kingdomList[kingdomID]->getType (currentType) ->setScaleX (endValue);
			Kingdom::kingdomList[kingdomID]->getType (currentType) ->setScaleY (endValue);
			Kingdom::kingdomList[kingdomID]->getType (currentType) ->setScaleZ (endValue);
			return;
		}

		// scaleX
		if (!s2str::stof (scaleValues[0], endValue))
		{
			Out::warning << "Invalid x value in \"scale\"; fallback to 1.0 in type #" << currentType << Out::endl;
			endValue = 1.0;
		}
		Kingdom::kingdomList[kingdomID]->getType (currentType) ->setScaleX (endValue);


		// scaleY
		if (!s2str::stof (scaleValues[1], endValue))
		{
			Out::warning << "Invalid y value in \"scale\"; fallback to 1.0 in type #" << currentType << Out::endl;
			endValue = 1.0;
		}
		Kingdom::kingdomList[kingdomID]->getType (currentType) ->setScaleY (endValue);



		// scaleZ
		if (!s2str::stof (scaleValues[2], endValue))
		{
			Out::warning << "Invalid z value in \"scale\"; fallback to 1.0 in type #" << currentType << Out::endl;
			endValue = 1.0;
		}
		Kingdom::kingdomList[kingdomID]->getType (currentType) ->setScaleZ (endValue);
	}
	else
	{
		Out::warning << "Double assignment of value \"scale\" in type #" << currentType << Out::endl;
	}
}



void
def::RAction::operator() (std::string rtext)
{
	int newColorR;
	bool strValid;

	if (currentType == 0)
	{
		Out::warning << "No type ID specified, assignment of value \"r\" not possible" << Out::endl;
		return;
	}

	strValid = s2str::stoi (rtext, newColorR);
	if (!strValid || newColorR < 0 || newColorR > 255)
	{
		Out::warning << "Invalid argument for \"r\" value in type #" << currentType << ", fallback to 0" << Out::endl;
		newColorR = 0;
	}

	if (this->lastType != currentType)
		 Kingdom::kingdomList[kingdomID]->getType (currentType) ->setColorR ( (uint8_t) newColorR);
	else
		Out::warning << "Double assignment of value \"r\" in type #" << currentType << Out::endl;
}



void
def::GAction::operator() (std::string rtext)
{
	int newColorG;
	bool strValid;

	if (currentType == 0)
	{
		Out::warning << "No type ID specified, assignment of value \"g\" not possible" << Out::endl;
		return;
	}

	strValid = s2str::stoi (rtext, newColorG);
	if (!strValid || newColorG < 0 || newColorG > 255)
	{
		Out::warning << "Invalid argument for \"g\" value in type #" << currentType << ", fallback to 0" << Out::endl;
		newColorG = 0;
	}

	if (this->lastType != currentType)
		 Kingdom::kingdomList[kingdomID]->getType (currentType) ->setColorG ( (uint8_t) newColorG);
	else
		Out::warning << "Double assignment of value \"g\" in type #" << currentType << Out::endl;
}



void
def::BAction::operator() (std::string rtext)
{
	int newColorB;
	bool strValid;

	if (currentType == 0)
	{
		Out::warning << "No type ID specified, assignment of value \"b\" not possible" << Out::endl;
		return;
	}

	strValid = s2str::stoi (rtext, newColorB);
	if (!strValid || newColorB < 0 || newColorB > 255)
	{
		Out::warning << "Invalid argument for \"b\" value in type #" << currentType << ", fallback to 0" << Out::endl;
		newColorB = 0;
	}

	if (this->lastType != currentType)
		 Kingdom::kingdomList[kingdomID]->getType (currentType) ->setColorB ( (uint8_t) newColorB);
	else
		Out::warning << "Double assignment of value \"b\" in type #" << currentType << Out::endl;
}



void
def::ColorAction::operator() (std::string rtext)
{
	std::string colorValues[3];
	int endValue;
	int warnFlag;
	bool strValid;

	if (currentType == 0)
	{
		Out::warning << "No type ID specified, assignment of value \"color\" not possible" << Out::endl;
		return;
	}

	warnFlag = s2str::explode (rtext, colorValues, ",", 3);

	// Display error message when number of arguments wasn't either 1 or 3
	if (warnFlag != EXPLODE_OK && !inFlag (warnFlag, EXPLODE_NO_TOKEN))
	{
		Out::warning << "Warning: Wrong number of arguments for value \"color\"" << Out::endl;
	}

	if (this->lastType != currentType)
	{
		// Catch special case when only one value was given and needs to be applied
		// to all r, g and b
		if (inFlag (warnFlag, EXPLODE_NO_TOKEN))
		{
			strValid = s2str::stoi (colorValues[0], endValue);
			if (!strValid || endValue < 0 || endValue > 255)
			{
				Out::warning << "Invalid value in \"color\"; fallback to 0.0 in type #" << currentType << Out::endl;
				endValue = 0;
			}
			Kingdom::kingdomList[kingdomID]->getType (currentType) ->setColorR (endValue);
			Kingdom::kingdomList[kingdomID]->getType (currentType) ->setColorG (endValue);
			Kingdom::kingdomList[kingdomID]->getType (currentType) ->setColorB (endValue);
			return;
		}

		// R
		strValid = s2str::stoi (colorValues[0], endValue);
		if (!strValid || endValue < 0 || endValue > 255)
		{
			Out::warning << "Invalid argument for \"r\" value in \"color\" in type #" << currentType << ", fallback to 0" << Out::endl;
			endValue = 0;
		}

		Kingdom::kingdomList[kingdomID]->getType (currentType) ->setColorR ( (uint8_t) endValue);


		// G
		strValid = s2str::stoi (colorValues[1], endValue);
		if (!strValid || endValue < 0 || endValue > 255)
		{
			Out::warning << "Invalid argument for \"g\" value in \"color\" in type #" << currentType << ", fallback to 0" << Out::endl;
			endValue = 0;
		}

		Kingdom::kingdomList[kingdomID]->getType (currentType) ->setColorG ( (uint8_t) endValue);


		// B
		strValid = s2str::stoi (colorValues[2], endValue);
		if (!strValid || endValue > 255)
		{
			Out::warning << "Invalid argument for \"b\" value in \"color\" in type #" << currentType << ", fallback to 0" << Out::endl;
			endValue = 0;
		}

		Kingdom::kingdomList[kingdomID]->getType (currentType) ->setColorB ( (uint8_t) endValue);

	}
	else
	{
		Out::warning << "Double assignment of value \"color\" in type #" << currentType << Out::endl;
	}
}



void
def::AutofadeAction::operator() (std::string rtext)
{
	float endValue;

	if (currentType == 0)
	{
		Out::warning << "No type id specified, assignment of value \"autofade\" not possible" << Out::endl;
		return;
	}

	if (this->lastType != currentType)
	{
		if (!s2str::stof (rtext, endValue))
		{
			Out::warning << "Invalid argument for value \"autofade\" in type #" << currentType << ", fallback to 500" << Out::endl;
			endValue = 500.0;
		}

		Kingdom::kingdomList[kingdomID]->getType (currentType) ->setFadingDistance (endValue);
	}
	else
	{
		Out::warning << "Double assignment of value \"autofade\" in type #" << currentType << Out::endl;
	}
}



void
def::AlphaAction::operator() (std::string rtext)
{
	float endValue;

	if (currentType == 0)
	{
		Out::warning << "No type id specified, assignment of value \"alpha\" not possible" << Out::endl;
		return;
	}

	if (this->lastType != currentType)
	{
		if (!s2str::stof (rtext, endValue) || endValue < 0.0 || endValue > 1.0)
		{
			Out::warning << "Invalid argument for value \"alpha\" in type #" << currentType << ", fallback to 1.0" << Out::endl;
			endValue = 1.0;
		}

		Kingdom::kingdomList[kingdomID]->getType (currentType) ->setAlpha (endValue);
	}
	else
	{
		Out::warning << "Double assignment of value \"alpha\" in type #" << currentType << Out::endl;
	}
}



void
def::ShineAction::operator() (std::string rtext)
{
	float endValue;

	if (currentType == 0)
	{
		Out::warning << "No type id specified, assignment of value \"shine\" not possible" << Out::endl;
		return;
	}

	if (this->lastType != currentType)
	{
		if (!s2str::stof (rtext, endValue) || endValue < 0.0 || endValue > 1.0)
		{
			Out::warning << "Invalid argument for value \"shine\" in type #" << currentType << ", fallback to 0.0" << Out::endl;
			endValue = 0.0;
		}

		Kingdom::kingdomList[kingdomID]->getType (currentType) ->setShine (endValue);
	}
	else
	{
		Out::warning << "Double assignment of value \"shine\" in type #" << currentType << Out::endl;
	}
}
