/*
 * See defparse.hh for information about this file.
 *
 * Copyright (C) 2008 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <fstream>
#include "defparse.hh"


std::string def::text;

std::string def::lex (std::ifstream &fin)
{
	std::string line;

	while (std::getline (fin, line))
	{
  		if (line[0] == '#')
			continue;

		std::string::size_type eqpos = line.find ('=', 0);

		if (eqpos == std::string::npos)
			continue;

		def::text = line.substr (eqpos + 1);

		// handle CR-LF terminated lines
		if (def::text [def::text.size () -1] == '\r')
		{
			def::text.erase( def::text.size () -1 );
		}

		return line.substr (0, eqpos);
	}

	return std::string ();
}
