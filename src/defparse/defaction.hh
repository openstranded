/*
 * This header includes more specific functions for definition actions.
 * Actions are functors that are executed by the definition parser
 * upon recognizing valid entries.
 *
 * Copyright (C) 2008 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_DEFACTION_HH
#define STRANDED_DEFACTION_HH

#include "defparse.hh"

typedef unsigned int ID;
typedef unsigned char uint8_t;

namespace def {

	class Action
	{
		protected:
		// What kingdom this action is used for
		ID kingdomID;
		// lastID is used for some provisional error checking
		ID lastType;

		public:
			Action (ID kingdomID);

			virtual void
			operator() (std::string rtext) =0;
	};

	//typedef std::map <std::string, Action*> ActionList;
	class ActionList: public std::map <std::string, Action*>
	{
		public:
			ActionList ();
			virtual ~ActionList ();

			Action*&
			listItem (const char*);
	};


	class IDAction: public def::Action
	{
		public:
			IDAction (ID kingdomID);

			void
			operator() (std::string rtext);

	};


	class NameAction: public Action
	{
		public:
			NameAction (ID kingdomID);

			void
			operator() (std::string rtext);

	};


	class ModelAction: public Action
	{
		public:
			ModelAction (ID kingdomID);

			void
			operator() (std::string rtext);

	};


	class IconAction: public Action
	{
		public:
			IconAction (ID kingdomID);

			void
			operator() (std::string rtext);

	};


	class XAction: public Action
	{
		public:
			XAction (ID kingdomID);

			void
			operator() (std::string rtext);
	};


	class YAction: public Action
	{
		public:
			YAction (ID kingdomID);

			void
			operator() (std::string rtext);
};


	class ZAction: public Action
	{
		public:
			ZAction (ID kingdomID);

			void
			operator() (std::string rtext);
	};


	class ScaleAction: public Action
	{
		public:
			ScaleAction (ID kingdomID);

			void
			operator() (std::string rtext);
	};


	class RAction: public Action
	{
		public:
			RAction (ID kingdomID);

			void
			operator() (std::string rtext);
	};


	class GAction: public Action
	{
		public:
			GAction (ID kingdomID);

			void
			operator() (std::string rtext);
	};


	class BAction: public Action
	{
		public:
			BAction (ID kingdomID);

			void
			operator() (std::string rtext);
	};


	class ColorAction: public Action
	{
		public:
			ColorAction (ID kingdomID);

			void
			operator() (std::string rtext);
	};

	class AutofadeAction: public Action
	{
		public:
			AutofadeAction (ID kingdomID);

			void
			operator() (std::string rtext);
	};

	class AlphaAction: public Action
	{
		public:
			AlphaAction (ID kingdom);

			void
			operator() (std::string rtext);
	};

	class ShineAction: public Action
	{
		public:
			ShineAction (ID kingdom);

			void
			operator() (std::string rtext);
	};




	namespace object
	{
		//extern ActionList actionList;
		class ActionList: public def::ActionList
		{
			public:
				ActionList ();
		};
	}

	namespace unit
	{
		//extern ActionList actionList;
		class ActionList: public def::ActionList
		{
			public:
				ActionList ();
		};
	}

	namespace item
	{
		//extern ActionList actionList;
		class ActionList: public def::ActionList
		{
			public:
				ActionList ();
		};
	}

} /* namespace def */


#endif /* STRANDED_DEFACTION_HH */
