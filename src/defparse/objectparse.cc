/*
 * See defparse.hh for information about this file.
 *
 * Copyright (C) 2008 Hermann Walth
 *
 * This file is part of OpenStranded
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <fstream>
#include <iostream>
#include "../error.hh"
#include "defparse.hh"
#include "defaction.hh"

void
def::object::parse (const char* filename)
{
	// Open file
	std::ifstream defin (filename, std::ios::in);
	
	std::string lval;
	
	//def::object::initActionList ();
	def::object::ActionList objectActions;

	
	while ((lval = def::lex (defin)) != "")
	{
		try
		{
			if (objectActions.find (lval) !=  objectActions.end ()
				 )
				(*objectActions [lval]) (def::text);
			else
				throw StrandedError ("Unknown definition entry '" + lval + "' (ignored)");
		}
		catch (StrandedException &e)
		{
			e.print ();
		}
	}
}
