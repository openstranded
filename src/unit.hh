/*
 * This file includes the definitions for the unit specific classes
 * derived from the Kingdom, Type and Entity base classes
 *
 * This file is part of OpenStranded
 *
 * Copyright (C) 2008 David Kolossa
 *
 * OpenStranded is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenStranded is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenStranded. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRANDED_UNIT_HH
#define STRANDED_UNIT_HH

#include "kingdom.hh"
#include "type.hh"
#include "entity.hh"

// Unit Kingdom's Constant ID
#define S2_UNIT 2

/**
 * This is the class of all entities which belong to the "unit" kingdom.
 * An unit is a mobile and intelligent (or controlled) entity.
 */
class UnitEntity: public Entity
{
	public:
		/**
		 * The constructor.
		 * @param type The ID of the new entity's type
		 */
		UnitEntity (ID type);
};


/**
 * This is the class of the unit type.
 */
class UnitType: public Type
{
	public:
		/**
		 * The constructor.
		 * @param name The name of the new type
		 */
		UnitType (const char* name);
};


/**
 * This is the class of the unit kingdom
 */
class UnitKingdom: public Kingdom
{
	public:
		/**
		 * The constructor.
		 */
		UnitKingdom ();
		


		//
		// Virtual functions
		//

		/**
		 * Create a new unit entity.
		 * @param The ID of the new entity's type
		 * @return A pointer to the created entity
		 * @see UnitEntity::UnitEntity()
		 */
		Entity*
		createEntity (ID type);

		/**
		 * Create a new type.
		 * @param The name of the new type
		 * @return A pointer to the created type
		 * @see UnitType::UnitType()
		 */
		Type*
		createType (const char* name);

};

#endif /* STRANDED_UNIT_HH */
