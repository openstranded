[[FX]]
<!--
// =================================================================================================
	Terrain Shader
// =================================================================================================
-->

<Sampler id="colormap" />

<Sampler id="detailmap" />

<Sampler id="ambientMap">
	<StageConfig addressMode="CLAMP" filtering="BILINEAR" maxAnisotropy="1" />
</Sampler>

<Uniform id="specParams" a="0.1" b="16.0">
	<!-- Description
		a - Specular mask
		b - Specular exponent
	-->
</Uniform>

<Context id="ATTRIBPASS">
	<Shaders vertex="VS_GENERAL" fragment="FS_ATTRIBPASS" />
</Context>

<Context id="SHADOWMAP">
	<Shaders vertex="VS_SHADOWMAP" fragment="FS_SHADOWMAP" />
</Context>

<Context id="LIGHTING">
	<Shaders vertex="VS_GENERAL" fragment="FS_LIGHTING" />
	<RenderConfig writeDepth="false" blendMode="ADD" />
</Context>

<Context id="AMBIENT">
	<Shaders vertex="VS_GENERAL" fragment="FS_AMBIENT" />
</Context>


[[VS_GENERAL]]
// =================================================================================================

#include "shaders/utilityLib/vertCommon.glsl"

uniform vec3 viewer;
attribute vec2 texCoords0;

varying vec4 pos, vsPos;
varying vec2 texCoords;

varying vec3 tsbNormal;

uniform vec4 terrainSize;

void main( void )
{
	// Calculate normal
	vec3 _normal = calcWorldVec( gl_Normal );

	// Calculate tangent and bitangent
	tsbNormal = _normal;

	// Calculate world space position
	pos = calcWorldPos( gl_Vertex );

	vsPos = calcViewPos( pos );

	// Calculate tangent space eye vector

	// Calculate texture coordinates and clip space position
	texCoords = vec2(gl_Vertex.x, gl_Vertex.z);
	gl_Position = gl_ModelViewProjectionMatrix * pos;
}


[[FS_ATTRIBPASS]]
// =================================================================================================

#include "shaders/utilityLib/fragDeferredWrite.glsl" />

uniform vec4 specParams;
uniform sampler2D colormap;
uniform sampler2D detailmap;

varying vec4 pos;
varying vec2 texCoords;

varying vec3 tsbNormal;

uniform vec4 terrainSize;

void main( void )
{
	vec3 newCoords = vec3( texCoords, 0 );
	
	// Flip texture vertically to match the GL coordinate system
	//newCoords.t *= -1.0;

	vec3 detail = texture2D( detailmap, newCoords.st ).rgb;
	newCoords.s /= terrainSize.x;
	newCoords.t /= terrainSize.y;
	vec3 albedo = texture2D( colormap, newCoords.st ).rgb;
	albedo += (detail - 0.5) * 0.3;
	
	vec3 normal = tsbNormal;

	vec3 newPos = pos.xyz;

	setMatID( 1.0 );
	setPos( newPos );
	setNormal( normalize( normal ) );
	setAlbedo( albedo );
	setSpecMask( specParams.x );
}

	
[[VS_SHADOWMAP]]
// =================================================================================================
	
#include "shaders/utilityLib/vertCommon.glsl"

uniform vec4 lightPos;
varying float dist;

void main( void )
{
	vec4 pos = calcWorldPos( gl_Vertex );
	dist = length( lightPos.xyz - pos.xyz ) / lightPos.w;
	
	gl_Position = gl_ModelViewProjectionMatrix * pos;
}
	
	
[[FS_SHADOWMAP]]
// =================================================================================================

uniform float shadowBias;
varying float dist;

void main( void )
{
	gl_FragDepth = dist + shadowBias;
	
	// Clearly better bias but requires SM 3.0
	// gl_FragDepth =  dist + abs( dFdx( dist ) ) + abs( dFdy( dist ) ) + shadowBias;
}


[[FS_LIGHTING]]
// =================================================================================================

#include "shaders/utilityLib/fragLighting.glsl" />

uniform vec4 specParams;
uniform sampler2D colormap;
uniform sampler2D detailmap;

varying vec4 pos, vsPos;
varying vec2 texCoords;

varying vec3 tsbNormal;

uniform vec4 terrainSize;

void main( void )
{
	vec3 newCoords = vec3( texCoords, 0 );
	
	// Flip texture vertically to match the GL coordinate system
	//newCoords.t *= -1.0;

	vec3 detail = texture2D( detailmap, newCoords.st ).rgb;
	newCoords.s /= terrainSize.x;
	newCoords.t /= terrainSize.y;
	vec3 albedo = texture2D( colormap, newCoords.st ).rgb;
	albedo += (detail - 0.5) * 0.3;
	
	vec3 normal = tsbNormal;

	vec3 newPos = pos.xyz;

	gl_FragColor.rgb =
		calcPhongSpotLight( newPos, normalize( normal ), albedo, specParams.x, specParams.y, -vsPos.z, 0.3 );
}


[[FS_AMBIENT]]	
// =================================================================================================

#include "shaders/utilityLib/fragLighting.glsl" />

uniform sampler2D colormap;
uniform sampler2D detailmap;
uniform samplerCube ambientMap;


varying vec4 pos;
varying vec2 texCoords;

varying vec3 tsbNormal;

uniform vec4 terrainSize;

void main( void )
{
	vec3 newCoords = vec3( texCoords, 0 );
	
	// Flip texture vertically to match the GL coordinate system
	//newCoords.t *= -1.0;

	vec3 detail = texture2D( detailmap, newCoords.st ).rgb;
	newCoords.s /= terrainSize.x;
	newCoords.t /= terrainSize.y;
	vec3 albedo = texture2D( colormap, newCoords.st ).rgb;
	albedo += (detail - 0.5) * 0.3;
	
	vec3 normal = tsbNormal;
	
	gl_FragColor.rgb = albedo * textureCube( ambientMap, normal ).rgb;
}
